SET search_path = 'project';

CREATE FUNCTION check_date_of_termination() RETURNS TRIGGER AS
$BODY$
DECLARE
BEGIN
	SET search_path = 'project';
	
	IF EXISTS (SELECT H.ManagerContactID FROM Employee E, Manager M, HotelChain H 
		WHERE E.PersonContactID = M.EmployeeContactID AND M.EmployeeContactID = H.ManagerContactID 
		AND E.PersonContactID = NEW.PersonContactID) THEN
 			RAISE EXCEPTION 'Unable to set Date Of Termination because manager is currently working for a hotel.';
	ELSIF EXISTS (SELECT W.NonManagerContactID FROM Employee E, NonManager N, worksFor W 
		WHERE E.PersonContactID = N.EmployeeContactID AND N.EmployeeContactID = W.NonManagerContactID 
		AND E.PersonContactID = NEW.PersonContactID) THEN
	    	RAISE EXCEPTION 'Unable to set Date Of Termination because non-manager is currently working for a hotel.';
	END IF;
	
	RETURN NEW;
END
$BODY$ LANGUAGE plpgsql;

CREATE FUNCTION check_manager() RETURNS TRIGGER AS
$BODY$
DECLARE
	newManagerDateOfTermination DATE;
BEGIN
	SET search_path = 'project';
	
	IF EXISTS (SELECT M.EmployeeContactID FROM Manager M WHERE M.EmployeeContactID = NEW.ManagerContactID) THEN
		
		SELECT DateOfTermination INTO newManagerDateOfTermination FROM Employee E WHERE 
			E.PersonContactID = NEW.ManagerContactID;
					 				 
		IF (newManagerDateOfTermination IS NOT NULL) THEN
	    	RAISE EXCEPTION 'Unable to update ManagerContactID in HotelChain table because new manager has date of termination.';
		END IF;
		
	END IF;
	
	RETURN NEW;
END
$BODY$ LANGUAGE plpgsql;

CREATE FUNCTION check_non_manager() RETURNS TRIGGER AS
$BODY$
DECLARE
	newNonManagerDateOfTermination DATE;
BEGIN
	SET search_path = 'project';
	
	IF EXISTS (SELECT N.EmployeeContactID FROM NonManager N WHERE N.EmployeeContactID = NEW.NonManagerContactID) THEN
		SELECT DateOfTermination INTO newNonManagerDateOfTermination FROM Employee E WHERE 
			E.PersonContactID = NEW.NonManagerContactID;
			
		IF (newNonManagerDateOfTermination IS NOT NULL) THEN
	    	RAISE EXCEPTION 'Unable to update NonManagerContactID in worksFor table because new non-manager has date of termination.';
		END IF;
		
	END IF;
	
	RETURN NEW;
END
$BODY$ LANGUAGE plpgsql;

CREATE FUNCTION check_CheckInDateTime() RETURNS TRIGGER AS
$BODY$
DECLARE
	registrationDate DATE;
BEGIN
	SET search_path = 'project';
			
	SELECT C.DateOfRegistration INTO registrationDate FROM Booking B, Customer C 
		WHERE B.CustomerContactID = C.PersonContactID AND B.BookingID = NEW.BookingID;
	
	IF ( DATE(NEW.CheckInDateTime) < registrationDate ) THEN
		RAISE EXCEPTION 'Unable to update CheckInDateTime in Renting table because check-in date is before customer registration date.';
	END IF;
	
	RETURN NEW;
END
$BODY$ LANGUAGE plpgsql;

CREATE FUNCTION check_ReserveStartDateTime() RETURNS TRIGGER AS
$BODY$
DECLARE
	registrationDate DATE;
BEGIN
	SET search_path = 'project';
		
	SELECT C.DateOfRegistration INTO registrationDate FROM Booking B, Customer C 
		WHERE B.CustomerContactID = C.PersonContactID AND B.BookingID = NEW.BookingID;
	
	IF ( DATE(NEW.ReserveStartDateTime) < registrationDate ) THEN
		RAISE EXCEPTION 'Unable to update ReservedStateDateTime in Booking table because reserved start date is before customer registration date.';
	END IF;
	
	RETURN NEW;
END
$BODY$ LANGUAGE plpgsql;

CREATE FUNCTION delete_contactableEntity() RETURNS TRIGGER AS
$BODY$
DECLARE
BEGIN
	SET search_path = 'project';
	
	IF (TG_TABLE_NAME = 'customer' OR TG_TABLE_NAME = 'employee') THEN
		DELETE FROM ContactableEntity WHERE ContactID = OLD.PersonContactID;
	ELSIF (TG_TABLE_NAME = 'manager' OR TG_TABLE_NAME = 'nonmanager') THEN
		DELETE FROM ContactableEntity WHERE ContactID = OLD.EmployeeContactID;	
	ELSIF (TG_TABLE_NAME = 'person' OR TG_TABLE_NAME = 'hotelchain' OR TG_TABLE_NAME = 'hotelbrand') THEN
		DELETE FROM ContactableEntity WHERE ContactID = OLD.ContactID;
	END IF;
	
	RETURN NEW;
END
$BODY$ LANGUAGE plpgsql;

CREATE FUNCTION check_ContactID_exclusive_person_brand_chain() RETURNS TRIGGER AS
$BODY$
DECLARE
BEGIN
	SET search_path = 'project';
	
	IF (TG_TABLE_NAME = 'person') THEN
		IF (EXISTS (SELECT ContactID FROM HotelChain WHERE ContactID = NEW.ContactID) )	THEN
			RAISE EXCEPTION 'Unable to update ContactID to % because it is used by HotelChain',  NEW.ContactID;
		ELSIF ( EXISTS(SELECT ContactID FROM HotelBrand WHERE ContactID = NEW.ContactID) ) THEN
			RAISE EXCEPTION 'Unable to update ContactID % because it is used by HotelBrand',  NEW.ContactID;
		END IF;
	ELSIF (TG_TABLE_NAME = 'hotelchain') THEN
		IF (EXISTS (SELECT ContactID FROM Person WHERE ContactID = NEW.ContactID) )	THEN
			RAISE EXCEPTION 'Unable to update ContactID to % because it is used by Person',  NEW.ContactID;
		ELSIF ( EXISTS(SELECT ContactID FROM HotelBrand WHERE ContactID = NEW.ContactID) ) THEN
			RAISE EXCEPTION 'Unable to update ContactID to % because it is used by HotelBrand',  NEW.ContactID;
		END IF;
	ELSIF (TG_TABLE_NAME = 'hotelbrand') THEN
		IF (EXISTS (SELECT ContactID FROM Person WHERE ContactID = NEW.ContactID) )	THEN
			RAISE EXCEPTION 'Unable to update ContactID to % because it is used by Person',  NEW.ContactID;
		ELSIF ( EXISTS(SELECT ContactID FROM HotelChain WHERE ContactID = NEW.ContactID) ) THEN
			RAISE EXCEPTION 'Unable to update ContactID to % because it is used by HotelChain',  NEW.ContactID;
		END IF;
	END IF;
	
	RETURN NEW;
END
$BODY$ LANGUAGE plpgsql;


CREATE FUNCTION check_double_booking() RETURNS TRIGGER AS
$BODY$
DECLARE
BEGIN
	SET search_path = 'project';
	
	IF EXISTS (SELECT BookingID FROM Booking B
			  WHERE B.HotelChainContactID = NEW.HotelChainContactID AND B.RoomNumber = NEW.RoomNumber AND (
			   (extract(epoch from B.ReserveEndDateTime)>=extract(epoch from NEW.ReserveStartDateTime) 
					AND extract(epoch from B.ReserveEndDateTime)<=extract(epoch from NEW.ReserveEndDateTime)) OR
				(extract(epoch from B.ReserveStartDateTime)>=extract(epoch from NEW.ReserveStartDateTime) 
				 	AND extract(epoch from B.ReserveStartDateTime)<=extract(epoch from NEW.ReserveEndDateTime)) OR
				(extract(epoch from B.ReserveStartDateTime)<=extract(epoch from NEW.ReserveStartDateTime) 
				 	AND extract(epoch from B.ReserveEndDateTime)>=extract(epoch from NEW.ReserveEndDateTime))		
			  ) ) THEN
			  RAISE EXCEPTION 'The room is already booked during the provided time period.';
	END IF;
	
	RETURN NEW;
END
$BODY$ LANGUAGE plpgsql;


CREATE FUNCTION check_renting_start_time() RETURNS TRIGGER AS
$BODY$
DECLARE
BEGIN
	SET search_path = 'project';
	
	IF NOT EXISTS (SELECT BookingID FROM Booking B WHERE NEW.BookingID = B.BookingID AND
			  		extract(epoch from NEW.CheckInDateTime)>= extract(epoch from B.ReserveStartDateTime) AND
			  		extract(epoch from NEW.CheckInDateTime)<=extract(epoch from B.ReserveEndDateTime) )
	
	THEN
		RAISE EXCEPTION 'Cannot create renting since check-in time is not within reservation period.';
	END IF;
	
	RETURN NEW;
END
$BODY$ LANGUAGE plpgsql;



CREATE TRIGGER set_date_of_termination BEFORE INSERT OR UPDATE OF DateOfTermination ON Employee
FOR EACH ROW
WHEN (NEW.DateOfTermination IS NOT NULL)
EXECUTE PROCEDURE check_date_of_termination();

CREATE TRIGGER set_manager BEFORE INSERT OR UPDATE OF ManagerContactID ON HotelChain
FOR EACH ROW
EXECUTE PROCEDURE check_manager();

CREATE TRIGGER set_non_manager BEFORE INSERT OR UPDATE OF NonManagerContactID ON worksFor
FOR EACH ROW
EXECUTE PROCEDURE check_non_manager();

CREATE TRIGGER set_CheckInDateTime BEFORE INSERT OR UPDATE OF CheckInDateTime ON Renting
FOR EACH ROW
EXECUTE PROCEDURE check_CheckInDateTime();

CREATE TRIGGER insert_ReserveStartDateTime AFTER INSERT ON Booking
FOR EACH ROW
EXECUTE PROCEDURE check_ReserveStartDateTime();

CREATE TRIGGER set_ReserveStartDateTime BEFORE UPDATE OF ReserveStartDateTime ON Booking
FOR EACH ROW
EXECUTE PROCEDURE check_ReserveStartDateTime();

CREATE TRIGGER delete_customer AFTER DELETE ON Customer
FOR EACH ROW
EXECUTE PROCEDURE delete_contactableEntity();

CREATE TRIGGER delete_manager AFTER DELETE ON Manager
FOR EACH ROW
EXECUTE PROCEDURE delete_contactableEntity();

CREATE TRIGGER delete_nonmanager AFTER DELETE ON NonManager
FOR EACH ROW
EXECUTE PROCEDURE delete_contactableEntity();

CREATE TRIGGER delete_employee AFTER DELETE ON Employee
FOR EACH ROW
EXECUTE PROCEDURE delete_contactableEntity();

CREATE TRIGGER delete_person AFTER DELETE ON Person
FOR EACH ROW
EXECUTE PROCEDURE delete_contactableEntity();

CREATE TRIGGER delete_hotelchain AFTER DELETE ON HotelChain
FOR EACH ROW
EXECUTE PROCEDURE delete_contactableEntity();

CREATE TRIGGER delete_hotelbrand AFTER DELETE ON HotelBrand
FOR EACH ROW
EXECUTE PROCEDURE delete_contactableEntity();

CREATE TRIGGER check_person_ContactID BEFORE INSERT OR UPDATE OF ContactID ON Person
FOR EACH ROW
EXECUTE PROCEDURE check_ContactID_exclusive_person_brand_chain();

CREATE TRIGGER check_hotelchain_ContactID BEFORE INSERT OR UPDATE OF ContactID ON HotelChain
FOR EACH ROW
EXECUTE PROCEDURE check_ContactID_exclusive_person_brand_chain();

CREATE TRIGGER check_hotelbrand_ContactID BEFORE INSERT OR UPDATE OF ContactID ON HotelBrand
FOR EACH ROW
EXECUTE PROCEDURE check_ContactID_exclusive_person_brand_chain();

CREATE TRIGGER prevent_double_booking BEFORE INSERT OR UPDATE ON Booking
FOR EACH ROW
EXECUTE PROCEDURE check_double_booking();

CREATE TRIGGER prevent_invalid_check_in BEFORE INSERT OR UPDATE ON Renting
FOR EACH ROW
EXECUTE PROCEDURE check_renting_start_time();