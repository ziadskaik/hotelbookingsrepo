SET search_path = 'project';

CREATE TABLE IF NOT EXISTS ContactableEntity(
	ContactID SERIAL, 
	PhysicalAddress_Line1 VARCHAR, 
	PhysicalAddress_Line2 VARCHAR, 
	City VARCHAR, 
	Prov_State VARCHAR, 
	Country VARCHAR, 
	Postal_Zip VARCHAR,
	PRIMARY KEY (ContactID)
);
											  
CREATE TABLE IF NOT EXISTS EmailInformation(
	EmailAddress VARCHAR,
	Description VARCHAR, 
	ContactID INTEGER NOT NULL,
	PRIMARY KEY (EmailAddress),
	FOREIGN KEY (ContactID) REFERENCES ContactableEntity (ContactID) ON DELETE CASCADE ON UPDATE CASCADE
);
											 
CREATE TABLE IF NOT EXISTS PhoneInformation(
	PhoneNumber VARCHAR,
	Description VARCHAR, 
	ContactID INTEGER NOT NULL,
	PRIMARY KEY (PhoneNumber),
	FOREIGN KEY (ContactID) REFERENCES ContactableEntity (ContactID) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS Person(
	ContactID INTEGER, 
	SIN VARCHAR UNIQUE, 
	FirstName VARCHAR NOT NULL,
	LastName VARCHAR NOT NULL,
	Password VARCHAR NOT NULL,
	PRIMARY KEY (ContactID),
	FOREIGN KEY (ContactID) REFERENCES ContactableEntity (ContactID) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS Employee(
	PersonContactID INTEGER,
	Salary NUMERIC(8,2) NOT NULL,
	DateOfHire DATE NOT NULL,
	DateOfTermination DATE,
	PRIMARY KEY (PersonContactID),
	FOREIGN KEY (PersonContactID) REFERENCES Person (ContactID) on DELETE CASCADE ON UPDATE CASCADE,
	CHECK (Salary >= 0),
	CHECK (DateOfTermination >= DateOfHire)
);

CREATE TABLE IF NOT EXISTS Customer(
	PersonContactID INTEGER,
	DateOfRegistration DATE NOT NULL,
	PRIMARY KEY (PersonContactID),
	FOREIGN KEY (PersonContactID) REFERENCES Person (ContactID) on DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS NonManager(
	EmployeeContactID INTEGER,
	EvaluationRating INTEGER NOT NULL,
	PRIMARY KEY (EmployeeContactID),
	FOREIGN KEY (EmployeeContactID) REFERENCES Employee (PersonContactID) ON DELETE CASCADE ON UPDATE CASCADE,
	CHECK (EvaluationRating BETWEEN 1 AND 10)
);

CREATE TABLE IF NOT EXISTS Manager(
	EmployeeContactID INTEGER,
	IsAnEmergencyContact BOOLEAN NOT NULL,
	PRIMARY KEY (EmployeeContactID),
	FOREIGN KEY (EmployeeContactID) REFERENCES Employee (PersonContactID) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS HotelBrand(
	ContactID INTEGER, 
	BrandName VARCHAR UNIQUE NOT NULL,
	PRIMARY KEY (ContactID),
	FOREIGN KEY (ContactID) REFERENCES ContactableEntity (ContactID) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS HotelChain(
	ContactID INTEGER, 
	StarCategory INTEGER NOT NULL,
	HotelBrandContactID INTEGER NOT NULL,
	ManagerContactID INTEGER NOT NULL,
	PRIMARY KEY (ContactID),
	FOREIGN KEY (ContactID) REFERENCES ContactableEntity (ContactID) ON DELETE CASCADE ON UPDATE CASCADE,
	FOREIGN KEY (HotelBrandContactID) REFERENCES HotelBrand (ContactID) ON DELETE CASCADE ON UPDATE CASCADE,
	FOREIGN KEY (ManagerContactID) REFERENCES Manager (EmployeeContactID),
    	CHECK (StarCategory BETWEEN 1 AND 5)
);

CREATE TABLE IF NOT EXISTS worksFor(
	HotelChainContactID INTEGER,
	NonManagerContactID INTEGER,
	Position VARCHAR,
	PRIMARY KEY (HotelChainContactID, NonManagerContactID, Position),
	FOREIGN KEY (HotelChainContactID) REFERENCES HotelChain (ContactID) ON DELETE CASCADE ON UPDATE CASCADE,
	FOREIGN KEY (NonManagerContactID) REFERENCES NonManager (EmployeeContactID) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS RoomType(
	RoomTypeID INTEGER,
	Price NUMERIC(8,2) NOT NULL,
	Capacity INTEGER NOT NULL,
	View VARCHAR,
	Extendable BOOLEAN NOT NULL,
	PRIMARY KEY (RoomTypeID),
	CHECK (Price > 0),
    CHECK (Capacity > 0),
	CHECK ( View IN ('Sea', 'Mountain') )
);

CREATE TABLE IF NOT EXISTS HotelRoom(
	HotelChainContactID INTEGER,
	RoomNumber INTEGER,
	RoomTypeID INTEGER NOT NULL,
	PRIMARY KEY (HotelChainContactID, RoomNumber),
	FOREIGN KEY (HotelChainContactID) REFERENCES HotelChain (ContactID) ON DELETE CASCADE ON UPDATE CASCADE,
	FOREIGN KEY (RoomTypeID) REFERENCES RoomType (RoomTypeID)
);
 
CREATE TABLE IF NOT EXISTS RoomTypeAmenity(
	RoomTypeID INTEGER,
	Amenity VARCHAR,
	PRIMARY KEY (RoomTypeID, Amenity),
	FOREIGN KEY (RoomTypeID) REFERENCES RoomType (RoomTypeID) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS Booking(
	BookingID INTEGER,
	ReserveStartDateTime TIMESTAMP NOT NULL,
	ReserveEndDateTime TIMESTAMP NOT NULL,
	TotalNumberOfOccupants INTEGER NOT NULL,
	RoomNumber INTEGER NOT NULL,
	HotelChainContactID INTEGER NOT NULL,
	CustomerContactID INTEGER NOT NULL,
	PRIMARY KEY (BookingID),
	FOREIGN KEY (RoomNumber, HotelChainContactID) REFERENCES HotelRoom (RoomNumber, HotelChainContactID),
	FOREIGN KEY (CustomerContactID) REFERENCES Customer (PersonContactID) ON UPDATE CASCADE,
	CHECK (ReserveEndDateTime > ReserveStartDateTime),
	CHECK (TotalNumberOfOccupants > 0)
);

CREATE TABLE IF NOT EXISTS Renting(
	BookingID INTEGER,
	CheckInDateTime TIMESTAMP NOT NULL,
	CheckOutDateTime TIMESTAMP,
	Payment NUMERIC(8,2),
	PRIMARY KEY (BookingID),
	FOREIGN KEY (BookingID) REFERENCES Booking (BookingID) ON DELETE CASCADE ON UPDATE CASCADE,
	CHECK(CheckOutDateTime > CheckInDateTime),
	CHECK(Payment >= 0)
);

CREATE TABLE IF NOT EXISTS BookingArchive(
	ArchiveID INTEGER,
	ReserveStartDateTime TIMESTAMP,
	ReserveEndDateTime TIMESTAMP,
	TotalNumberOfOccupants INTEGER,
	CheckInDateTime TIMESTAMP,
	CheckOutDateTime TIMESTAMP,
	RoomNumber INTEGER,
	RoomPrice NUMERIC(8,2),
	RoomCapacity INTEGER,
	RoomView VARCHAR,
	RoomExtendable BOOLEAN,
	HotelBrandName VARCHAR,
	HotelChainPhysicalAddress_Line1 VARCHAR,
	HotelChainPhysicalAddress_Line2 VARCHAR,
	HotelChainCity VARCHAR,
	HotelChainProv_State VARCHAR,
	HotelChainCountry VARCHAR,
	HotelChainPostal_Zip VARCHAR,
	CustomerContactID INTEGER,
	PRIMARY KEY (ArchiveID),
	FOREIGN KEY (CustomerContactID) REFERENCES Customer (PersonContactID) ON UPDATE CASCADE,
	CHECK (ReserveEndDateTime > ReserveStartDateTime),
	CHECK (CheckOutDateTime > CheckInDateTime),	
	CHECK (RoomPrice > 0),
    CHECK (RoomCapacity > 0),
	CHECK ( RoomView IN ('Sea', 'Mountain') )
);

CREATE TABLE IF NOT EXISTS BookingArchiveAmenity(
	ArchiveID INTEGER,
	Amenity VARCHAR,
	PRIMARY KEY (ArchiveID, Amenity),
	FOREIGN KEY (ArchiveID) REFERENCES BookingArchive (ArchiveID) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS DatabaseAdministrator(
	UserName VARCHAR,
	Password VARCHAR NOT NULL,
	PRIMARY KEY (UserName)
);