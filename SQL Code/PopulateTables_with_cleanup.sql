SET search_path = 'project';

DELETE FROM Booking;
DELETE FROM BookingArchive;
DELETE FROM ContactableEntity;
DELETE FROM RoomType;
DELETE FROM DatabaseAdministrator;

-- Customer
INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(1, '687 Birchland Crescent', '', 'Stittsville', 'Ontario', 'Canada', 'K2S 0S9');

INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(2, '11 Cedar Valley Drive', '', 'Kanata', 'Ontario', 'Canada', 'K2M 2Y5');
	
INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(3, '2881 Richmond Road', 'Unit 1210', 'Nepean', 'Ontario', 'Canada', 'K2B 8J5');	

-- Manager
INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(4, '18 Deerfield Drive', 'Unit 911', 'Nepean', 'Ontario', 'Canada', 'K2G 4L1');
	
INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(5, '55 Skymark Drive', 'Unit 2105', 'North York', 'Ontario', 'Canada', 'M2H 3N4');

-- NonManager
INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(6, '191 Main Street West', 'Unit 804', 'Hamilton', 'Ontario', 'Canada','L8P 4S2');
	
INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(7, '6 Jardin Hill Court', '', 'North York', 'Ontario', 'Canada', 'M2H 3R8');

-- HotelBrand (6 in total)
INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(8, '7930 Jones Branch Drive', '', 'McLean', 'Virginia', 'USA', '22102');
	
INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(9, '10400 Fernwood Road', '', 'Bethesda', 'Maryland', 'USA', '20817');

INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(10, '150 N Riverside Plaza', '', 'Chicago', 'Illinois', 'USA', '60606');

INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(11, 'Broadwater Park', '', 'Denham', 'Buckinghamshire', 'UK', 'UB9 5HR');
	
INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(12, '6201 N. 24th Parkway', '', 'Phoenix', 'Arizona', 'USA', '85016');
	
INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(13, '310 - 1755 West Broadway', '', 'Vancouver', 'Bristish Columbia', 'Canada', 'V6J 4S5');	

-- HotelChain (31 in total)
INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(14, '50 Bodrington Court', '', 'Markham', 'Ontario', 'Canada', 'L6H0A9');	

INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(15, '900 Great Lakes Avenue', '', 'Kanata', 'Ontario', 'Canada', 'K2K 0L4');	

INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(16, '361 Queen Street', '', 'Ottawa', 'Ontario', 'Canada', 'K1R 0C7');
	
INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(17, '125 Lusk Street', '', 'Nepean', 'Ontario', 'Canada', 'K2J 6S5');
	
INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(18, 'One Bigelow Square', '', 'Pittsburgh', 'Pennsylvania', 'USA', '15219');
	
INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(19, '1251 Maritime Way', '', 'Kanata', 'Ontario', 'Canada', 'K2K 0J6');
	
INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(20, '101 Lyon Street North', '', 'Ottawa', 'Ontario', 'Canada', 'K1R 5T9');
	
INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(21, '578 Terry Fox Drive', '', 'Kanata', 'Ontario', 'Canada', 'K2L 4G8');
	
INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(22, '808 N. High Street', '', 'Columbus', 'Ohio', 'USA', '43215');
	
INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(23, '6540 Riverside Drive', '', 'Dublin', 'Ohio', 'USA', '43017');

INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(24, '1050 Paignton House Road', 'Minett', 'Muskoka Lakes', 'Ontario', 'Canada', 'POB 1G0');
	
INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(25, '7490 Vantage Drive', '', 'Columbus', 'Ohio', 'USA', '43235');
	
INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(26, '300 Moodie Drive', '', 'Ottawa', 'Ontario', 'Canada', 'K2H 8G3');

INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(27, '325 Dalhousie Street', '', 'Ottawa', 'Ontario', 'Canada', 'K1N 7G1');
	
INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(28, '3100 Dallas Parkway', '', 'Plano', 'Texas', 'US', '75093');
	
INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(29, '153 West 57th Street', '', 'New York', 'New York', 'US', '10019');
	
INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(30, '101 Kanata Avenue', '', 'Kanata', 'Ontario', 'Canada', 'K2T 1E6');
	
INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(31, '2055 Robertson Road', '', 'Ottawa', 'Ontario', 'Canada', 'K2H 5Y9');
	
INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(32, '111 Cone Street NW', '', 'Atlanta', 'Georgia', 'US', '30303');

INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(33, '800 Sidney Marcus Boulevard', '', 'Atlanta', 'Georgia', 'US', '30324');
	
INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(34, '225 Front St W', '', 'Toronto', 'Ontario', 'Canada', 'M5V 2X3');

INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(35, '1876 Robertson Road', '', 'Nepean', 'Ontario', 'Canada', 'K2H 5B8');
	
INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(36, '160 Hearst Way', '', 'Ottawa', 'Ontario', 'Canada', 'K2L 3A2');
	
INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(37, '1274 Carling Ave', '', 'Ottawa', 'Ontario', 'Canada', 'K1Z 7K8');	

INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(38, '1604 Quintard Ave', '', 'Anniston', 'Alabama', 'USA', '36201');
	
INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(39, '4100 W Flagler Street', '', 'Miami', 'Florida', 'USA', '33134');
	
INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(40, '250 W Hunt Club Road', '', 'Ottawa', 'Ontario', 'Canada', 'K2E 0B7');
	
INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(41, '8451 Parkwood Boulevard', '', 'Plano', 'Texas', 'USA', '75024');
	
INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(42, '8001 11 Street SE', '', 'Calgary', 'Alberta', 'Canada', 'T2H 0B8');
	
INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(43, '10111 Ellerslie Rd SW', '', 'Edmonton', 'Alberta', 'Canada', 'T6X 0J3');	

INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(44, '999 Rue de Sérigny', '', 'Montréal', 'Quebec', 'Canada', 'J4K 2T1');	

-- Manager
INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(45, '1107 Auger Avenue', '', 'Sudbury', 'Ontario', 'Canada', 'P3A 4B1');
	
INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(46, '2943 Hwy 69 North', 'Unit 110', 'Val Caron', 'Ontario', 'Canada', 'P3N 1N3');

INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(47, '125 Bayfield Street', '', 'Barrie', 'Ontario', 'Canada', 'L4M 4T5');

INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(48, '1525 Rockwell Avenue', '', 'Cleveland', 'Ohio', 'USA', '44114');

INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(49, '5050 Yonge Street', '', 'Toronto', 'Ontario', 'Canada', 'M2N 5N8');
	
INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(50, '1280 Main Street West', '', 'Hamilton', 'Ontario', 'Canada', 'L8S 4L8');

INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(51, '135 Kaiser William Avenue West', '', 'Langenburg', 'Saskatchewan', 'Canada', 'S0A 2A0');
	
INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(52, '1705 Meyerside Drive', '', 'Mississauga', 'Ontario', 'Canada', 'L5T 1B9');
	
INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(53, '941 Progress Ave', '', 'Scarborough', 'Ontario', 'Canada', 'M1G 3T8');
	
INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(54, '241 5th Street', '', 'San Francisco', 'California', 'USA', '94103');
	
INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(55, '51 Madison Avenue', '22nd Floor', 'New York', 'New York', 'USA', '10010');
	
INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(56, '14 Plaza Drive', '', 'Latham', 'New York', 'USA', '12110');
	
INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(57, '351 King Street East', 'Suite 1600', 'Toronto', 'Ontario', 'Canada', 'M5A 0N1');
	
INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(58, '503 Sud Street', '', 'Cowansville', 'Quebec', 'Canada', 'J2K 2X9');
	
INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(59, '2425 E Camelback Road', '', 'Pheonix', 'Arizona', 'USA', '85016');
	
INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(60, '31 Saulter Street', 'Unit 2', 'Toronto', 'Ontario', 'Canada', 'M4M 2H7');
	
INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(61, '18 York Street', 'Suite 2600', 'Toronto', 'Ontario', 'Canada', 'M5J 0B2');
	
INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(62, '50 Bayshore Drive', '', 'Nepean', 'Ontario', 'Canada', 'K2B 6M8');
			
-- Non-Manager	
INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(63, '50 Centrepointe Drive', '', 'Nepean', 'Ontario', 'Canada', 'K2G 5L4');

INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(64, '620 Colorado Avenue', '', 'Onadarko', 'Oklahoma', 'USA', '73005');	

INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(65, '900 Morrison Dr', '', 'Ottawa', 'Ontario', 'Canada', 'K2H 8K7');	

INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(66, '999 Barton Street', '', 'Stoney Creek', 'Ontario', 'Canada', 'L8E 5H4');
	
INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(67, '2725 Queensview Drive', 'Suite 500', 'Ottawa', 'Ontario', 'Canada', 'K2B 0A1');	

INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(68, '5450 Canotek Road', 'Number 62', 'Gloucester', 'Ontario', 'Canada', 'K1J 9G4');
	
INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(69, '1 University Plaza', '', 'Platteville', 'Wisconsin', 'USA', '53818');
	
INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(70, '150 Tunney''s Pasture Driveway', '', 'Ottawa', 'Ontario', 'Canada', 'K1A 0T6');
	
INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(71, '6225 Colony Street', '', 'Bakersfield', 'California', 'USA', '93307');

INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(72, '6030 West Harold Gatty Drive', '', 'Salt Lake City', 'Utah', 'USA', '84116');

-- Manager
INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(73, '8600 Hayden Place', '', 'Culver City', 'California', 'USA', '90232');
	
INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(74, '865 Carling Avenue', 'Suite 600', 'Ottawa', 'Ontraio', 'Canada', 'K1S 5S8');
	
-- NonManager	
INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(75, '1719 South Main Street', '', 'Salt Lake City', 'Utah', 'USA', '84115');
	
INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(76, '5 Necco Street', '', 'Boston', 'Massachusetts', 'USA', '02210');
	
INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(77, '2121 Tasman Drive', '', 'Santa Clara', 'California', 'USA', '95054');
	
INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(78, '1 Place Ville Marie', 'Suite 3240', 'Montreal', 'Quebec', 'Canada', 'H3B 3N2');
	
-- Manager
INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(79, '1050 Morrison Drive', '', 'Ottawa', 'Ontario', 'Canada', 'K2H 8K7');

INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(80, '850 Summer Street', 'Suite 207', 'Boston', 'Massachusetts', 'USA', '02127');
	
-- NonManager
INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(81, '540 7 Avenue SW', '', 'Medicine Hat', 'Alberta', 'Canada', 'T1A 5B9');

INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(82, '101 Queensway West', '204', 'Mississauga', 'Ontario', 'Canada', 'L5B 2P7');
	
INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(83, '117 Elma Street', '', 'Okitoks', 'Alberta', 'Canada', 'T1S 1J9');
	
INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(84, '5201 43 Street', '182', 'Red Deer', 'Alberta', 'Canada', 'T4N 1C7');
	
-- Manager
INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(85, '5201 43 Street', '279', 'Red Deer', 'Alberta', 'Canada', 'T4N 1C7');
	
INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(86, '2827 30 Avenue', '1201', 'Red Deer', 'Alberta', 'Canada', 'T4R 2P7');	

INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(87, '10628 96 Street NW', '', 'Edmonton', 'Alberta', 'Canada', 'T5H 2J2');	

INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(88, '5010 Richard Road SW', '220', 'Calgary', 'Alberta', 'Canada', 'T3E 6L1');	

-- NonManager
INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(89, '28 Oki Drive NW', '', 'Calgary', 'Alberta', 'Canada', 'T3B 6A8');
	
INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(90, '4411 16 Avenue NW', '242', 'Calgary', 'Alberta', 'Canada', 'T3B 0M3');

INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(91, '4448 Front Street SE', '', 'Calgary', 'Alberta', 'Canada', 'T3M 1M4');
	
INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(92, '29 Street NW', '1403', 'Calgary', 'Alberta', 'Canada', 'T2N 2T9');
	
-- Manager
INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(93, '14 Street SW', '7007', 'Calgary', 'Alberta', 'Canada', 'T2V 1P9');
	
INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(94, '4070 Bowness Road NW', '', 'Calgary', 'Alberta', 'Canada', 'T3B 3R7');

INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(95, '3535 Research Road NW', '9', 'Calgary', 'Alberta', 'Canada', 'T2L 2K8');

INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(96, '3134 Hospital Drive NW', 'Room 7543', 'Calgary', 'Alberta', 'Canada', 'T2N 2Y9');
	
-- NonManager	
INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(97, '3500 26 Avenue NW', 'Room 1424', 'Calgary', 'Alberta', 'Canada', 'T1Y 6J4');	

INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(98, '2139 4 Avenue NW', '201', 'Calgary', 'Alberta', 'Canada', 'T2N 0N6');
	
INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(99, '131 9 Avenue SW', '415', 'Calgary', 'Alberta', 'Canada', 'T2P 1K1');
	
INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(100, '2204 2 Street SW', '304', 'Calgary', 'Alberta', 'Canada', 'T2S 3C2');
	
-- Manager
INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(101, '300 Meredith Road NE', '609', 'Calgary', 'Alberta', 'Canada', 'T2E 7A8');

INSERT INTO ContactableEntity 
	(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip) VALUES
	(102, '1848 Varsity Estates Drive NW', '', 'Calgary', 'Alberta', 'Canada', 'T3B 2W9');

-- HotelBrand
INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-703-883-1000', 'Global Headquarters', 8);
	
INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-301-380-3000', 'Corporate Headquarters', 9);
	
INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-312-750-1234', 'Corporate Headquarters', 10);

INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-888-591-1234', 'Customer Service', 10);

INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-312-780-6234', 'Rosemont Purchasing', 10);
	
INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-800-233-1234', 'U.S., CANADA & CARIBBEAN', 10);
	
INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-800-522-1100', 'Hyatt National Sales Office', 10);
	
INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('44 (0) 1895 512000', 'Global Headquarters', 11);
	
INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-877-424-2449', 'Customer Service', 11);
	
INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-770-604-2000', 'Americans Office', 11);	
	
INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-877-660-8550', 'Reservation', 11);
	
INSERT INTO PhoneInformation (PhoneNumber, Description, ContactID) 
	VALUES ('44 (0) 1895 512097', 'IHG Group Corporate and Financial Communications', 11);
	
INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-770-604-7130', 'IHG Regional Corporate Communications', 11);	
	
INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-602-957-4200', 'Headquarters', 12);
	
INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-800-780-7234', 'Reservations', 12);

INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-604-730-6600', 'Head Office', 13);	
	
INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-800-726-3626', 'Reservations', 13);

-- HotelChain
INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-905-477-4663', NULL, 14);
	
INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-613-270-2050', NULL, 15);
	
INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-613-234-6363', NULL, 16);

INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-613-216-7829', NULL, 17);

INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-412-281-5800', NULL, 18);
	
INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-613-599-7200', NULL, 19);
	
INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-613-237-3600', NULL, 20);
	
INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-613-599-7767', NULL, 21);
	
INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-614-412-7664', NULL, 22);
	
INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-614-798-8652', NULL, 23);

INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-705-765-1900', NULL, 24);	

INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-614-846-4355', NULL, 25);	

INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-613-702-9800', NULL, 26);

INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-613-321-1234', NULL, 27);
	
INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-972-378-3997', NULL, 28);
	
INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-855-501-3247', NULL, 29);
	
INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-613-271-3057', NULL, 30);
	
INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-613-690-0100', NULL, 31);	
	
INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-404-524-7000', NULL, 32);
	
INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-404-949-4000', NULL, 33);
	
INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-416-597-1400', NULL, 34);
	
INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-613-828-2741', NULL, 35);
	
INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-343-417-4561', NULL, 36);	

INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-613-728-1951', NULL, 37);
	
INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-256-236-0503', NULL, 38);
	
INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-304-774-6100', NULL, 39);
	
INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-613-216-7263', NULL, 40);
	
INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-469-535-5100', NULL, 41);
	
INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-403-252-7263', NULL, 42);	

INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-780-430-7263', NULL, 43);
	
INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-450-670-3030', NULL, 44);
	
-- Manager
INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-705-524-5600', NULL, 45);
	
INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-705-897-3422', NULL, 46);
	
INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-705-722-3441', NULL, 47);
	
INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-416-397-3000', NULL, 49);
	
INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-905-525-9140', NULL, 50);

INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-306-743-5430', NULL, 51);
	
INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-905-670-8383', NULL, 52);
	
INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-416-289-5300', NULL, 53);
	
INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-415-705-7800', NULL, 54);
	
INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-212-849-7000', NULL, 55);
	
INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-518-795-1174', NULL, 56);
	
INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-416-585-5000', NULL, 57);
	
INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-405-955-3858', NULL, 58);
	
INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-408-999-5199', NULL, 59);

INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-416-686-8526', NULL, 60);

INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-416-863-1133', NULL, 61);
	
INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-613-828-5158', NULL, 62);
	
-- NonManager
INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-613-723-5136', NULL, 63);
	
INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-405-247-9493', NULL, 64);

INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-613-596-9202', NULL, 65);

INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-905-643-8685', NULL, 66);

INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-613-592-3591', NULL, 67);

INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-613-230-4104', NULL, 68);

INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-608-342-1567', NULL, 69);

INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-514-283-8300', NULL, 70);
	
INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-661-398-7297', NULL, 71);
	
INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-801-355-2705', NULL, 72);

-- Manager
INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-310-442-4000', NULL, 73);	

INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-613-226-2553', NULL, 74);

-- NonManager
INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-385-528-2950', NULL, 75);

INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-617-443-3000', NULL, 76);
	
INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-408-919-0600', NULL, 77);
	
INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-514-954-4000', NULL, 78);
	
-- Manager
INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-613-228-9595', NULL, 79);
	
INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-603-601-8080', NULL, 80);

-- NonManager
INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-403-527-5500', NULL, 81);
	
INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-647-478-0778', NULL, 82);
	
INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-587-364-4995', NULL, 83);
	
INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-403-343-6404', NULL, 84);

-- Manager
INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-403-986-3601', NULL, 85);
	
INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-403-342-2717', NULL, 86);	

INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-780-422-7333', NULL, 87);	

INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-403-727-5055', NULL, 88);	

-- NonManager
INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-403-955-2978', NULL, 89);	

INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-403-457-1900', NULL, 90);	
	
INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-403-956-1359', NULL, 91);	
	
INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-403-944-8225', NULL, 92);

-- Manager
INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-403-943-8430', NULL, 93);
	
INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-403-297-8123', NULL, 94);
	
INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-403-770-3201', NULL, 95);
	
INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-403-944-4763', NULL, 96);

-- NonManager
INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-403-943-4328', NULL, 97);
	
INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-403-454-4158', NULL, 98);

INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-403-299-0600', NULL, 99);

INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-403-685-5333', NULL, 100);
	
-- Manager
INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-403-971-6760', NULL, 101);

INSERT INTO PhoneInformation 
	(PhoneNumber, Description, ContactID) VALUES ('1-403-978-0092', NULL, 102);
	
-- HotelBrand
INSERT INTO EmailInformation 
	(EmailAddress, Description, ContactID) VALUES ('ir@hilton.com', 'Investment Inquiries', 8);
	
INSERT INTO EmailInformation 
	(EmailAddress, Description, ContactID) VALUES ('hiltonpr@hilton.com', 'Media Inquiries', 8);
	
INSERT INTO EmailInformation 
	(EmailAddress, Description, ContactID) VALUES ('Investorrelations@marriott.com', 'Investor Relations', 9);

INSERT INTO EmailInformation 
	(EmailAddress, Description, ContactID) VALUES ('newsroom@marriott.com', 'Media Contacts', 9);
	
INSERT INTO EmailInformation 
	(EmailAddress, Description, ContactID) VALUES ('concierge@Hyatt.com', 'Customer Service', 10);
	
INSERT INTO EmailInformation 
	(EmailAddress, Description, ContactID) VALUES ('hyattgrp@hyatt.com', 'Hyatt National Sales Office', 10);
	
INSERT INTO EmailInformation (EmailAddress, Description, ContactID) 
	VALUES ('mark.debenham@ihg.com', 'IHG Group Corporate and Financial Communications', 11);
	
INSERT INTO EmailInformation (EmailAddress, Description, ContactID) 
	VALUES ('AmericasComms@ihg.com', 'IHG Regional Corporate Communications', 11);	
	
INSERT INTO EmailInformation 
	(EmailAddress, Description, ContactID) VALUES ('mediarequest@bestwestern.com', 'News Media', 12);
	
INSERT INTO EmailInformation 
	(EmailAddress, Description, ContactID) VALUES ('advertisingrequests@bestwestern.com', 'Advertising & Bloggers', 12);

INSERT INTO EmailInformation 
	(EmailAddress, Description, ContactID) VALUES ('info@sandman.ca', 'Head Office', 13);

-- Manager
INSERT INTO EmailInformation 
	(EmailAddress, Description, ContactID) VALUES ('valcaron@yahoo.ca', NULL, 46);

INSERT INTO EmailInformation 
	(EmailAddress, Description, ContactID) VALUES ('collections@hotmail.com', NULL, 47);
	
INSERT INTO EmailInformation 
	(EmailAddress, Description, ContactID) VALUES ('ka@gmail.com', NULL, 48);
	
INSERT INTO EmailInformation 
	(EmailAddress, Description, ContactID) VALUES ('db@yahoo.com', NULL, 49);
	
INSERT INTO EmailInformation 
	(EmailAddress, Description, ContactID) VALUES ('wongkm@yahoo.ca', NULL, 50);
	
INSERT INTO EmailInformation 
	(EmailAddress, Description, ContactID) VALUES ('paragon.ws@sasktel.net', NULL, 51);
	
INSERT INTO EmailInformation 
	(EmailAddress, Description, ContactID) VALUES ('pl@gmail.com', NULL, 52);
	
INSERT INTO EmailInformation 
	(EmailAddress, Description, ContactID) VALUES ('js@hotmail.com', NULL, 53);
	
INSERT INTO EmailInformation 
	(EmailAddress, Description, ContactID) VALUES ('jh@yahoo.ca', NULL, 54);	

INSERT INTO EmailInformation 
	(EmailAddress, Description, ContactID) VALUES ('justineyoung@sasktel.net', NULL, 55);
	
INSERT INTO EmailInformation 
	(EmailAddress, Description, ContactID) VALUES ('seleemcheeks@gmail.com', NULL, 56);
	
INSERT INTO EmailInformation 
	(EmailAddress, Description, ContactID) VALUES ('vgibson@hotmail.com', NULL, 57);

INSERT INTO EmailInformation 
	(EmailAddress, Description, ContactID) VALUES ('sh@yahoo.com', NULL, 58);
	
INSERT INTO EmailInformation 
	(EmailAddress, Description, ContactID) VALUES ('mayho@sasktel.net', NULL, 60);	

INSERT INTO EmailInformation 
	(EmailAddress, Description, ContactID) VALUES ('susanawong@gmail.com', NULL, 61);
	
INSERT INTO EmailInformation 
	(EmailAddress, Description, ContactID) VALUES ('Kim.Midenmeier@hotmail.com', NULL, 62);
	
-- NonManager
INSERT INTO EmailInformation 
	(EmailAddress, Description, ContactID) VALUES ('nikki.farquhar-morton@yahoo.com', NULL, 63);

INSERT INTO EmailInformation 
	(EmailAddress, Description, ContactID) VALUES ('sandy.jay@sasktel.net', NULL, 64);
	
INSERT INTO EmailInformation 
	(EmailAddress, Description, ContactID) VALUES ('peggy.brule@gmail.com', NULL, 65);
	
INSERT INTO EmailInformation 
	(EmailAddress, Description, ContactID) VALUES ('sara.adkins@hotmail.com', NULL, 66);
	
INSERT INTO EmailInformation 
	(EmailAddress, Description, ContactID) VALUES ('tanya.laughlin@yahoo.ca', NULL, 67);
	
INSERT INTO EmailInformation 
	(EmailAddress, Description, ContactID) VALUES ('johanna.marie@sasktel.net', NULL, 68);

INSERT INTO EmailInformation 
	(EmailAddress, Description, ContactID) VALUES ('Mccarvillek@gmail.com', NULL, 69);
	
INSERT INTO EmailInformation 
	(EmailAddress, Description, ContactID) VALUES ('Wildman-Frieseni@hotmail.com', NULL, 70);	
	
INSERT INTO EmailInformation 
	(EmailAddress, Description, ContactID) VALUES ('Mariem@yahoo.com', NULL, 71);	
	
INSERT INTO EmailInformation 
	(EmailAddress, Description, ContactID) VALUES ('Schoenherrb@sasktel.net', NULL, 72);
	
-- Manager
INSERT INTO EmailInformation 
	(EmailAddress, Description, ContactID) VALUES ('Aikenr@gmail.com', NULL, 73);
	
INSERT INTO EmailInformation 
	(EmailAddress, Description, ContactID) VALUES ('Bowesm@gmail.com', NULL, 74);

-- NonManager
INSERT INTO EmailInformation 
	(EmailAddress, Description, ContactID) VALUES ('Dickinsonk@gmail.com', NULL, 75);

INSERT INTO EmailInformation
	(EmailAddress, Description, ContactID) VALUES ('Foren@yahoo.com', NULL, 76);
	
INSERT INTO EmailInformation
	(EmailAddress, Description, ContactID) VALUES ('Mukherjeeb@sasktel.net', NULL, 77);
	
INSERT INTO EmailInformation
	(EmailAddress, Description, ContactID) VALUES ('Larocquec@gmail.com', NULL, 78);		

-- Manager
INSERT INTO EmailInformation
	(EmailAddress, Description, ContactID) VALUES ('Ledainm@yahoo.ca', NULL, 79);
	
INSERT INTO EmailInformation
	(EmailAddress, Description, ContactID) VALUES ('Cosgrove-Boireb@sasktel.net', NULL, 80);
	
-- NonManager
INSERT INTO EmailInformation
	(EmailAddress, Description, ContactID) VALUES ('Fidan@gmail.com', NULL, 81);
	
INSERT INTO EmailInformation
	(EmailAddress, Description, ContactID) VALUES ('Abdurrahmanz@yahoo.com', NULL, 82);
	
INSERT INTO EmailInformation
	(EmailAddress, Description, ContactID) VALUES ('Friesenf@hotmail.com', NULL, 83);
	
INSERT INTO EmailInformation
	(EmailAddress, Description, ContactID) VALUES ('Driedgere@yahoo.ca', NULL, 84);
	
-- Manager
INSERT INTO EmailInformation
	(EmailAddress, Description, ContactID) VALUES ('duPlooym@sasktel.net', NULL, 85);
	
INSERT INTO EmailInformation
	(EmailAddress, Description, ContactID) VALUES ('masoudw@gmail.com', NULL, 86);

INSERT INTO EmailInformation
	(EmailAddress, Description, ContactID) VALUES ('mogust@yahoo.ca', NULL, 87);

INSERT INTO EmailInformation
	(EmailAddress, Description, ContactID) VALUES ('soperk@hotmail.com', NULL, 88);

-- NonManager
INSERT INTO EmailInformation
	(EmailAddress, Description, ContactID) VALUES ('vyvere@sasktel.net', NULL, 89);
	
INSERT INTO EmailInformation
	(EmailAddress, Description, ContactID) VALUES ('alanenk@gmail.com', NULL, 90);	

INSERT INTO EmailInformation
	(EmailAddress, Description, ContactID) VALUES ('andersk@yahoo.com', NULL, 91);	

INSERT INTO EmailInformation
	(EmailAddress, Description, ContactID) VALUES ('aueri@hotmail.com', NULL, 92);
	
-- Manager
INSERT INTO EmailInformation
	(EmailAddress, Description, ContactID) VALUES ('bismart@sasktel.net', NULL, 93);
	
INSERT INTO EmailInformation
	(EmailAddress, Description, ContactID) VALUES ('bole@gmail.com', NULL, 94);
	
INSERT INTO EmailInformation
	(EmailAddress, Description, ContactID) VALUES ('brennt@hotmail.com', NULL, 95);
	
INSERT INTO EmailInformation
	(EmailAddress, Description, ContactID) VALUES ('dugganm@sasktel.net', NULL, 96);

-- NonManager
INSERT INTO EmailInformation
	(EmailAddress, Description, ContactID) VALUES ('gorombeys@yahoo.ca', NULL, 97);
	
INSERT INTO EmailInformation
	(EmailAddress, Description, ContactID) VALUES ('huhnk@hotmail.com', NULL, 98);
	
INSERT INTO EmailInformation
	(EmailAddress, Description, ContactID) VALUES ('jadusinghi@gmail.com', NULL, 99);
	
INSERT INTO EmailInformation
	(EmailAddress, Description, ContactID) VALUES ('lesterw@hotmail.com', NULL, 100);
	
-- Manager
INSERT INTO EmailInformation
	(EmailAddress, Description, ContactID) VALUES ('pinto-rojasa@sasktel.net', NULL, 101);
	
INSERT INTO EmailInformation
	(EmailAddress, Description, ContactID) VALUES ('waghrayr@yahoo.com', NULL, 102);
	

-- Customer	
INSERT INTO Person
    (ContactID, SIN, FirstName, LastName, Password) VALUES (1,'123456778','Benny','Tong',MD5('password'));

INSERT INTO Person
    (ContactID, SIN, FirstName, LastName, Password) VALUES (2,'123456772','Kimberly','Clack',MD5('password2'));

INSERT INTO Person
    (ContactID, SIN, FirstName, LastName, Password) VALUES (3,'123456774','Scarlett','Chan',MD5('abcd'));

-- Manager
INSERT INTO Person
    (ContactID, SIN, FirstName, LastName, Password) VALUES (4,'123456776', 'Gary', 'Wsward', MD5('1234'));
	
INSERT INTO Person
    (ContactID, SIN, FirstName, LastName, Password) VALUES (5, '223456778', 'May', 'Fifty', MD5('MiamiBeach'));

-- Non Manager
INSERT INTO Person
    (ContactID, SIN, FirstName, LastName, Password) VALUES (6, '123456790', 'Katie', 'Parent', MD5('NYC'));
	
INSERT INTO Person
    (ContactID, SIN, FirstName, LastName, Password) VALUES (7, '123456780', 'Ada', 'Ehi', MD5('Los Angeles'));
	
-- Manager	
INSERT INTO Person
    (ContactID, SIN, FirstName, LastName, Password) VALUES (45, '423456778', 'Stephane', 'Villeneuve', MD5('password3'));

INSERT INTO Person
    (ContactID, SIN, FirstName, LastName, Password) VALUES (46, '123456782', 'Miranda', 'Corbett', MD5('xyz'));
	
INSERT INTO Person
    (ContactID, SIN, FirstName, LastName, Password) VALUES (47, '623456778', 'Brent', 'Olsen', MD5('xyzt'));

INSERT INTO Person
    (ContactID, SIN, FirstName, LastName, Password) VALUES (48, '123456784', 'Kenneth', 'Adkin', MD5('password2'));

INSERT INTO Person
    (ContactID, SIN, FirstName, LastName, Password) VALUES (49, '823456786', 'David', 'Bickerton', MD5('password21'));
	
INSERT INTO Person
    (ContactID, SIN, FirstName, LastName, Password) VALUES (50, '123456786', 'Max', 'Wong', MD5('password22'));
	
INSERT INTO Person
    (ContactID, SIN, FirstName, LastName, Password) VALUES (51, '123456788', 'Warren', 'Schappert', MD5('password24'));

INSERT INTO Person
    (ContactID, SIN, FirstName, LastName, Password) VALUES (52, '123456792', 'Peter', 'Lo', MD5('915'));
	
INSERT INTO Person
    (ContactID, SIN, FirstName, LastName, Password) VALUES (53, '123456794', 'Joyce', 'Meyer', MD5('RonD'));
	
INSERT INTO Person
    (ContactID, SIN, FirstName, LastName, Password) VALUES (54, '123456796', 'Jenkin', 'Hui', MD5('CityOfAngels'));

INSERT INTO Person
    (ContactID, SIN, FirstName, LastName, Password) VALUES (55, '123456798', 'Justine', 'Young', MD5('password3'));
	
INSERT INTO Person
    (ContactID, SIN, FirstName, LastName, Password) VALUES (56, '123456800', 'Saleem', 'Cheeks', MD5('password33'));
	
INSERT INTO Person
    (ContactID, SIN, FirstName, LastName, Password) VALUES (57, '123456802', 'Victoria', 'Gibson', MD5('QWS@#FG'));
	
INSERT INTO Person
    (ContactID, SIN, FirstName, LastName, Password) VALUES (58, '123456804', 'Simon', 'Hébert', MD5('711'));
	
INSERT INTO Person
    (ContactID, SIN, FirstName, LastName, Password) VALUES (59, '123456806', 'Kirsten', 'Hanes', MD5('PizzaPizza'));
	
INSERT INTO Person
    (ContactID, SIN, FirstName, LastName, Password) VALUES (60, '123456808', 'May', 'Ho', MD5('BigMac'));
	
INSERT INTO Person
    (ContactID, SIN, FirstName, LastName, Password) VALUES (61, '123456810', 'Susana', 'Wong', MD5('Montreal'));
	
INSERT INTO Person
    (ContactID, SIN, FirstName, LastName, Password) VALUES (62, '123456813', 'Kim', 'Widenmeier', MD5('MetroDeMontreal'));
	
-- NonManager
INSERT INTO Person
    (ContactID, SIN, FirstName, LastName, Password) VALUES (63, '123456816', 'Nikki', 'Farquhar-Morton', MD5('Ottawa'));

INSERT INTO Person
    (ContactID, SIN, FirstName, LastName, Password) VALUES (64, '123456818', 'Sandy', 'Jay', MD5('Texas'));
	
INSERT INTO Person
    (ContactID, SIN, FirstName, LastName, Password) VALUES (65, '123456830', 'Peggy', 'Brule', MD5('Dallas'));
	
INSERT INTO Person
    (ContactID, SIN, FirstName, LastName, Password) VALUES (66, '123456832', 'Sara', 'Adkins', MD5('NorthCarolina'));
	
INSERT INTO Person
    (ContactID, SIN, FirstName, LastName, Password) VALUES (67, '123456834', 'Tanya', 'Laughlin', MD5('Beautiful'));
	
INSERT INTO Person
    (ContactID, SIN, FirstName, LastName, Password) VALUES (68, '123456836', 'Johanna', 'Marie', MD5('password6'));
	
INSERT INTO Person
    (ContactID, SIN, FirstName, LastName, Password) VALUES (69, '123456838', 'Kara', 'McCarville', MD5('password61'));

INSERT INTO Person
    (ContactID, SIN, FirstName, LastName, Password) VALUES (70, '123456840', 'Ingrid', 'Wildman-Friesen', MD5('password62'));
	
INSERT INTO Person
    (ContactID, SIN, FirstName, LastName, Password) VALUES (71, '123456842', 'Missy', 'Marie', MD5('password63'));
	
INSERT INTO Person
    (ContactID, SIN, FirstName, LastName, Password) VALUES (72, '123456844', 'Bradley', 'Schoenherr', MD5('password64'));

-- Manager
INSERT INTO Person
    (ContactID, SIN, FirstName, LastName, Password) VALUES (73, '123456846', 'Rick', 'Aiken', MD5('password71'));
	
INSERT INTO Person
    (ContactID, SIN, FirstName, LastName, Password) VALUES (74, '123456848', 'Megan Ashlee', 'Bowes', MD5('password72'));
	
-- NonManager
INSERT INTO Person
    (ContactID, SIN, FirstName, LastName, Password) VALUES (75, '123456850', 'Kat', 'Dickinson', MD5('password74'));
	
INSERT INTO Person
    (ContactID, SIN, FirstName, LastName, Password) VALUES (76, '123456852', 'Nancy', 'Fore', MD5('password73'));

INSERT INTO Person
    (ContactID, SIN, FirstName, LastName, Password) VALUES (77, '123456854', 'Ballari', 'Mukherjee', MD5('password75'));

INSERT INTO Person
    (ContactID, SIN, FirstName, LastName, Password) VALUES (78, '123456856', 'Cassandra', 'Larocque', MD5('password76'));
	
-- Manager	
INSERT INTO Person
    (ContactID, SIN, FirstName, LastName, Password) VALUES (79, '123456858', 'Mandy', 'Ledain', MD5('password81'));
	
INSERT INTO Person
    (ContactID, SIN, FirstName, LastName, Password) VALUES (80, '123456860', 'Briana', 'Cosgrove-Boire', MD5('password82'));
	
-- NonManager
INSERT INTO Person
    (ContactID, SIN, FirstName, LastName, Password) VALUES (81, '123456862', 'Norina', 'Fida', MD5('password80'));
	
INSERT INTO Person
    (ContactID, SIN, FirstName, LastName, Password) VALUES (82, '123456864', 'Zainab', 'Abdurrahman', MD5('password85'));
	
INSERT INTO Person
    (ContactID, SIN, FirstName, LastName, Password) VALUES (83, '123456866', 'Frank', 'Friesen', MD5('password92'));
	
INSERT INTO Person
    (ContactID, SIN, FirstName, LastName, Password) VALUES (84, '123456868', 'Emmi', 'Driedger', MD5('password99'));
	
-- Manager	
INSERT INTO Person
    (ContactID, SIN, FirstName, LastName, Password) VALUES (85, '123456870', 'Magriet', 'du Plooy', MD5('password98'));
	
INSERT INTO Person
    (ContactID, SIN, FirstName, LastName, Password) VALUES (86, '123456872', 'Waleed', 'Masoud', MD5('password97'));
	
INSERT INTO Person
    (ContactID, SIN, FirstName, LastName, Password) VALUES (87, '123456874', 'Tally Michelle', 'Mogus', MD5('password89'));
	
INSERT INTO Person
    (ContactID, SIN, FirstName, LastName, Password) VALUES (88, '123456876', 'Katie', 'Soper', MD5('password99.9'));
	
-- NonManager															
INSERT INTO Person
    (ContactID, SIN, FirstName, LastName, Password) VALUES (89, '123456878', 'Ellie Elizabeth', 'Vyver', MD5('password100'));
															
INSERT INTO Person
    (ContactID, SIN, FirstName, LastName, Password) VALUES (90, '123456880', 'Ken', 'Alanen', MD5('password101'));

INSERT INTO Person
    (ContactID, SIN, FirstName, LastName, Password) VALUES (91, '123456882', 'Karl', 'Anders', MD5('password102'));

INSERT INTO Person
    (ContactID, SIN, FirstName, LastName, Password) VALUES (92, '123456884', 'Iwona', 'Auer', MD5('password103'));
	
-- Manager
INSERT INTO Person
    (ContactID, SIN, FirstName, LastName, Password) VALUES (93, '223456884', 'Terek', 'Bismar', MD5('password104'));

INSERT INTO Person
    (ContactID, SIN, FirstName, LastName, Password) VALUES (94, '123456886', 'Eric Gerald', 'Bol', MD5('password107'));
	
INSERT INTO Person
    (ContactID, SIN, FirstName, LastName, Password) VALUES (95, '123456888', 'Thomas', 'Brenn', MD5('password106'));

INSERT INTO Person
    (ContactID, SIN, FirstName, LastName, Password) VALUES (96, '123456890', 'Maire A.', 'Duggan', MD5('password108'));

-- NonManager
INSERT INTO Person
    (ContactID, SIN, FirstName, LastName, Password) VALUES (97, '123456892', 'Steve', 'Gorombey', MD5('password105'));
	
INSERT INTO Person
    (ContactID, SIN, FirstName, LastName, Password) VALUES (98, '123456894', 'Karen M', 'Huhn', MD5('password210'));
	
INSERT INTO Person
    (ContactID, SIN, FirstName, LastName, Password) VALUES (99, '123456896', 'Inderman H.', 'Jadusingh', MD5('password410'));
	
INSERT INTO Person
    (ContactID, SIN, FirstName, LastName, Password) VALUES (100, '123456898', 'Wanda M', 'Lester', MD5('password411'));
	
-- Manager
INSERT INTO Person
    (ContactID, SIN, FirstName, LastName, Password) VALUES (101, '123458000', 'Alfredo', 'Pinto-Rojas', MD5('password412'));

INSERT INTO Person
    (ContactID, SIN, FirstName, LastName, Password) VALUES (102, '123458002', 'Ranjit', 'Waghray', MD5('password414'));
	
INSERT INTO Customer (PersonContactID, DateOfRegistration) VALUES (1, 'March 6, 2021');
INSERT INTO Customer (PersonContactID, DateOfRegistration) VALUES (2, 'March 7, 2021');
INSERT INTO Customer (PersonContactID, DateOfRegistration) VALUES (3, 'March 6, 2020');
INSERT INTO Customer (PersonContactID, DateOfRegistration) VALUES (4, 'March 4, 2020');
INSERT INTO Customer (PersonContactID, DateOfRegistration) VALUES (5, 'March 2, 2020');
INSERT INTO Customer (PersonContactID, DateOfRegistration) VALUES (6, 'April 6, 2020');
INSERT INTO Customer (PersonContactID, DateOfRegistration) VALUES (7, 'May 6, 2020');
INSERT INTO Customer (PersonContactID, DateOfRegistration) VALUES (45, 'June 6, 2020');
INSERT INTO Customer (PersonContactID, DateOfRegistration) VALUES (46, 'July 6, 2020');
INSERT INTO Customer (PersonContactID, DateOfRegistration) VALUES (47, 'August 6, 2020');
INSERT INTO Customer (PersonContactID, DateOfRegistration) VALUES (48, 'September 6, 2020');
INSERT INTO Customer (PersonContactID, DateOfRegistration) VALUES (49, 'October 6, 2020');
INSERT INTO Customer (PersonContactID, DateOfRegistration) VALUES (50, 'November 6, 2020');
INSERT INTO Customer (PersonContactID, DateOfRegistration) VALUES (51, 'December 6, 2020');

-- Manager
INSERT INTO Employee (PersonContactID, Salary, DateOfHire, DateOfTermination) VALUES (4, 100000.00, 'March 4, 2010', NULL);
INSERT INTO Employee (PersonContactID, Salary, DateOfHire, DateOfTermination) VALUES (5, 120000.00, 'April 6, 2000', NULL);

-- NonManager
INSERT INTO Employee (PersonContactID, Salary, DateOfHire, DateOfTermination) VALUES (6, 80000.00, 'May 8, 2016', NULL);

INSERT INTO Employee (PersonContactID, Salary, DateOfHire, DateOfTermination) VALUES (7, 60000.00, 'June 10, 2019', NULL);

-- Manager
INSERT INTO Employee (PersonContactID, Salary, DateOfHire, DateOfTermination) VALUES (45, 80000.00, 'June 12, 2019', NULL);
INSERT INTO Employee (PersonContactID, Salary, DateOfHire, DateOfTermination) VALUES (46, 100000.00, 'June 14, 2019', NULL);
INSERT INTO Employee (PersonContactID, Salary, DateOfHire, DateOfTermination) VALUES (47, 120000.00, 'June 16, 2019', NULL);
INSERT INTO Employee (PersonContactID, Salary, DateOfHire, DateOfTermination) VALUES (48, 140000.00, 'June 18, 2019', NULL);
INSERT INTO Employee (PersonContactID, Salary, DateOfHire, DateOfTermination) VALUES (49, 160000.00, 'June 20, 2019', NULL);
INSERT INTO Employee (PersonContactID, Salary, DateOfHire, DateOfTermination) VALUES (50, 180000.00, 'June 22, 2019', NULL);
INSERT INTO Employee (PersonContactID, Salary, DateOfHire, DateOfTermination) VALUES (51, 200000.00, 'June 24, 2019', NULL);
INSERT INTO Employee (PersonContactID, Salary, DateOfHire, DateOfTermination) VALUES (52, 220000.00, 'June 26, 2019', NULL);
INSERT INTO Employee (PersonContactID, Salary, DateOfHire, DateOfTermination) VALUES (53, 240000.00, 'June 28, 2019', NULL);
INSERT INTO Employee (PersonContactID, Salary, DateOfHire, DateOfTermination) VALUES (54, 260000.00, 'June 30, 2019', NULL);
INSERT INTO Employee (PersonContactID, Salary, DateOfHire, DateOfTermination) VALUES (55, 82000.00, 'July 12, 2019', NULL);
INSERT INTO Employee (PersonContactID, Salary, DateOfHire, DateOfTermination) VALUES (56, 102000.00, 'July 14, 2019', NULL);
INSERT INTO Employee (PersonContactID, Salary, DateOfHire, DateOfTermination) VALUES (57, 104000.00, 'July 16, 2019', NULL);
INSERT INTO Employee (PersonContactID, Salary, DateOfHire, DateOfTermination) VALUES (58, 106000.00, 'July 18, 2019', NULL);
INSERT INTO Employee (PersonContactID, Salary, DateOfHire, DateOfTermination) VALUES (59, 108000.00, 'July 20, 2019', NULL);
INSERT INTO Employee (PersonContactID, Salary, DateOfHire, DateOfTermination) VALUES (60, 128000.00, 'July 22, 2019', NULL);
INSERT INTO Employee (PersonContactID, Salary, DateOfHire, DateOfTermination) VALUES (61, 128002.00, 'July 24, 2019', NULL);
INSERT INTO Employee (PersonContactID, Salary, DateOfHire, DateOfTermination) VALUES (62, 128004.00, 'July 26, 2019', NULL);

-- NonManager
INSERT INTO Employee (PersonContactID, Salary, DateOfHire, DateOfTermination) VALUES (63, 28004.00, 'July 26, 2018', NULL);
INSERT INTO Employee (PersonContactID, Salary, DateOfHire, DateOfTermination) VALUES (64, 28006.00, 'July 28, 2018', NULL);
INSERT INTO Employee (PersonContactID, Salary, DateOfHire, DateOfTermination) VALUES (65, 28008.00, 'July 30, 2018', NULL);
INSERT INTO Employee (PersonContactID, Salary, DateOfHire, DateOfTermination) VALUES (66, 28008.00, 'August 30, 2018', NULL);
INSERT INTO Employee (PersonContactID, Salary, DateOfHire, DateOfTermination) VALUES (67, 28008.00, 'September 2, 2018', NULL);
INSERT INTO Employee (PersonContactID, Salary, DateOfHire, DateOfTermination) VALUES (68, 28008.00, 'September 4, 2018', NULL);
INSERT INTO Employee (PersonContactID, Salary, DateOfHire, DateOfTermination) VALUES (69, 28020.00, 'September 6, 2018', NULL);
INSERT INTO Employee (PersonContactID, Salary, DateOfHire, DateOfTermination) VALUES (70, 28022.00, 'September 8, 2018', NULL);
INSERT INTO Employee (PersonContactID, Salary, DateOfHire, DateOfTermination) VALUES (71, 28024.00, 'September 10, 2018', NULL);
INSERT INTO Employee (PersonContactID, Salary, DateOfHire, DateOfTermination) VALUES (72, 28026.00, 'September 12, 2018', NULL);

-- Manager
INSERT INTO Employee (PersonContactID, Salary, DateOfHire, DateOfTermination) VALUES (73, 88026.00, 'September 14, 2018', NULL);
INSERT INTO Employee (PersonContactID, Salary, DateOfHire, DateOfTermination) VALUES (74, 128909.06, 'September 14, 2017', NULL);

-- NonManager
INSERT INTO Employee (PersonContactID, Salary, DateOfHire, DateOfTermination) VALUES (75, 28909.06, 'October 14, 2017', NULL);
INSERT INTO Employee (PersonContactID, Salary, DateOfHire, DateOfTermination) VALUES (76, 28909.08, 'October 16, 2017', NULL);
INSERT INTO Employee (PersonContactID, Salary, DateOfHire, DateOfTermination) VALUES (77, 28909.10, 'October 18, 2017', NULL);
INSERT INTO Employee (PersonContactID, Salary, DateOfHire, DateOfTermination) VALUES (78, 28909.12, 'October 20, 2017', NULL);

-- Manager
INSERT INTO Employee (PersonContactID, Salary, DateOfHire, DateOfTermination) VALUES (79, 128909.12, 'November 20, 2017', NULL);
INSERT INTO Employee (PersonContactID, Salary, DateOfHire, DateOfTermination) VALUES (80, 148909.12, 'November 22, 2017', NULL);

-- NonManager
INSERT INTO Employee (PersonContactID, Salary, DateOfHire, DateOfTermination) VALUES (81, 48909.12, 'November 24, 2017', NULL);
INSERT INTO Employee (PersonContactID, Salary, DateOfHire, DateOfTermination) VALUES (82, 46909.12, 'November 26, 2017', NULL);
INSERT INTO Employee (PersonContactID, Salary, DateOfHire, DateOfTermination) VALUES (83, 46929.12, 'November 28, 2017', NULL);
INSERT INTO Employee (PersonContactID, Salary, DateOfHire, DateOfTermination) VALUES (84, 46949.12, 'November 30, 2017', NULL);

-- Manager
INSERT INTO Employee (PersonContactID, Salary, DateOfHire, DateOfTermination) VALUES (85, 146909.12, 'December 1, 2017', NULL);
INSERT INTO Employee (PersonContactID, Salary, DateOfHire, DateOfTermination) VALUES (86, 166909.12, 'November 26, 2016', NULL);
INSERT INTO Employee (PersonContactID, Salary, DateOfHire, DateOfTermination) VALUES (87, 166920.12, 'November 28, 2016', NULL);
INSERT INTO Employee (PersonContactID, Salary, DateOfHire, DateOfTermination) VALUES (88, 166922.12, 'November 30, 2016', NULL);

-- NonManager
INSERT INTO Employee (PersonContactID, Salary, DateOfHire, DateOfTermination) VALUES (89, 66909.12, 'December 2, 2016', NULL);
INSERT INTO Employee (PersonContactID, Salary, DateOfHire, DateOfTermination) VALUES (90, 46909.12, 'December 4, 2016', NULL);
INSERT INTO Employee (PersonContactID, Salary, DateOfHire, DateOfTermination) VALUES (91, 46909.14, 'December 6, 2016', NULL);
INSERT INTO Employee (PersonContactID, Salary, DateOfHire, DateOfTermination) VALUES (92, 46909.16, 'December 8, 2016', NULL);

-- Manager
INSERT INTO Employee (PersonContactID, Salary, DateOfHire, DateOfTermination) VALUES (93, 146909.16, 'December 10, 2016', NULL);
INSERT INTO Employee (PersonContactID, Salary, DateOfHire, DateOfTermination) VALUES (94, 66909.16, 'December 12, 2016', NULL);

INSERT INTO Employee (PersonContactID, Salary, DateOfHire, DateOfTermination) 
	VALUES (95, 86909.16, 'December 14, 2016', NULL);

INSERT INTO Employee (PersonContactID, Salary, DateOfHire, DateOfTermination) 
	VALUES (96, 88909.16, 'December 16, 2016', 'January 18, 2018');

-- NonManager
INSERT INTO Employee (PersonContactID, Salary, DateOfHire, DateOfTermination) 
	VALUES (97, 46909.16, 'December 18, 2016', NULL);

INSERT INTO Employee (PersonContactID, Salary, DateOfHire, DateOfTermination) 
	VALUES (98, 66909.16, 'December 20, 2016', NULL);

INSERT INTO Employee (PersonContactID, Salary, DateOfHire, DateOfTermination) 
	VALUES (99, 66909.20, 'December 22, 2018', NULL);
	
INSERT INTO Employee (PersonContactID, Salary, DateOfHire, DateOfTermination) 
	VALUES (100, 66909.40, 'December 23, 2018', NULL);
	
-- Manager
INSERT INTO Employee (PersonContactID, Salary, DateOfHire, DateOfTermination) 
	VALUES (101, 166909.16, 'December 24, 2018', NULL);

INSERT INTO Employee (PersonContactID, Salary, DateOfHire, DateOfTermination) 
	VALUES (102, 146909.16, 'January 20, 2016', 'January 20, 2018');

INSERT INTO Manager (EmployeeContactID, IsAnEmergencyContact) VALUES (4, TRUE);
INSERT INTO Manager (EmployeeContactID, IsAnEmergencyContact) VALUES (5, FALSE);
INSERT INTO Manager (EmployeeContactID, IsAnEmergencyContact) VALUES (45, FALSE);
INSERT INTO Manager (EmployeeContactID, IsAnEmergencyContact) VALUES (46, FALSE);
INSERT INTO Manager (EmployeeContactID, IsAnEmergencyContact) VALUES (47, FALSE);
INSERT INTO Manager (EmployeeContactID, IsAnEmergencyContact) VALUES (48, FALSE);
INSERT INTO Manager (EmployeeContactID, IsAnEmergencyContact) VALUES (49, FALSE);
INSERT INTO Manager (EmployeeContactID, IsAnEmergencyContact) VALUES (50, FALSE);
INSERT INTO Manager (EmployeeContactID, IsAnEmergencyContact) VALUES (51, FALSE);
INSERT INTO Manager (EmployeeContactID, IsAnEmergencyContact) VALUES (52, FALSE);
INSERT INTO Manager (EmployeeContactID, IsAnEmergencyContact) VALUES (53, FALSE);
INSERT INTO Manager (EmployeeContactID, IsAnEmergencyContact) VALUES (54, FALSE);
INSERT INTO Manager (EmployeeContactID, IsAnEmergencyContact) VALUES (55, FALSE);
INSERT INTO Manager (EmployeeContactID, IsAnEmergencyContact) VALUES (56, FALSE);
INSERT INTO Manager (EmployeeContactID, IsAnEmergencyContact) VALUES (57, FALSE);
INSERT INTO Manager (EmployeeContactID, IsAnEmergencyContact) VALUES (58, TRUE);
INSERT INTO Manager (EmployeeContactID, IsAnEmergencyContact) VALUES (59, FALSE);
INSERT INTO Manager (EmployeeContactID, IsAnEmergencyContact) VALUES (60, TRUE);
INSERT INTO Manager (EmployeeContactID, IsAnEmergencyContact) VALUES (61, FALSE);
INSERT INTO Manager (EmployeeContactID, IsAnEmergencyContact) VALUES (62, TRUE);
INSERT INTO Manager (EmployeeContactID, IsAnEmergencyContact) VALUES (73, TRUE);
INSERT INTO Manager (EmployeeContactID, IsAnEmergencyContact) VALUES (74, FALSE);
INSERT INTO Manager (EmployeeContactID, IsAnEmergencyContact) VALUES (79, FALSE);
INSERT INTO Manager (EmployeeContactID, IsAnEmergencyContact) VALUES (80, FALSE);
INSERT INTO Manager (EmployeeContactID, IsAnEmergencyContact) VALUES (85, FALSE);
INSERT INTO Manager (EmployeeContactID, IsAnEmergencyContact) VALUES (86, TRUE);
INSERT INTO Manager (EmployeeContactID, IsAnEmergencyContact) VALUES (87, TRUE);
INSERT INTO Manager (EmployeeContactID, IsAnEmergencyContact) VALUES (88, FALSE);
INSERT INTO Manager (EmployeeContactID, IsAnEmergencyContact) VALUES (93, TRUE);
INSERT INTO Manager (EmployeeContactID, IsAnEmergencyContact) VALUES (94, TRUE);
INSERT INTO Manager (EmployeeContactID, IsAnEmergencyContact) VALUES (95, TRUE);
INSERT INTO Manager (EmployeeContactID, IsAnEmergencyContact) VALUES (96, FALSE);
INSERT INTO Manager (EmployeeContactID, IsAnEmergencyContact) VALUES (101, FALSE);
INSERT INTO Manager (EmployeeContactID, IsAnEmergencyContact) VALUES (102, TRUE);


INSERT INTO NonManager (EmployeeContactID, EvaluationRating) VALUES (6, 8);
INSERT INTO NonManager (EmployeeContactID, EvaluationRating) VALUES (7, 7);
INSERT INTO NonManager (EmployeeContactID, EvaluationRating) VALUES (63, 2);
INSERT INTO NonManager (EmployeeContactID, EvaluationRating) VALUES (64, 4);
INSERT INTO NonManager (EmployeeContactID, EvaluationRating) VALUES (65, 6);
INSERT INTO NonManager (EmployeeContactID, EvaluationRating) VALUES (66, 8);
INSERT INTO NonManager (EmployeeContactID, EvaluationRating) VALUES (67, 10);
INSERT INTO NonManager (EmployeeContactID, EvaluationRating) VALUES (68, 1);
INSERT INTO NonManager (EmployeeContactID, EvaluationRating) VALUES (69, 2);
INSERT INTO NonManager (EmployeeContactID, EvaluationRating) VALUES (70, 3);
INSERT INTO NonManager (EmployeeContactID, EvaluationRating) VALUES (71, 4);
INSERT INTO NonManager (EmployeeContactID, EvaluationRating) VALUES (72, 4);
INSERT INTO NonManager (EmployeeContactID, EvaluationRating) VALUES (75, 3);
INSERT INTO NonManager (EmployeeContactID, EvaluationRating) VALUES (76, 4);
INSERT INTO NonManager (EmployeeContactID, EvaluationRating) VALUES (77, 5);
INSERT INTO NonManager (EmployeeContactID, EvaluationRating) VALUES (78, 6);
INSERT INTO NonManager (EmployeeContactID, EvaluationRating) VALUES (81, 5);
INSERT INTO NonManager (EmployeeContactID, EvaluationRating) VALUES (82, 4);
INSERT INTO NonManager (EmployeeContactID, EvaluationRating) VALUES (83, 5);
INSERT INTO NonManager (EmployeeContactID, EvaluationRating) VALUES (84, 6);
INSERT INTO NonManager (EmployeeContactID, EvaluationRating) VALUES (89, 6);
INSERT INTO NonManager (EmployeeContactID, EvaluationRating) VALUES (90, 7);
INSERT INTO NonManager (EmployeeContactID, EvaluationRating) VALUES (91, 7);
INSERT INTO NonManager (EmployeeContactID, EvaluationRating) VALUES (92, 8);
INSERT INTO NonManager (EmployeeContactID, EvaluationRating) VALUES (97, 8);
INSERT INTO NonManager (EmployeeContactID, EvaluationRating) VALUES (98, 9);
INSERT INTO NonManager (EmployeeContactID, EvaluationRating) VALUES (99, 10);
INSERT INTO NonManager (EmployeeContactID, EvaluationRating) VALUES (100, 10);


INSERT INTO HotelBrand (ContactID, BrandName) VALUES (8, 'Hilton Worldwide Holdings Inc.');
INSERT INTO HotelBrand (ContactID, BrandName) VALUES (9, 'Marriott International Inc.');
INSERT INTO HotelBrand (ContactID, BrandName) VALUES (10, 'Hyatt Hotels Corporation');
INSERT INTO HotelBrand (ContactID, BrandName) VALUES (11, 'InterContinental Hotels Group plc');
INSERT INTO HotelBrand (ContactID, BrandName) VALUES (12, 'Best Western International, Inc.');
INSERT INTO HotelBrand (ContactID, BrandName) VALUES (13, 'Sandman Hotel Group');

-- Hilton
INSERT INTO HotelChain (ContactID, StarCategory, HotelBrandContactID, ManagerContactID) VALUES (14, 3, 8, 45);
INSERT INTO HotelChain (ContactID, StarCategory, HotelBrandContactID, ManagerContactID) VALUES (15, 3, 8, 46);
INSERT INTO HotelChain (ContactID, StarCategory, HotelBrandContactID, ManagerContactID) VALUES (16, 4, 8, 47);
INSERT INTO HotelChain (ContactID, StarCategory, HotelBrandContactID, ManagerContactID) VALUES (17, 2, 8, 48);
INSERT INTO HotelChain (ContactID, StarCategory, HotelBrandContactID, ManagerContactID) VALUES (18, 3, 8, 49);

-- Marriott
INSERT INTO HotelChain (ContactID, StarCategory, HotelBrandContactID, ManagerContactID) VALUES (19, 3, 9, 50);
INSERT INTO HotelChain (ContactID, StarCategory, HotelBrandContactID, ManagerContactID) VALUES (20, 4, 9, 51);
INSERT INTO HotelChain (ContactID, StarCategory, HotelBrandContactID, ManagerContactID) VALUES (21, 3, 9, 52);
INSERT INTO HotelChain (ContactID, StarCategory, HotelBrandContactID, ManagerContactID) VALUES (22, 4, 9, 53);
INSERT INTO HotelChain (ContactID, StarCategory, HotelBrandContactID, ManagerContactID) VALUES (23, 4, 9, 54);
INSERT INTO HotelChain (ContactID, StarCategory, HotelBrandContactID, ManagerContactID) VALUES (24, 5, 9, 55);

-- Hyatt
INSERT INTO HotelChain (ContactID, StarCategory, HotelBrandContactID, ManagerContactID) VALUES (25, 3, 10, 56);
INSERT INTO HotelChain (ContactID, StarCategory, HotelBrandContactID, ManagerContactID) VALUES (26, 3, 10, 57);
INSERT INTO HotelChain (ContactID, StarCategory, HotelBrandContactID, ManagerContactID) VALUES (27, 4, 10, 58);
INSERT INTO HotelChain (ContactID, StarCategory, HotelBrandContactID, ManagerContactID) VALUES (28, 3, 10, 59);
INSERT INTO HotelChain (ContactID, StarCategory, HotelBrandContactID, ManagerContactID) VALUES (29, 5, 10, 60);

-- IHG
INSERT INTO HotelChain (ContactID, StarCategory, HotelBrandContactID, ManagerContactID) VALUES (30, 1, 11, 4);
INSERT INTO HotelChain (ContactID, StarCategory, HotelBrandContactID, ManagerContactID) VALUES (31, 1, 11, 5);
INSERT INTO HotelChain (ContactID, StarCategory, HotelBrandContactID, ManagerContactID) VALUES (32, 3, 11, 61);
INSERT INTO HotelChain (ContactID, StarCategory, HotelBrandContactID, ManagerContactID) VALUES (33, 3, 11, 62);
INSERT INTO HotelChain (ContactID, StarCategory, HotelBrandContactID, ManagerContactID) VALUES (34, 4, 11, 73);

-- Best Western
INSERT INTO HotelChain (ContactID, StarCategory, HotelBrandContactID, ManagerContactID) VALUES (35, 3, 12, 74);
INSERT INTO HotelChain (ContactID, StarCategory, HotelBrandContactID, ManagerContactID) VALUES (36, 3, 12, 79);
INSERT INTO HotelChain (ContactID, StarCategory, HotelBrandContactID, ManagerContactID) VALUES (37, 3, 12, 80);
INSERT INTO HotelChain (ContactID, StarCategory, HotelBrandContactID, ManagerContactID) VALUES (38, 2, 12, 85);
INSERT INTO HotelChain (ContactID, StarCategory, HotelBrandContactID, ManagerContactID) VALUES (39, 4, 12, 86);

-- Sandman
INSERT INTO HotelChain (ContactID, StarCategory, HotelBrandContactID, ManagerContactID) VALUES (40, 3, 13, 87);
INSERT INTO HotelChain (ContactID, StarCategory, HotelBrandContactID, ManagerContactID) VALUES (41, 3, 13, 88);
INSERT INTO HotelChain (ContactID, StarCategory, HotelBrandContactID, ManagerContactID) VALUES (42, 3, 13, 93);
INSERT INTO HotelChain (ContactID, StarCategory, HotelBrandContactID, ManagerContactID) VALUES (43, 4, 13, 94);
INSERT INTO HotelChain (ContactID, StarCategory, HotelBrandContactID, ManagerContactID) VALUES (44, 5, 13, 95);

INSERT INTO RoomType (RoomTypeID, Price, Capacity, View, Extendable) VALUES (1, 200.00, 2, 'Sea', TRUE);
INSERT INTO RoomType (RoomTypeID, Price, Capacity, View, Extendable) VALUES (2, 400.00, 2, 'Sea', TRUE);
INSERT INTO RoomType (RoomTypeID, Price, Capacity, View, Extendable) VALUES (3, 400.00, 4, 'Sea', TRUE);
INSERT INTO RoomType (RoomTypeID, Price, Capacity, View, Extendable) VALUES (4, 400.00, 4, 'Mountain', TRUE);
INSERT INTO RoomType (RoomTypeID, Price, Capacity, View, Extendable) VALUES (5, 400.00, 4, 'Mountain', FALSE);
INSERT INTO RoomType (RoomTypeID, Price, Capacity, View, Extendable) VALUES (6, 380.00, 4, 'Mountain', FALSE);
INSERT INTO RoomType (RoomTypeID, Price, Capacity, View, Extendable) VALUES (7, 500.00, 1, 'Sea', FALSE);
INSERT INTO RoomType (RoomTypeID, Price, Capacity, View, Extendable) VALUES (8, 380.00, 2, 'Mountain', TRUE);
INSERT INTO RoomType (RoomTypeID, Price, Capacity, View, Extendable) VALUES (9, 380.00, 3, 'Sea', FALSE);
INSERT INTO RoomType (RoomTypeID, Price, Capacity, View, Extendable) VALUES (10, 380.00, 5, 'Mountain', FALSE);

INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (14, 100, 1);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (14, 101, 1);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (14, 102, 2);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (14, 103, 2);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (14, 104, 3);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (14, 105, 7);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (14, 106, 8);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (14, 107, 9);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (14, 108, 10);

INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (15, 100, 1);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (15, 101, 1);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (15, 102, 2);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (15, 103, 2);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (15, 104, 3);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (15, 105, 7);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (15, 106, 8);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (15, 107, 9);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (15, 108, 10);


INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (16, 200, 1);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (16, 201, 1);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (16, 202, 2);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (16, 203, 2);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (16, 204, 3);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (16, 104, 7);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (16, 205, 8);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (16, 206, 9);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (16, 207, 10);


INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (17, 100, 3);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (17, 101, 3);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (17, 102, 4);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (17, 103, 4);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (17, 104, 1);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (17, 105, 7);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (17, 106, 8);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (17, 107, 9);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (17, 108, 10);


INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (18, 110, 3);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (18, 111, 3);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (18, 112, 4);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (18, 113, 4);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (18, 114, 1);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (18, 124, 7);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (18, 134, 8);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (18, 144, 9);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (18, 154, 10);


INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (19, 100, 4);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (19, 101, 4);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (19, 102, 4);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (19, 103, 4);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (19, 104, 2);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (19, 105, 7);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (19, 106, 8);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (19, 107, 9);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (19, 108, 10);


INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (20, 100, 3);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (20, 101, 3);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (20, 102, 4);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (20, 103, 4);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (20, 104, 1);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (20, 105, 7);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (20, 106, 8);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (20, 107, 9);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (20, 108, 10);

INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (21, 101, 3);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (21, 102, 3);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (21, 103, 4);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (21, 104, 4);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (21, 106, 1);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (21, 107, 7);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (21, 108, 8);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (21, 109, 9);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (21, 206, 10);


INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (22, 100, 4);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (22, 101, 4);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (22, 102, 5);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (22, 103, 5);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (22, 104, 2);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (22, 105, 7);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (22, 106, 8);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (22, 107, 9);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (22, 108, 10);

																			
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (23, 100, 4);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (23, 101, 4);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (23, 102, 5);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (23, 103, 5);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (23, 104, 2);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (23, 105, 7);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (23, 106, 8);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (23, 107, 9);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (23, 108, 10);

																			
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (24, 400, 4);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (24, 501, 4);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (24, 602, 5);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (24, 703, 5);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (24, 804, 2);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (24, 805, 7);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (24, 806, 8);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (24, 807, 9);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (24, 808, 10);

																			
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (25, 100, 5);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (25, 101, 5);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (25, 102, 5);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (25, 103, 5);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (25, 104, 5);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (25, 105, 7);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (25, 106, 8);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (25, 107, 9);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (25, 108, 10);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (25, 109, 1);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (25, 110, 2);

INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (26, 140, 4);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (26, 151, 4);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (26, 162, 5);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (26, 173, 5);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (26, 184, 2);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (26, 185, 7);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (26, 186, 8);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (26, 187, 9);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (26, 188, 10);


INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (27, 100, 5);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (27, 101, 6);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (27, 102, 6);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (27, 103, 5);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (27, 104, 5);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (27, 105, 7);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (27, 106, 8);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (27, 107, 9);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (27, 108, 10);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (27, 109, 2);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (27, 204, 3);



INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (28, 100, 5);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (28, 101, 6);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (28, 102, 6);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (28, 103, 5);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (28, 104, 5);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (28, 105, 7);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (28, 106, 8);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (28, 107, 9);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (28, 108, 10);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (28, 109, 1);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (28, 114, 2);

																			
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (29, 600, 5);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (29, 601, 6);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (29, 602, 6);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (29, 603, 5);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (29, 604, 5);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (29, 605, 7);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (29, 606, 8);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (29, 607, 9);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (29, 608, 10);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (29, 609, 1);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (29, 610, 1);


INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (30, 600, 1);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (30, 601, 2);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (30, 602, 3);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (30, 603, 4);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (30, 604, 5);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (30, 605, 7);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (30, 606, 8);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (30, 607, 9);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (30, 608, 10);

INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (31, 610, 1);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (31, 611, 2);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (31, 612, 3);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (31, 613, 4);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (31, 614, 5);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (31, 615, 7);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (31, 616, 8);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (31, 617, 9);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (31, 618, 10);


INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (32, 600, 2);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (32, 601, 2);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (32, 602, 3);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (32, 603, 4);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (32, 604, 5);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (32, 605, 7);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (32, 606, 8);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (32, 607, 9);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (32, 608, 10);

INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (33, 606, 1);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (33, 607, 2);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (33, 608, 3);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (33, 609, 4);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (33, 610, 5);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (33, 611, 10);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (33, 612, 9);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (33, 613, 8);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (33, 614, 7);

INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (34, 600, 1);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (34, 601, 2);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (34, 602, 3);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (34, 603, 3);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (34, 604, 3);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (34, 605, 7);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (34, 606, 8);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (34, 607, 9);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (34, 608, 10);


INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (35, 60, 1);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (35, 61, 2);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (35, 62, 3);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (35, 63, 4);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (35, 64, 5);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (35, 66, 7);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (35, 67, 8);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (35, 68, 9);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (35, 69, 10);

INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (36, 600, 4);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (36, 601, 4);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (36, 602, 4);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (36, 603, 4);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (36, 604, 5);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (36, 605, 5);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (36, 606, 2);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (36, 607, 7);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (36, 608, 8);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (36, 609, 9);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (36, 614, 10);


INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (37, 602, 1);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (37, 603, 2);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (37, 604, 3);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (37, 605, 4);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (37, 606, 5);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (37, 607, 7);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (37, 608, 8);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (37, 609, 9);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (37, 706, 10);

INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (38, 600, 6);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (38, 601, 6);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (38, 602, 6);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (38, 603, 4);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (38, 604, 5);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (38, 605, 5);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (38, 606, 2);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (38, 607, 7);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (38, 608, 8);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (38, 609, 9);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (38, 804, 10);


INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (39, 602, 1);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (39, 604, 2);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (39, 606, 3);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (39, 608, 4);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (39, 610, 5);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (39, 611, 7);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (39, 612, 8);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (39, 613, 9);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (39, 614, 10);


INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (40, 600, 2);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (40, 601, 2);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (40, 602, 4);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (40, 603, 4);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (40, 604, 6);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (40, 605, 7);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (40, 606, 8);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (40, 607, 9);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (40, 608, 10);

INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (41, 600, 2);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (41, 601, 2);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (41, 602, 4);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (41, 603, 4);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (41, 604, 6);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (41, 605, 7);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (41, 606, 8);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (41, 607, 9);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (41, 608, 10);


INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (42, 5000, 2);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (42, 5001, 2);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (42, 5002, 4);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (42, 5003, 4);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (42, 5004, 6);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (42, 5005, 7);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (42, 5006, 8);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (42, 5007, 9);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (42, 5008, 10);

INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (43, 600, 3);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (43, 601, 3);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (43, 602, 4);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (43, 603, 4);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (43, 604, 1);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (43, 605, 7);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (43, 606, 8);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (43, 607, 9);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (43, 608, 10);

INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (44, 800, 2);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (44, 801, 3);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (44, 802, 4);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (44, 803, 5);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (44, 804, 6);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (44, 805, 7);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (44, 806, 8);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (44, 807, 9);
INSERT INTO HotelRoom (HotelChainContactID, RoomNumber, RoomTypeID) VALUES (44, 808, 10);

INSERT INTO RoomTypeAmenity (RoomTypeID, Amenity) VALUES (1, 'Fitness center');
INSERT INTO RoomTypeAmenity (RoomTypeID, Amenity) VALUES (1, 'Free WiFi');
INSERT INTO RoomTypeAmenity (RoomTypeID, Amenity) VALUES (1, 'Business center');
INSERT INTO RoomTypeAmenity (RoomTypeID, Amenity) VALUES (1, 'Indoor pool');
INSERT INTO RoomTypeAmenity (RoomTypeID, Amenity) VALUES (2, 'Fitness center');
INSERT INTO RoomTypeAmenity (RoomTypeID, Amenity) VALUES (3, 'Free WiFi');
INSERT INTO RoomTypeAmenity (RoomTypeID, Amenity) VALUES (4, 'Business center');
INSERT INTO RoomTypeAmenity (RoomTypeID, Amenity) VALUES (5, 'Indoor pool');
INSERT INTO RoomTypeAmenity (RoomTypeID, Amenity) VALUES (6, 'Fitness center');
INSERT INTO RoomTypeAmenity (RoomTypeID, Amenity) VALUES (6, 'Free WiFi');
INSERT INTO RoomTypeAmenity (RoomTypeID, Amenity) VALUES (7, 'Free WiFi');
INSERT INTO RoomTypeAmenity (RoomTypeID, Amenity) VALUES (8, 'Free WiFi');
INSERT INTO RoomTypeAmenity (RoomTypeID, Amenity) VALUES (9, 'Free WiFi');

INSERT INTO worksFor(HotelChainContactID, NonManagerContactID, Position) VALUES (14, 6, 'concierge');
INSERT INTO worksFor(HotelChainContactID, NonManagerContactID, Position) VALUES (14, 7, 'housekeeper');
INSERT INTO worksFor(HotelChainContactID, NonManagerContactID, Position) VALUES (15, 63, 'concierge');
INSERT INTO worksFor(HotelChainContactID, NonManagerContactID, Position) VALUES (15, 64, 'housekeeper');
INSERT INTO worksFor(HotelChainContactID, NonManagerContactID, Position) VALUES (16, 65, 'concierge');
INSERT INTO worksFor(HotelChainContactID, NonManagerContactID, Position) VALUES (16, 66, 'housekeeper');
INSERT INTO worksFor(HotelChainContactID, NonManagerContactID, Position) VALUES (17, 68, 'housekeeper');
INSERT INTO worksFor(HotelChainContactID, NonManagerContactID, Position) VALUES (18, 69, 'concierge');
INSERT INTO worksFor(HotelChainContactID, NonManagerContactID, Position) VALUES (18, 70, 'housekeeper');
INSERT INTO worksFor(HotelChainContactID, NonManagerContactID, Position) VALUES (19, 71, 'concierge');
INSERT INTO worksFor(HotelChainContactID, NonManagerContactID, Position) VALUES (19, 72, 'housekeeper');
INSERT INTO worksFor(HotelChainContactID, NonManagerContactID, Position) VALUES (20, 75, 'concierge');
INSERT INTO worksFor(HotelChainContactID, NonManagerContactID, Position) VALUES (20, 76, 'housekeeper');
INSERT INTO worksFor(HotelChainContactID, NonManagerContactID, Position) VALUES (21, 77, 'concierge');
INSERT INTO worksFor(HotelChainContactID, NonManagerContactID, Position) VALUES (21, 78, 'housekeeper');
INSERT INTO worksFor(HotelChainContactID, NonManagerContactID, Position) VALUES (22, 81, 'concierge');
INSERT INTO worksFor(HotelChainContactID, NonManagerContactID, Position) VALUES (22, 82, 'housekeeper');
INSERT INTO worksFor(HotelChainContactID, NonManagerContactID, Position) VALUES (23, 83, 'concierge');
INSERT INTO worksFor(HotelChainContactID, NonManagerContactID, Position) VALUES (23, 84, 'housekeeper');
INSERT INTO worksFor(HotelChainContactID, NonManagerContactID, Position) VALUES (24, 89, 'concierge');
INSERT INTO worksFor(HotelChainContactID, NonManagerContactID, Position) VALUES (24, 90, 'housekeeper');
INSERT INTO worksFor(HotelChainContactID, NonManagerContactID, Position) VALUES (25, 91, 'concierge');
INSERT INTO worksFor(HotelChainContactID, NonManagerContactID, Position) VALUES (25, 92, 'housekeeper');
INSERT INTO worksFor(HotelChainContactID, NonManagerContactID, Position) VALUES (26, 97, 'concierge');
INSERT INTO worksFor(HotelChainContactID, NonManagerContactID, Position) VALUES (26, 98, 'housekeeper');
INSERT INTO worksFor(HotelChainContactID, NonManagerContactID, Position) VALUES (27, 99, 'concierge');
INSERT INTO worksFor(HotelChainContactID, NonManagerContactID, Position) VALUES (27, 100, 'housekeeper');
INSERT INTO worksFor(HotelChainContactID, NonManagerContactID, Position) VALUES (28, 63, 'concierge');
INSERT INTO worksFor(HotelChainContactID, NonManagerContactID, Position) VALUES (28, 64, 'housekeeper');
INSERT INTO worksFor(HotelChainContactID, NonManagerContactID, Position) VALUES (29, 65, 'concierge');
INSERT INTO worksFor(HotelChainContactID, NonManagerContactID, Position) VALUES (29, 66, 'housekeeper');
INSERT INTO worksFor(HotelChainContactID, NonManagerContactID, Position) VALUES (30, 67, 'concierge');
INSERT INTO worksFor(HotelChainContactID, NonManagerContactID, Position) VALUES (30, 68, 'housekeeper');
INSERT INTO worksFor(HotelChainContactID, NonManagerContactID, Position) VALUES (31, 69, 'concierge');
INSERT INTO worksFor(HotelChainContactID, NonManagerContactID, Position) VALUES (31, 70, 'housekeeper');
INSERT INTO worksFor(HotelChainContactID, NonManagerContactID, Position) VALUES (32, 71, 'concierge');
INSERT INTO worksFor(HotelChainContactID, NonManagerContactID, Position) VALUES (32, 72, 'housekeeper');
INSERT INTO worksFor(HotelChainContactID, NonManagerContactID, Position) VALUES (33, 75, 'concierge');
INSERT INTO worksFor(HotelChainContactID, NonManagerContactID, Position) VALUES (33, 76, 'housekeeper');
INSERT INTO worksFor(HotelChainContactID, NonManagerContactID, Position) VALUES (34, 77, 'concierge');
INSERT INTO worksFor(HotelChainContactID, NonManagerContactID, Position) VALUES (34, 78, 'housekeeper');
INSERT INTO worksFor(HotelChainContactID, NonManagerContactID, Position) VALUES (35, 81, 'concierge');
INSERT INTO worksFor(HotelChainContactID, NonManagerContactID, Position) VALUES (35, 82, 'housekeeper');
INSERT INTO worksFor(HotelChainContactID, NonManagerContactID, Position) VALUES (36, 83, 'concierge');
INSERT INTO worksFor(HotelChainContactID, NonManagerContactID, Position) VALUES (36, 84, 'housekeeper');
INSERT INTO worksFor(HotelChainContactID, NonManagerContactID, Position) VALUES (37, 72, 'concierge');
INSERT INTO worksFor(HotelChainContactID, NonManagerContactID, Position) VALUES (37, 71, 'housekeeper');
INSERT INTO worksFor(HotelChainContactID, NonManagerContactID, Position) VALUES (38, 70, 'concierge');
INSERT INTO worksFor(HotelChainContactID, NonManagerContactID, Position) VALUES (38, 69, 'housekeeper');
INSERT INTO worksFor(HotelChainContactID, NonManagerContactID, Position) VALUES (39, 68, 'concierge');
INSERT INTO worksFor(HotelChainContactID, NonManagerContactID, Position) VALUES (40, 67, 'housekeeper');
INSERT INTO worksFor(HotelChainContactID, NonManagerContactID, Position) VALUES (40, 66, 'concierge');
INSERT INTO worksFor(HotelChainContactID, NonManagerContactID, Position) VALUES (41, 65, 'housekeeper');
INSERT INTO worksFor(HotelChainContactID, NonManagerContactID, Position) VALUES (41, 64, 'concierge');
INSERT INTO worksFor(HotelChainContactID, NonManagerContactID, Position) VALUES (42, 63, 'housekeeper');
INSERT INTO worksFor(HotelChainContactID, NonManagerContactID, Position) VALUES (42, 63, 'concierge');
INSERT INTO worksFor(HotelChainContactID, NonManagerContactID, Position) VALUES (43, 64, 'housekeeper');
INSERT INTO worksFor(HotelChainContactID, NonManagerContactID, Position) VALUES (43, 65, 'concierge');
INSERT INTO worksFor(HotelChainContactID, NonManagerContactID, Position) VALUES (44, 66, 'housekeeper');
INSERT INTO worksFor(HotelChainContactID, NonManagerContactID, Position) VALUES (44, 67, 'concierge');

INSERT INTO Booking (BookingID, ReserveStartDateTime, ReserveEndDateTime, TotalNumberOfOccupants, RoomNumber, 
					 HotelChainContactID, CustomerContactID) 
					 VALUES (1, 'March 24, 2021 13:00', 'March 26, 2021 14:00', 2, 600, 30, 1);

INSERT INTO Booking (BookingID, ReserveStartDateTime, ReserveEndDateTime, TotalNumberOfOccupants, RoomNumber, 
					 HotelChainContactID, CustomerContactID) 
					 VALUES (2, 'March 26, 2021 13:00', 'March 28, 2021 14:00', 2, 610, 31, 2);
					 
INSERT INTO Booking (BookingID, ReserveStartDateTime, ReserveEndDateTime, TotalNumberOfOccupants, RoomNumber, 
					 HotelChainContactID, CustomerContactID) 
					 VALUES (3, 'March 22, 2021 13:00', 'March 30, 2021 14:00', 1, 601, 30, 3);
					 
INSERT INTO Booking (BookingID, ReserveStartDateTime, ReserveEndDateTime, TotalNumberOfOccupants, RoomNumber, 
					 HotelChainContactID, CustomerContactID) 
					 VALUES (4, 'March 20, 2021 13:00', 'March 24, 2021 14:00', 4, 612, 31, 1);

INSERT INTO Booking (BookingID, ReserveStartDateTime, ReserveEndDateTime, TotalNumberOfOccupants, RoomNumber, 
					 HotelChainContactID, CustomerContactID) 
					 VALUES (5, 'March 10, 2021 13:00', 'April 25, 2021 14:00', 1, 602, 32, 5);
					 
INSERT INTO Booking (BookingID, ReserveStartDateTime, ReserveEndDateTime, TotalNumberOfOccupants, RoomNumber, 
					 HotelChainContactID, CustomerContactID) 
					 VALUES (6, 'February 10, 2021 13:00', 'April 24, 2021 14:00', 2, 608, 33, 6);

INSERT INTO Booking (BookingID, ReserveStartDateTime, ReserveEndDateTime, TotalNumberOfOccupants, RoomNumber, 
					 HotelChainContactID, CustomerContactID) 
					 VALUES (7, 'January 12, 2021 13:00', 'April 26, 2021 14:00', 3, 606, 34, 7);
					 
INSERT INTO Booking (BookingID, ReserveStartDateTime, ReserveEndDateTime, TotalNumberOfOccupants, RoomNumber, 
					 HotelChainContactID, CustomerContactID) 
					 VALUES (8, 'February 10, 2021 13:00', 'April 24, 2021 14:00', 4, 60, 35, 45);

INSERT INTO Booking (BookingID, ReserveStartDateTime, ReserveEndDateTime, TotalNumberOfOccupants, RoomNumber, 
					 HotelChainContactID, CustomerContactID) 
					 VALUES (9, 'March 10, 2021 13:00', 'April 24, 2021 14:00', 3, 61, 35, 46);
					 
INSERT INTO Booking (BookingID, ReserveStartDateTime, ReserveEndDateTime, TotalNumberOfOccupants, RoomNumber, 
					 HotelChainContactID, CustomerContactID) 
					 VALUES (10, 'February 9, 2021 13:00', 'April 24, 2021 14:00', 2, 604, 36, 47);
					 
INSERT INTO Booking (BookingID, ReserveStartDateTime, ReserveEndDateTime, TotalNumberOfOccupants, RoomNumber, 
					 HotelChainContactID, CustomerContactID) 
					 VALUES (11, 'March 10, 2021 13:00', 'April 24, 2021 14:00', 1, 64, 35, 48);
					 
INSERT INTO Booking (BookingID, ReserveStartDateTime, ReserveEndDateTime, TotalNumberOfOccupants, RoomNumber, 
					 HotelChainContactID, CustomerContactID) 
					 VALUES (12, 'February 10, 2021 9:00', 'April 24, 2021 14:00', 2, 608, 34, 49);
					 
INSERT INTO Booking (BookingID, ReserveStartDateTime, ReserveEndDateTime, TotalNumberOfOccupants, RoomNumber, 
					 HotelChainContactID, CustomerContactID) 
					 VALUES (13, 'February 8, 2021 13:00', 'April 26, 2021 14:00', 2, 607, 34, 50);
					 
INSERT INTO Booking (BookingID, ReserveStartDateTime, ReserveEndDateTime, TotalNumberOfOccupants, RoomNumber, 
				     HotelChainContactID, CustomerContactID) 
				     VALUES (14, 'January 3, 2022 14:00', 'April 24, 2022 16:00', 3, 606, 34, 51);
					 
INSERT INTO Renting (BookingID, CheckInDateTime, CheckOutDateTime, Payment) VALUES (3, 'March 22, 2021 14:00', NULL, 1600);  
INSERT INTO Renting (BookingID, CheckInDateTime, CheckOutDateTime, Payment) VALUES (4, 'March 20, 2021 14:00', NULL, 1600);
INSERT INTO Renting (BookingID, CheckInDateTime, CheckOutDateTime, Payment) VALUES (5, 'March 10, 2021 14:00', NULL, NULL); 
INSERT INTO Renting (BookingID, CheckInDateTime, CheckOutDateTime, Payment) VALUES (6, 'February 10, 2021 14:00', NULL, NULL);
INSERT INTO Renting (BookingID, CheckInDateTime, CheckOutDateTime, Payment) VALUES (7, 'January 12, 2021 14:00', NULL, NULL); 
INSERT INTO Renting (BookingID, CheckInDateTime, CheckOutDateTime, Payment) VALUES (8, 'February 10, 2021 14:00', NULL, NULL);
INSERT INTO Renting (BookingID, CheckInDateTime, CheckOutDateTime, Payment) VALUES (9, 'March 10, 2021 14:00', NULL, NULL); 
INSERT INTO Renting (BookingID, CheckInDateTime, CheckOutDateTime, Payment) VALUES (10, 'February 10, 2021 12:00', NULL, NULL);
INSERT INTO Renting (BookingID, CheckInDateTime, CheckOutDateTime, Payment) VALUES (11, 'March 10, 2021 14:00', NULL, NULL); 
INSERT INTO Renting (BookingID, CheckInDateTime, CheckOutDateTime, Payment) VALUES (12, 'February 10, 2021 12:00', NULL, NULL);
INSERT INTO Renting (BookingID, CheckInDateTime, CheckOutDateTime, Payment) VALUES (13, 'February 10, 2021 14:00', NULL, NULL); 
INSERT INTO Renting (BookingID, CheckInDateTime, CheckOutDateTime, Payment) VALUES (14, 'January 10, 2022 12:20', NULL, NULL);

INSERT INTO BookingArchive (ArchiveID, ReserveStartDateTime, ReserveEndDateTime, TotalNumberOfOccupants, CheckInDateTime, 
							CheckOutDateTime, RoomNumber, RoomPrice, RoomCapacity, RoomView, RoomExtendable, HotelBrandName, 
							HotelChainPhysicalAddress_Line1, HotelChainPhysicalAddress_Line2, HotelChainCity, HotelChainProv_State,
						    HotelChainCountry, HotelChainPostal_Zip, CustomerContactID) 
							VALUES (1, 'March 22, 2020 16:00', 'March 24, 2020 15:00', 2, 'March 22, 2020 14:00', 
									'March 24, 2020 15:00', 100, 200.00, 2, NULL, TRUE, 'Choice Hotels International', 
									'1 Rue Victoria', '', 'Gatineau', 'Quebec', 'Canada', 'J8X 1Z6', 1);
									
INSERT INTO BookingArchive (ArchiveID, ReserveStartDateTime, ReserveEndDateTime, TotalNumberOfOccupants, CheckInDateTime, 
							CheckOutDateTime, RoomNumber, RoomPrice, RoomCapacity, RoomView, RoomExtendable, HotelBrandName, 
							HotelChainPhysicalAddress_Line1, HotelChainPhysicalAddress_Line2, HotelChainCity, HotelChainProv_State,
						    HotelChainCountry, HotelChainPostal_Zip, CustomerContactID) 
							VALUES (2, 'March 24, 2020 16:00', 'March 30, 2020 15:00', 2, 'March 24, 2020 14:00', 
									'March 26, 2020 15:00', 102, 200.00, 4, 'Sea', FALSE, 'Hilton Worldwide Holdings Inc.', 
									'125 Lusk Street', '', 'Nepean', 'Ontario', 'Canada', 'K2J 6S5', 1);


INSERT INTO BookingArchive (ArchiveID, ReserveStartDateTime, ReserveEndDateTime, TotalNumberOfOccupants, CheckInDateTime, 
							CheckOutDateTime, RoomNumber, RoomPrice, RoomCapacity, RoomView, RoomExtendable, HotelBrandName, 
							HotelChainPhysicalAddress_Line1, HotelChainPhysicalAddress_Line2, HotelChainCity, HotelChainProv_State,
						    HotelChainCountry, HotelChainPostal_Zip, CustomerContactID) 
							VALUES (3, 'January 10, 2020 13:00', 'February 2, 2020 15:00', 1, 'January 10, 2020 13:05', 
									'February 2, 2020 14:39', 102, 200.00, 4, 'Sea', FALSE, 'Hilton Worldwide Holdings Inc.', 
									'125 Lusk Street', '', 'Nepean', 'Ontario', 'Canada', 'K2J 6S5', 1);

									
INSERT INTO BookingArchiveAmenity (ArchiveID, Amenity) VALUES (1, 'Business center');

INSERT INTO DatabaseAdministrator (UserName, Password) VALUES ('KarimDahel', MD5('KarimDahel'));
INSERT INTO DatabaseAdministrator (UserName, Password) VALUES ('ZiadSkaik', MD5('ZiadSkaik'));
INSERT INTO DatabaseAdministrator (UserName, Password) VALUES ('ScarlettChan', MD5('ScarlettChan'));
