"""
Created on : March 14 2021

Author : KD

This is the GUI for employees to either sign in or reset their forgotten password.
"""

import sys

from PyQt5.QtWidgets import (QApplication, QWidget, QLineEdit, QPushButton)

from pubsub import pub as ThePubSubMachine

from GenericUtilities.MessageStructures import AuthenticateEmployeeStructure
from GenericUtilities import NotificationDialogsLibrary
from GenericUtilities import UILoadingLibrary
from GenericUtilities.PublishSubscribeTopics import *


class EmployeeSignInPageController(QWidget):

    def __init__(self):

        QWidget.__init__(self)

        # Load the GUI using this generic function. This will look for the UI file to load first and if not found will look
        # for the generated equivalent python file. This allows for deployment but also does not require rebuilding each time the UI file is changed.
        self._employee_sign_in_page = UILoadingLibrary.load_gui("EmployeeSignInPage.ui", self)

        # Get the widgets from the UI and connect their signals to the slots for them as required.

        self._login_button = self.findChild(QPushButton, "loginPushButton")
        self._login_button.clicked.connect(self._login_button_clicked)

        self._password_recovery_button = self.findChild(QPushButton, "passwordRecoveryPushButton")
        self._password_recovery_button.clicked.connect(self._password_recovery_button_clicked)

        self._cancel_button = self.findChild(QPushButton, "cancelPushButton")
        self._cancel_button.clicked.connect(self._cancel_button_clicked)

        self._employee_id_line_edit = self.findChild(QLineEdit, "employeeIDLineEdit")

        self._password_line_edit = self.findChild(QLineEdit, "passwordLineEdit")



    def _login_button_clicked(self):
        # The employee has clicked the button and is now requesting to log in.

        # First make sure there is an employee id or phone or email and a password set in the text fields.
        if self._employee_id_line_edit.text() == "" or self._password_line_edit.text() == "":
            NotificationDialogsLibrary.show_error_dialog("Missing Field Error", "The employee user identification and password fields must both be non-empty.")
            return

        if self._employee_id_line_edit.text().find("'") != -1 or self._password_line_edit.text().find("'") != -1:
            NotificationDialogsLibrary.show_error_dialog("Invalid Character Error", "The following character is invalid for credentials: '")
            return

        # Create an employee authentication message and send it to the database access module for it to process. It will then place its response inside this message.
        employee_authentication_data = AuthenticateEmployeeStructure(self._employee_id_line_edit.text(), self._password_line_edit.text())
        ThePubSubMachine.sendMessage(AUTHENTICATE_EMPLOYEE_TOPIC, authentication_employee_structure=employee_authentication_data)

        if employee_authentication_data.special_admin_login:
            # This is a special backdoor that database administrators can use to get to the SQL query dialog.
            ThePubSubMachine.sendMessage(GENERATE_NEW_COMPONENT_TOPIC, component_name="SQLQueryPage")
            self.close()
            return

        # Check the response provided to see if the employee id and password were good and the employee was properly authenticated and no errors occurred.
        if employee_authentication_data.authentication_success:

            if employee_authentication_data.error_message is not None:
                NotificationDialogsLibrary.show_error_dialog("Hotel Matching error", employee_authentication_data.error_message)
                return

            NotificationDialogsLibrary.show_success_dialog("Authentication Completion", "Employee Login Successful")

            list_of_hotels_info = employee_authentication_data.list_of_hotel_id_name_and_full_address_tuples

            if len(list_of_hotels_info) == 1:
                # The employee has now successfully signed in and he or she works for only 1 hotel so we can send a request to get the employee main menu page directly.

                hotel_id = list_of_hotels_info[0][0]
                hotel_name = list_of_hotels_info[0][1]
                hotel_address = list_of_hotels_info[0][2]

                params = {"employee_id" : employee_authentication_data.employee_id, "employee_name" : employee_authentication_data.employee_full_name,
                          "hotel_contact_id" : hotel_id, "hotel_name" : hotel_name, "hotel_address" : hotel_address}
                ThePubSubMachine.sendMessage(GENERATE_NEW_COMPONENT_TOPIC, component_name="EmployeeMenuPage", kwargs=params)
                self.close()
            else:
                # There are multiple hotels for this employee so we must now use an intermediate dialog to select which hotel to sign on for.
                params = {"employee_id" : employee_authentication_data.employee_id, "employee_name" : employee_authentication_data.employee_full_name,
                          "list_of_hotel_ids_names_and_addresses" : list_of_hotels_info}
                ThePubSubMachine.sendMessage(GENERATE_NEW_COMPONENT_TOPIC, component_name="SelectHotelDialog", kwargs=params)
                self.close()

        else:
            # The authentication failed. Do not indicate any more information than that for security purposes.
            NotificationDialogsLibrary.show_error_dialog("Authentication Failure", "Could not login with the credentials provided.")


    def _password_recovery_button_clicked(self):
        # Send a request to instantiate the next component the component factory.
        ThePubSubMachine.sendMessage(GENERATE_NEW_COMPONENT_TOPIC, component_name="PasswordRecoveryDialog")


    def _cancel_button_clicked(self):
        # Send a request to instantiate the next component the component factory.
        ThePubSubMachine.sendMessage(GENERATE_NEW_COMPONENT_TOPIC, component_name="WelcomePage")
        self.close()



if __name__ == "__main__":
    # This can be used as a quick way to test this component.
    app = QApplication(sys.argv)
    controller = EmployeeSignInPageController()
    controller.show()
    app.exec_()
