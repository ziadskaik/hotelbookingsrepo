"""
Created on : March 20 2021

Author : KD

This is the GUI for displaying all the results from a room search to a customer.
"""

import sys
from datetime import datetime
import time

from PyQt5.QtWidgets import (QApplication, QWidget, QLineEdit, QPushButton, QLabel, QDateTimeEdit, QTableWidget, QCheckBox, QTableWidgetItem)
from PyQt5.QtCore import Qt, QDate, QTime, QDateTime

from pubsub import pub as ThePubSubMachine

from GenericUtilities.MessageStructures import BookCustomerStructure, HotelRoomBookingInfoStructure, CustomerInformationStructure
from GenericUtilities import NotificationDialogsLibrary
from GenericUtilities import UILoadingLibrary
from GenericUtilities import EmailLibrary
from GenericUtilities.PublishSubscribeTopics import *



class CustomerRoomSearchResultsDialogController(QWidget):

    def __init__(self, customer_id, hotel_room_search_structure, hotel_room_search_results):

        QWidget.__init__(self,)


        self._customer_id = customer_id
        self._hotel_room_search_structure = hotel_room_search_structure

        # Load the GUI using this generic function. This will look for the UI file to load first and if not found will look
        # for the generated equivalent python file. This allows for deployment but also does not require rebuilding each time the UI file is changed.
        self._room_search_dialog = UILoadingLibrary.load_gui("CustomerRoomSearchResultsDialog.ui", self)

        # Get the widgets from the UI and connect their signals to the slots for them as required.

        self._close_button = self.findChild(QPushButton, "closeButton")
        self._close_button.clicked.connect(self._close_button_clicked)

        self._search_results_table_widget = self.findChild(QTableWidget, "searchResultsTableWidget")

        # Convert the unix epoch to a human readable datetime value so it can be displayed on the GUI.
        human_readable_start_time = time.ctime(hotel_room_search_structure.reserve_start_unix_epoch)
        human_readable_end_time = time.ctime(hotel_room_search_structure.reserve_end_unix_epoch)

        # Place the annotation at the top left of the dialog.
        self._hotel_rooms_label = self.findChild(QLabel, "hotelRoomsLabel")
        self._hotel_rooms_label.setText("Hotel rooms in " + hotel_room_search_structure.city + ":\t\t"
                                        + "Reservation start : {0} \t\tReservation end : {1}".format(human_readable_start_time, human_readable_end_time))


        # Populate the search results table with the provided search results.
        # But do not present "identical" rooms more than once - instead just indicate how many such rooms are available.
        self._hotel_representative_room_to_number_of_rooms_map = {}
        self._populate_table_widget(hotel_room_search_results, hotel_room_search_structure)



    def _set_line_edit_in_table(self, row_index, column_index, line_edit_content, fixed_width):
        # Fill the content of 1 particular cell in the search results table.
        line_edit = QLineEdit(str(line_edit_content))
        line_edit.setAlignment(Qt.AlignCenter)
        line_edit.setFixedWidth(fixed_width)
        line_edit.setReadOnly(True)
        self._search_results_table_widget.setCellWidget(row_index, column_index, line_edit)


    def _populate_table_widget(self, hotel_room_search_results, hotel_room_search_structure):

        # First iterate through the list of search results and group all "identical rooms" in a map where the values of the map are the number os such available rooms.
        for room_search_result in hotel_room_search_results:
            if room_search_result not in self._hotel_representative_room_to_number_of_rooms_map:
                # This is a "new" room type.
                self._hotel_representative_room_to_number_of_rooms_map[room_search_result] = 1
            else:
                # There is already a search result in the map that represents this room type so we just increment the counter to keep track of how many such rooms there are.
                self._hotel_representative_room_to_number_of_rooms_map[room_search_result] += 1


        row_index = 0
        # Iterate through all "unique" room results, fill in he column and indicate how many such rooms there are.
        for hotel_representative_room, number_of_available_rooms in self._hotel_representative_room_to_number_of_rooms_map.items():

            # Add a new row at the bottom of the Q table.
            self._search_results_table_widget.insertRow(row_index)

            # The first column in the table is for the hotel brand name.
            self._set_line_edit_in_table(row_index, 0, hotel_representative_room.hotel_name, 228)

            # The second column in the table is for the hotel star category.
            self._set_line_edit_in_table(row_index, 1, hotel_representative_room.star_category, 85)

            # The third column in the table is for the hotel chain's physical address
            self._set_line_edit_in_table(row_index, 2, hotel_representative_room.full_address, 290)

            # The fourth column in the table is for price per night for the hotel room.
            self._set_line_edit_in_table(row_index, 3,  "$ " + str(hotel_representative_room.price), 95)


            # The fifth column in the table is for total place for reserving the room for the entire amount of time. This must first be calculated.
            total_reserve_duration_in_seconds = hotel_room_search_structure.reserve_end_unix_epoch - hotel_room_search_structure.reserve_start_unix_epoch
            total_number_of_days_decimal = total_reserve_duration_in_seconds/(60*60*24)  # Divide by the total number of seconds in a day to get the total number of days.
            total_number_of_days = round(total_number_of_days_decimal + 0.5001)  # Calculate the ceiling function to get the total number of nights spent in the room.
            total_cost_of_reservation = total_number_of_days*hotel_representative_room.price  # Calculate the total cost accordingly.
            self._set_line_edit_in_table(row_index, 4, "$ " + str(total_cost_of_reservation), 80)

            # The sixth column in the table is for the room's capacity (this includes its ability to be extended).
            self._set_line_edit_in_table(row_index, 5, hotel_representative_room.capacity, 80)

            # The seventh column in the table is for the room's view type.
            self._set_line_edit_in_table(row_index, 6, hotel_representative_room.view, 80)

            # The eighth column in the table is for the room's amenities.
            list_of_amenities_string = str(hotel_representative_room.list_of_amenities).replace("[", "").replace("]", "").replace("'", "")  # Convert the list to a human readable string with commas.
            if list_of_amenities_string == "":
                list_of_amenities_string = "(None)"
            self._set_line_edit_in_table(row_index, 7, list_of_amenities_string, 300)

            # The ninth column in the table is for the hotel chain's phone number where the room is located at.
            self._set_line_edit_in_table(row_index, 8, hotel_representative_room.phone_number, 95)

            # The tenth column in the table is to indicate how many such rooms are currently available at the hotel.
            self._set_line_edit_in_table(row_index, 9, number_of_available_rooms, 115)

            # Each row has a "Book Room" push button. The customer uses this to book any room in the table.
            current_booking_button = QPushButton("Book Room")
            # Generate a special slot that takes into acoout which row was pressed to handle the customer pressing the "Book Room button.
            current_booking_button.clicked.connect(self._generate_booking_button_slot(row_index))
            current_booking_button.setFixedWidth(90)
            self._search_results_table_widget.setCellWidget(row_index, 10, current_booking_button)

            row_index = row_index + 1

        self._search_results_table_widget.resizeColumnsToContents()


    def _close_button_clicked(self):
        self.close()

    def _generate_booking_button_slot(self, row_index):
        # Create a slot specifically for the given row.

        def booking_selection_made():
            confirmation = NotificationDialogsLibrary.show_yes_or_no_dialog("Booking Confirmation", "Confirm booking for search result " + str(row_index+1) + "?")

            if confirmation:

                book_customer_data, selected_hotel_room_search_result = self._create_book_customer_structure(row_index)

                # Send a request to the database access module to book the customer.
                ThePubSubMachine.sendMessage(BOOK_CUSTOMER_TOPIC, book_customer_structure=book_customer_data)

                if book_customer_data.error_message is not None:
                    NotificationDialogsLibrary.show_error_dialog("Error", book_customer_data.error_message)
                elif book_customer_data.booking_id is None:
                    NotificationDialogsLibrary.show_error_dialog("Booking Failed!", "The booking could not be completed.")
                else:
                    NotificationDialogsLibrary.show_success_dialog("Booking Complete", "Your booking ID is " + str(book_customer_data.booking_id) + ".")

                    # Also send the customer an email to give them their entire booking info.
                    room_booking_info_structure = self._create_room_booking_info_structure(row_index, book_customer_data, selected_hotel_room_search_result)

                    if room_booking_info_structure.customer_email_address is not None:
                        # The customer must have an email address otherwise do not try to send any emails.
                        EmailLibrary.send_booking_confirmation_email(room_booking_info_structure)

                    self.close()


        return booking_selection_made


    def _create_book_customer_structure(self, row_index):
        # Create the message structure for all data required to book a customer.

        selected_hotel_room_search_result = list(self._hotel_representative_room_to_number_of_rooms_map.keys())[row_index]

        date_time_start = str(datetime.fromtimestamp(self._hotel_room_search_structure.reserve_start_unix_epoch)).split(".")[0]
        date_time_end = str(datetime.fromtimestamp(self._hotel_room_search_structure.reserve_end_unix_epoch)).split(".")[0]


        book_customer_structure = BookCustomerStructure(hotel_chain_contact_id=selected_hotel_room_search_result.hotelChainContactID,
                                                        room_number=selected_hotel_room_search_result.room_number,
                                                        reserve_start_datetime=date_time_start,
                                                        reserve_end_datetime=date_time_end,
                                                        customer_identification=self._customer_id,
                                                        number_of_occupants=self._hotel_room_search_structure.number_of_occupants)
        return book_customer_structure, selected_hotel_room_search_result


    def _create_room_booking_info_structure(self, row_index, book_customer_data, selected_hotel_room_search_result):

        total_cost_of_booking = self._search_results_table_widget.cellWidget(row_index , 4).text().replace("$ ", "")

        customer_info_data = CustomerInformationStructure(self._customer_id)

        # Send the request to the database access module for it to process and provide its response by placing it in this structure.
        ThePubSubMachine.sendMessage(GET_CUSTOMER_INFO_TOPIC, customer_info_structure=customer_info_data)


        return HotelRoomBookingInfoStructure(hotel_name=selected_hotel_room_search_result.hotel_name,
                                             star_category=str(selected_hotel_room_search_result.star_category),
                                             full_address=selected_hotel_room_search_result.full_address,
                                             phone_number=selected_hotel_room_search_result.phone_number,
                                             reserve_start_datetime=book_customer_data.reserve_start_datetime,
                                             reserve_end_datetime=book_customer_data.reserve_end_datetime,
                                             total_cost=total_cost_of_booking,
                                             capacity=str(selected_hotel_room_search_result.capacity),
                                             view_type=selected_hotel_room_search_result.view,
                                             list_of_amenities=selected_hotel_room_search_result.list_of_amenities,
                                             booking_id=str(book_customer_data.booking_id),
                                             customer_name=customer_info_data.customer_name,
                                             customer_email_address=customer_info_data.customer_email_address)






if __name__ == "__main__":
    app = QApplication(sys.argv)
    a = CustomerRoomSearchResultsDialogController()
    a.show()
    app.exec_()
