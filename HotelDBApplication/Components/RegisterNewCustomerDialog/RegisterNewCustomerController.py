"""
Created on : March 6 2021

Author : KD

This is the GUI for customers fill in the registration for to create a new account.
"""

import sys

from PyQt5.QtWidgets import (QApplication, QWidget, QLineEdit, QPushButton)

from pubsub import pub as ThePubSubMachine

from GenericUtilities.MessageStructures import RegisterCustomerStructure
from GenericUtilities import NotificationDialogsLibrary
from GenericUtilities import UILoadingLibrary
from GenericUtilities import EmailLibrary
from GenericUtilities.PublishSubscribeTopics import *



class RegisterNewCustomerController(QWidget):

    def __init__(self):

        QWidget.__init__(self)

        # Load the GUI using this generic function. This will look for the UI file to load first and if not found will look
        # for the generated equivalent python file. This allows for deployment but also does not require rebuilding each time the UI file is changed.
        self._register_new_customer_dialog = UILoadingLibrary.load_gui("RegisterNewCustomer.ui", self)

        # Get the widgets from the UI and connect their signals to the slots for them as required.

        self._first_name_line_edit = self.findChild(QLineEdit, "firstNameLineEdit")
        self._last_name_line_edit = self.findChild(QLineEdit, "lastNameLineEdit")
        self._sin_line_edit = self.findChild(QLineEdit, "sinLineEdit")
        self._address_line_1_edit = self.findChild(QLineEdit, "addressLine1LineEdit")
        self._address_line_2_edit = self.findChild(QLineEdit, "addressLine2LineEdit")
        self._city_line_edit = self.findChild(QLineEdit, "cityLineEdit")
        self._prov_state_line_edit = self.findChild(QLineEdit, "provStateLineEdit")
        self._country_line_edit = self.findChild(QLineEdit, "countryLineEdit")
        self._postal_zip_line_edit = self.findChild(QLineEdit, "postalZipLineEdit")
        self._phone_number_line_edit = self.findChild(QLineEdit, "phoneNumberLineEdit")
        self._email_line_edit = self.findChild(QLineEdit, "emailLineEdit")
        self._password_line_edit = self.findChild(QLineEdit, "passwordLineEdit")
        self._confirm_password_line_edit = self.findChild(QLineEdit, "confirmPasswordLineEdit")

        self._register_button = self.findChild(QPushButton, "registerPushButton")
        self._register_button.clicked.connect(self._register_button_clicked)

        self._cancel_button = self.findChild(QPushButton, "cancelPushButton")
        self._cancel_button.clicked.connect(self._cancel_button_clicked)


        # Keep a list of all the line edits for the fields that are mandatory.
        self._REQUIRED_FIELDS = [self._first_name_line_edit, self._last_name_line_edit, self._sin_line_edit, self._address_line_1_edit, self._city_line_edit,
                                 self._country_line_edit, self._email_line_edit, self._password_line_edit, self._confirm_password_line_edit]


        # Auto fill these fields to save time during the project demo.
        self._first_name_line_edit.setText("John")
        self._last_name_line_edit.setText("Doe")
        self._sin_line_edit.setText("567675983")
        self._address_line_1_edit.setText("563 Angels Street")
        self._address_line_2_edit.setText("")
        self._city_line_edit.setText("Los Angeles")
        self._prov_state_line_edit.setText("California")
        self._country_line_edit.setText("USA")
        self._phone_number_line_edit.setText("919-674-7564")
        self._email_line_edit.setText("jo1hn11doe.22@gmail.com")
        self._password_line_edit.setText("abcd")
        self._confirm_password_line_edit.setText("abcd")



    def _register_button_clicked(self):
        # The user has requested to register to the system.

        # First make sure that none of the required fields were left empty.
        if not self._verify_required_fields_filled():
            return

        # Make sure the password and its confirmation both match.
        if not self._verify_passwords():
            return

        if not self._verify_phone_number():
            return

        # Pick up the data in all the line edit fields and create a message request to register a new customer.
        register_new_customer_data = self._create_register_new_customer_structure()
        # Send the request to the database access module for it to process and put its response within the message.
        ThePubSubMachine.sendMessage(REGISTER_NEW_CUSTOMER_TOPIC, register_new_customer_structure=register_new_customer_data)

        if register_new_customer_data.error_message is not None:
            # The database access module has detected an error and has provided a corresponding error message to be displayed to the user
            NotificationDialogsLibrary.show_error_dialog("Registration Error", str(register_new_customer_data.error_message))
            return

        # The customer has been registered into the database and the database access module has placed its new customer ID in the structure for us to get.
        new_customer_id = register_new_customer_data.new_customer_id

        # Display the new customer ID to the user.
        NotificationDialogsLibrary.show_success_dialog("Registration Complete", "Your customer ID is " + str(new_customer_id) + ".")

        # Also email the user to confirm their registration and provide them with their customer ID. This is commented for now to prevent Google from blocking the account.
        EmailLibrary.send_registration_confirmation_email(register_new_customer_data.email, register_new_customer_data.new_customer_id)

        self.close()


    def _cancel_button_clicked(self):
        self.close()


    def _verify_required_fields_filled(self):
        # Look at the line edits for all the mandatory field and provide an error message if any is found to be empty.

        for line_edit in self._REQUIRED_FIELDS:

            if line_edit.text() == "":
                line_edit_name = line_edit.objectName()
                field_name = line_edit_name.split("LineEdit")[0]
                NotificationDialogsLibrary.show_error_dialog("Missing Field", "The following field is required : " + str(field_name))
                return False

        return True

    def _verify_phone_number(self):
        if self._phone_number_line_edit.text().find("-") == -1:
            NotificationDialogsLibrary.show_error_dialog("Incorrect Format Error", "The phone number must include the dashes.")
            return False

        return True

    def _verify_passwords(self):

        if self._password_line_edit.text() != self._confirm_password_line_edit.text():
            NotificationDialogsLibrary.show_error_dialog("Inconsistency Error", "The passwords entered do not match.")
            return False

        return True


    def _create_register_new_customer_structure(self):
        # Create the message structure from all the information the user has provided in the dialog GUI.

        register_new_customer_structure = RegisterCustomerStructure(first_name=self._first_name_line_edit.text(), last_name=self._last_name_line_edit.text(), sin=self._sin_line_edit.text(),
                            address_line_1=self._address_line_1_edit.text(), address_line_2=self._address_line_2_edit.text(), city=self._city_line_edit.text(),
                                    prov_state=self._prov_state_line_edit.text(), country=self._country_line_edit.text(), postal_zip=self._postal_zip_line_edit.text(),
                                                                    phone=self._phone_number_line_edit.text(), email=self._email_line_edit.text(),
                                                                    plain_text_password=self._password_line_edit.text())

        return register_new_customer_structure



if __name__ == "__main__":
    app = QApplication(sys.argv)
    a = RegisterNewCustomerController()
    a.show()
    app.exec_()
