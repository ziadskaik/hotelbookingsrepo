"""
Created on : March 14 2021

Author : KD

This is an intermediate GUI for employees to select which specific hotels sign are sign in for.
"""

import sys

from PyQt5.QtWidgets import (QApplication, QWidget, QComboBox, QPushButton, QLabel)

from pubsub import pub as ThePubSubMachine

from GenericUtilities import UILoadingLibrary
from GenericUtilities.PublishSubscribeTopics import *


class SelectHotelDialogController(QWidget):

    def __init__(self, employee_id, employee_name, list_of_hotel_ids_names_and_addresses):

        QWidget.__init__(self)

        self._employee_id = employee_id
        self._employee_name = employee_name
        self._list_of_hotel_ids_names_and_addresses = list_of_hotel_ids_names_and_addresses

        # Load the GUI using this generic function. This will look for the UI file to load first and if not found will look
        # for the generated equivalent python file. This allows for deployment but also does not require rebuilding each time the UI file is changed.
        self._select_hotel_dialog = UILoadingLibrary.load_gui("SelectHotelDialog.ui", self)


        # Get the widgets from the UI and connect their signals to the slots for them as required.

        self._hotels_combo_box = self.findChild(QComboBox, "hotelsComboBox")
        self._hotels_combo_box.currentIndexChanged.connect(self._hotels_combo_box_current_index_changed)

        self._continue_button = self.findChild(QPushButton, "continuePushButton")
        self._continue_button.clicked.connect(self._continue_button_clicked)

        self._cancel_button = self.findChild(QPushButton, "cancelPushButton")
        self._cancel_button.clicked.connect(self._cancel_button_clicked)

        self._instruction_label = self.findChild(QLabel, "instructionLabel")
        self._instruction_label.setText(employee_name + ", please select which hotel to sign in for.")

        # Fill in the combo box with the hotel names and addresses
        for hotel_id, hotel_name, hotel_address in list_of_hotel_ids_names_and_addresses:
            self._hotels_combo_box.addItem(hotel_name + " - " + hotel_address)


    def _continue_button_clicked(self):
        # The employee has selected the hotel to sign in for.

        # The index in the list is the same as the index on the combo box.
        hotel_contact_id, hotel_name,hotel_address = self._list_of_hotel_ids_names_and_addresses[self._hotels_combo_box.currentIndex()]

        # Send a request to get the employee main menu page.
        params = {"employee_id": self._employee_id,
                  "employee_name": self._employee_name,
                  "hotel_contact_id": hotel_contact_id, "hotel_name": hotel_name, "hotel_address": hotel_address}
        ThePubSubMachine.sendMessage(GENERATE_NEW_COMPONENT_TOPIC, component_name="EmployeeMenuPage", kwargs=params)
        self.close()


    def _hotels_combo_box_current_index_changed(self, index):
        # So far, needs to be done here.
        pass

    def _cancel_button_clicked(self):
        # Send a request to instantiate the next component the component factory.
        ThePubSubMachine.sendMessage(GENERATE_NEW_COMPONENT_TOPIC, component_name="WelcomePage")
        self.close()





if __name__ == "__main__":
    app = QApplication(sys.argv)
    a = SelectHotelDialogController()
    a.show()
    app.exec_()
