"""
Created on : March 20 2021

Author : ZS

This is the GUI for database administrators to enter SQL Queries and view results were applicable.
"""

import sys
from PyQt5.QtWidgets import (QApplication, QWidget, QComboBox, QLabel, QTableWidget, QPushButton, QTableWidget,
                             QTextEdit, QLineEdit)
from PyQt5.QtCore import Qt

from pubsub import pub as ThePubSubMachine
from GenericUtilities import UILoadingLibrary
from GenericUtilities.MessageStructures import SQLQueryStructure
from GenericUtilities import NotificationDialogsLibrary
from GenericUtilities.PublishSubscribeTopics import *


class SQLQueryPageController(QWidget):

    def __init__(self):

        QWidget.__init__(self)

        # Load the GUI using this generic function. This will look for the UI file to load first and if not found will look
        # for the generated equivalent python file. This allows for deployment but also does not require rebuilding each time the UI file is changed.
        self._sql_query_page = UILoadingLibrary.load_gui("SQLQueryPage.ui", self)

        # Get the widgets from the UI and connect their signals to the slots for them as required.

        self._sql_query_text_edit = self.findChild(QTextEdit, "SQLQueryTextEdit")
        self._sql_query_result_table = self.findChild(QTableWidget, "SQLQueryResultTable")

        self._execute_sql_query_button = self.findChild(QPushButton, "executeQueryPushButton")
        self._execute_sql_query_button.clicked.connect(self._execute_sql_query_button_clicked)


        self._logout_button = self.findChild(QPushButton, "logoutPushButton")
        self._logout_button.clicked.connect(self._logout_button_clicked)


    def _execute_sql_query_button_clicked(self):

        sql_query_structure_data = SQLQueryStructure(self._sql_query_text_edit.toPlainText())
        ThePubSubMachine.sendMessage(SQL_QUERY_TOPIC, sql_query_structure=sql_query_structure_data)

        if sql_query_structure_data.sql_query_result_tuples is not None:
            # Get all the headers for the tuples returned
            headers = [i[0] for i in sql_query_structure_data.sql_query_result_headers]

            # Clear the table widget every time before inserting new data from new query
            self._sql_query_result_table.clear()
            # Set the row count to the number of rows returned by the query
            self._sql_query_result_table.setRowCount(sql_query_structure_data.sql_query_result_row_count)
            self._sql_query_result_table.setColumnCount(len(headers))

            self._sql_query_result_table.setHorizontalHeaderLabels(headers)

            for i in range(0, sql_query_structure_data.sql_query_result_row_count):
                for j in range(0, len(headers)):
                    self._set_line_edit_in_table(i, j, sql_query_structure_data.sql_query_result_tuples[i][j])
        else:
            if sql_query_structure_data.error_message is not None:
                NotificationDialogsLibrary.show_error_dialog("SQL Error",
                                                             str(sql_query_structure_data.error_message))
            else:

                NotificationDialogsLibrary.show_success_dialog("Success!", "The Query was successful.")


    def _logout_button_clicked(self):
        # Send a request to instantiate the next component the component factory.
        ThePubSubMachine.sendMessage(GENERATE_NEW_COMPONENT_TOPIC, component_name="WelcomePage")
        self.close()

    def _set_line_edit_in_table(self, row_index, column_index, line_edit_content):
        # Fill the content of 1 particular cell in the search results table.
        line_edit = QLineEdit(str(line_edit_content))
        line_edit.setAlignment(Qt.AlignCenter)
        line_edit.setReadOnly(True)
        self._sql_query_result_table.setCellWidget(row_index, column_index, line_edit)



if __name__ == "__main__":
    # This can be used as a quick way to test this component.
    app = QApplication(sys.argv)
    controller = SQLQueryPageController()
    controller.show()
    app.exec_()
