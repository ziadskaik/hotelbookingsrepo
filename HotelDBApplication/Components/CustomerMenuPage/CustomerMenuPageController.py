"""
Created on : March 9 2021

Author : ZS

This is the GUI that appears when the customer has successfully signed in.
"""

import sys
from PyQt5.QtWidgets import (QApplication, QWidget, QComboBox, QLabel, QTableWidget, QLineEdit, QPushButton)

from pubsub import pub as ThePubSubMachine
from GenericUtilities import UILoadingLibrary
from GenericUtilities.PublishSubscribeTopics import *



class CustomerMenuPageController(QWidget):

    def __init__(self, customer_name, customer_id):

        QWidget.__init__(self)

        # Load the GUI using this generic function. This will look for the UI file to load first and if not found will look
        # for the generated equivalent python file. This allows for deployment but also does not require rebuilding each time the UI file is changed.
        self._messenger_customer_menu_window = UILoadingLibrary.load_gui("CustomerMenuPage.ui", self)

        # Get the widgets from the UI and connect their signals to the slots for them as required.

        self._customer_name_label = self.findChild(QLabel, "customerName")
        self._customer_id_label = self.findChild(QLabel, "customerID")

        self._book_hotel_room_button = self.findChild(QPushButton, "bookHotelRoomButton")
        self._book_hotel_room_button.clicked.connect(self._book_hotel_room_button_clicked)

        self._view_current_bookings_button = self.findChild(QPushButton, "viewCurrentBookingsButton")
        self._view_current_bookings_button.clicked.connect(self._view_current_bookings_button_clicked)

        self._view_past_bookings_button = self.findChild(QPushButton, "viewPastBookingsButton")
        self._view_past_bookings_button.clicked.connect(self._view_past_bookings_button_clicked)

        self._logout_button = self.findChild(QPushButton, "logoutButton")
        self._logout_button.clicked.connect(self._logout_button_clicked)

        self.customer_name = customer_name
        self.customer_id = customer_id

        self._customer_name_label.setText(self.customer_name)
        self._customer_id_label.setText(self._customer_id_label.text() + " " + str(self.customer_id))


    def _book_hotel_room_button_clicked(self):
        # Send a request to instantiate the next component the component factory.
        params = {"customer_id": self.customer_id}
        ThePubSubMachine.sendMessage(GENERATE_NEW_COMPONENT_TOPIC, component_name="RoomSearchDialog", kwargs=params)


    def _view_current_bookings_button_clicked(self):
        # Send a request to instantiate the next component the component factory.
        params = {"customer_name": self.customer_name, "customer_id": self.customer_id, "current_or_past_bookings": "Current"}
        ThePubSubMachine.sendMessage(GENERATE_NEW_COMPONENT_TOPIC, component_name="CustomerViewBookingsDialog", kwargs=params)


    def _view_past_bookings_button_clicked(self):
        # Send a request to instantiate the next component the component factory.
        params = {"customer_name": self.customer_name, "customer_id": self.customer_id, "current_or_past_bookings": "Past"}
        ThePubSubMachine.sendMessage(GENERATE_NEW_COMPONENT_TOPIC, component_name="CustomerViewBookingsDialog", kwargs=params)


    def _logout_button_clicked(self):
        # Send a request to instantiate the next component the component factory.
        ThePubSubMachine.sendMessage(GENERATE_NEW_COMPONENT_TOPIC, component_name="WelcomePage")
        self.close()



if __name__ == "__main__":
    # This can be used as a quick way to test this component.
    app = QApplication(sys.argv)
    controller = CustomerMenuPageController()
    controller.show()
    app.exec_()
