"""
Created on : March 6 2021

Author : KD

This is the GUI for customers to either sign in, create a new account, or reset their forgotten password.
"""

import sys

from PyQt5.QtWidgets import (QApplication, QWidget, QLineEdit, QPushButton)

from pubsub import pub as ThePubSubMachine

from GenericUtilities.MessageStructures import AuthenticateCustomerStructure
from GenericUtilities import NotificationDialogsLibrary
from GenericUtilities import UILoadingLibrary
from GenericUtilities.PublishSubscribeTopics import *


class CustomerSignInPageController(QWidget):

    def __init__(self):

        QWidget.__init__(self)

        # Load the GUI using this generic function. This will look for the UI file to load first and if not found will look
        # for the generated equivalent python file. This allows for deployment but also does not require rebuilding each time the UI file is changed.
        self._customer_sign_in_page = UILoadingLibrary.load_gui("CustomerSignInPage.ui", self)

        # Get the widgets from the UI and connect their signals to the slots for them as required.

        self._login_button = self.findChild(QPushButton, "loginPushButton")
        self._login_button.clicked.connect(self._login_button_clicked)

        self._register_button = self.findChild(QPushButton, "registerPushButton")
        self._register_button.clicked.connect(self._register_button_clicked)

        self._password_recovery_button = self.findChild(QPushButton, "passwordRecoveryPushButton")
        self._password_recovery_button.clicked.connect(self._password_recovery_button_clicked)

        self._customer_id_line_edit = self.findChild(QLineEdit, "customerIDLineEdit")

        self._password_line_edit = self.findChild(QLineEdit, "passwordLineEdit")

        self._cancel_button = self.findChild(QPushButton, "cancelPushButton")
        self._cancel_button.clicked.connect(self._cancel_button_clicked)



    def _login_button_clicked(self):
        # The customer has clicked the button and is now requesting to log in.

        # First make sure there is a user id or phone or email and a password set in the text fields.
        if self._customer_id_line_edit.text() == "" or self._password_line_edit.text() == "":
            NotificationDialogsLibrary.show_error_dialog("Missing Field Error", "The customer user identification and password fields must both be non-empty.")
            return

        if self._customer_id_line_edit.text().find("'") != -1 or self._password_line_edit.text().find("'") != -1:
            NotificationDialogsLibrary.show_error_dialog("Invalid Character Error", "The following character is invalid for credentials: '")
            return


        # Create a customer authentication message and send it to the database access module for it to process. It will then place its response inside this message.
        customer_authentication_data = AuthenticateCustomerStructure(self._customer_id_line_edit.text(), self._password_line_edit.text())
        ThePubSubMachine.sendMessage(AUTHENTICATE_CUSTOMER_TOPIC, authentication_customer_structure=customer_authentication_data)

        if customer_authentication_data.special_admin_login:
            # This is a special backdoor that database administrators can use to get to the SQL query dialog.
            ThePubSubMachine.sendMessage(GENERATE_NEW_COMPONENT_TOPIC, component_name="SQLQueryPage")
            self.close()
            return


        # Check the response provided to see if the user id and password were good and the user was properly authenticated.
        if customer_authentication_data.authentication_success:
            NotificationDialogsLibrary.show_success_dialog("Authentication Completion", "Customer Login Successful!")

            # The user has now successfully signed in so send a request to instantiate the next component the component factory.
            params = {"customer_name" : customer_authentication_data.customer_full_name, "customer_id" : customer_authentication_data.customer_id}
            ThePubSubMachine.sendMessage(GENERATE_NEW_COMPONENT_TOPIC, component_name="CustomerMenuPage", kwargs=params)
            self.close()
        else:
            # The authentication failed. Do not indicate any more information than that for security purposes.
            NotificationDialogsLibrary.show_error_dialog("Authentication Failure", "Could not login with the credentials provided.")


    def _cancel_button_clicked(self):
        # Send a request to instantiate the next component the component factory.
        ThePubSubMachine.sendMessage(GENERATE_NEW_COMPONENT_TOPIC, component_name="WelcomePage")
        self.close()


    def _register_button_clicked(self):
        # Send a request to instantiate the next component the component factory.
        ThePubSubMachine.sendMessage(GENERATE_NEW_COMPONENT_TOPIC, component_name="RegisterNewCustomerDialog")


    def _password_recovery_button_clicked(self):
        # Send a request to instantiate the next component the component factory.
        ThePubSubMachine.sendMessage(GENERATE_NEW_COMPONENT_TOPIC, component_name="PasswordRecoveryDialog")



if __name__ == "__main__":
    # This can be used as a quick way to test this component.
    app = QApplication(sys.argv)
    controller = CustomerSignInPageController()
    controller.show()
    app.exec_()
