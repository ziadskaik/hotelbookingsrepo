"""
Created on : March 13 2021

Author : ZS

This is the GUI that appears when the employee has successfully signed in.
"""

import sys
from PyQt5.QtWidgets import (QApplication, QWidget, QComboBox, QLabel, QTableWidget, QLineEdit, QPushButton)

from pubsub import pub as ThePubSubMachine
from GenericUtilities import UILoadingLibrary
from GenericUtilities.PublishSubscribeTopics import *


class EmployeeMenuPageController(QWidget):

    def __init__(self, employee_name, employee_id, hotel_contact_id, hotel_name, hotel_address):

        QWidget.__init__(self)

        # Load the GUI using this generic function. This will look for the UI file to load first and if not found will look
        # for the generated equivalent python file. This allows for deployment but also does not require rebuilding each time the UI file is changed.
        self._messenger_employee_menu_window = UILoadingLibrary.load_gui("EmployeeMenuPage.ui", self)

        # Get the widgets from the UI and connect their signals to the slots for them as required.

        self._employee_name_label = self.findChild(QLabel, "employeeName")
        self._employee_id_label = self.findChild(QLabel, "employeeID")

        self.hotel_name_label = self.findChild(QLabel, "hotelName")
        self.hotel_address_label = self.findChild(QLabel, "hotelAddress")

        self._view_hotel_rooms_button = self.findChild(QPushButton, "viewHotelRoomsButton")
        self._view_hotel_rooms_button.clicked.connect(self._view_hotel_rooms_button_clicked)

        self._view_personal_info_button = self.findChild(QPushButton, "viewPersonalInfoButton")
        self._view_personal_info_button.clicked.connect(self._view_personal_info_button_clicked)

        self._logout_button = self.findChild(QPushButton, "logoutButton")
        self._logout_button.clicked.connect(self._logout_button_clicked)

        self.employee_name = employee_name
        self.employee_id = employee_id

        self.hotel_contact_id = hotel_contact_id
        self.hotel_name = hotel_name
        self.hotel_address = hotel_address

        self._employee_name_label.setText(self.employee_name)
        self._employee_id_label.setText(self._employee_id_label.text() + " " + str(self.employee_id))

        self.hotel_name_label.setText(self.hotel_name)
        self.hotel_address_label.setText(self.hotel_address)


    def _view_hotel_rooms_button_clicked(self):
        # Send a request to instantiate the next component the component factory.
        # Provide the hotel information to the ViewHotelRoomsPage component
        params = {"hotel_chain_contact_id": self.hotel_contact_id, "hotel_name": self.hotel_name, "hotel_full_address": self.hotel_address}
        ThePubSubMachine.sendMessage(GENERATE_NEW_COMPONENT_TOPIC, component_name="EmployeeRoomSearchPage", kwargs=params)

    def _view_personal_info_button_clicked(self):
        # Send a request to instantiate the next component the component factory.
        # Provide the employee information to the ViewPersonalInformationPage component
        params = {"employee_name": self.employee_name, "employee_id": self.employee_id}
        ThePubSubMachine.sendMessage(GENERATE_NEW_COMPONENT_TOPIC, component_name="ViewPersonalInformationPage", kwargs=params)

    def _logout_button_clicked(self):
        # Send a request to instantiate the next component the component factory.
        ThePubSubMachine.sendMessage(GENERATE_NEW_COMPONENT_TOPIC, component_name="WelcomePage")
        self.close()



if __name__ == "__main__":
    # This can be used as a quick way to test this component.
    app = QApplication(sys.argv)
    controller = EmployeeMenuPageController()
    controller.show()
    app.exec_()
