"""
Created on : March 28 2021

Author : ZS

This is the GUI for viewing present and past bookings for a customer and providing the option to cancel the booking.

"""

import sys

from PyQt5.QtWidgets import (QApplication, QWidget, QLineEdit, QPushButton, QLabel, QTableWidget)
from PyQt5.QtCore import Qt

from pubsub import pub as ThePubSubMachine

from GenericUtilities.MessageStructures import CustomerBookingsStructure, CancelCustomerBookingStructure, CustomerInformationStructure
from GenericUtilities import NotificationDialogsLibrary
from GenericUtilities import UILoadingLibrary
from GenericUtilities import EmailLibrary
from GenericUtilities.PublishSubscribeTopics import *


CURRENT_BOOKINGS_COLUMN_INDEXES_TO_COLUMN_WIDTH_MAP = {0: 80, 1: 200, 2: 350, 3: 80, 4: 80, 5: 80, 6: 265, 7: 80, 8: 150, 9: 150, 10: 150, 11:100}
PAST_BOOKINGS_COLUMN_INDEXES_TO_COLUMN_WIDTH_MAP = {0: 170, 1: 200, 2: 345, 3: 123, 4: 80, 5: 80, 6: 345, 7: 80, 8: 150, 9: 150, 10: 150, 11:150}




class CustomerViewBookingsDialogController(QWidget):

    def __init__(self, customer_name, customer_id, current_or_past_bookings):

        QWidget.__init__(self,)

        self.customer_name = customer_name
        self._customer_id = customer_id
        self.current_or_past_bookings = current_or_past_bookings

        # Load the GUI using this generic function. This will look for the UI file to load first and if not found will look
        # for the generated equivalent python file. This allows for deployment but also does not require rebuilding each time the UI file is changed.
        self._room_search_dialog = UILoadingLibrary.load_gui("CustomerViewBookingsDialog.ui", self)

        # Get the widgets from the UI and connect their signals to the slots for them as required.

        self.bookings_label = self.findChild(QLabel, "bookingsLabel")
        self.bookings_label.setText(current_or_past_bookings + " " + self.bookings_label.text())

        self.customer_name_label = self.findChild(QLabel, "customerNameLabel")
        self.customer_name_label.setText(self.customer_name)

        self.customer_id_label = self.findChild(QLabel, "customerIDLabel")
        self.customer_id_label.setText(self.customer_id_label.text() + " " + str(self._customer_id))


        self._close_button = self.findChild(QPushButton, "closeButton")
        self._close_button.clicked.connect(self._close_button_clicked)

        self._customer_view_bookings_table_widget = self.findChild(QTableWidget, "customerViewBookingsTableWidget")

        self._request_booking_info()



    def _request_booking_info(self):

        customer_booking_info_data = CustomerBookingsStructure(self._customer_id)

        if self.current_or_past_bookings == "Current":
            ThePubSubMachine.sendMessage(CUSTOMER_CURRENT_BOOKING_INFO_TOPIC, customer_booking_info_structure=customer_booking_info_data)
        else:
            ThePubSubMachine.sendMessage(CUSTOMER_PAST_BOOKING_INFO_TOPIC, customer_booking_info_structure=customer_booking_info_data)

        self._customer_view_bookings_table_widget.setRowCount(0)
        self._populate_table_widget(customer_booking_info_data)

    def _set_line_edit_in_table(self, row_index, column_index, line_edit_content, fixed_width):
        # Fill the content of 1 particular cell in the search results table.
        line_edit = QLineEdit(str(line_edit_content))
        line_edit.setAlignment(Qt.AlignCenter)
        line_edit.setReadOnly(True)
        line_edit.setFixedWidth(fixed_width)
        self._customer_view_bookings_table_widget.setCellWidget(row_index, column_index, line_edit)


    def _populate_table_widget(self, customer_booking_info_data):

        if self.current_or_past_bookings == "Past":
            map = PAST_BOOKINGS_COLUMN_INDEXES_TO_COLUMN_WIDTH_MAP
        else:
            map = CURRENT_BOOKINGS_COLUMN_INDEXES_TO_COLUMN_WIDTH_MAP

        row_index = 0
        # Iterate through all "unique" room results, fill in he column and indicate how many such rooms there are.
        for CustomerBookingInfo in customer_booking_info_data.list_of_customer_booking_info:

            # Add a new row at the bottom of the Q table.
            self._customer_view_bookings_table_widget.insertRow(row_index)

            # The first column in the table is for the booking id.
            self._set_line_edit_in_table(row_index, 0, CustomerBookingInfo.booking_id, map[0])

            # The second column in the table is for the hotel brand name.

            self._set_line_edit_in_table(row_index, 1, CustomerBookingInfo.hotel_name, map[1])

            # The third column in the table is for the hotel full address.

            self._set_line_edit_in_table(row_index, 2, CustomerBookingInfo.full_address, map[2])

            # The fourth column in the table is for total place for reserving the room for the entire amount of time. This must first be calculated.
            if self.current_or_past_bookings == "Current":

                total_reserve_duration_in_seconds = CustomerBookingInfo.reserve_end_datetime.timestamp() - CustomerBookingInfo.reserve_start_datetime.timestamp()
            else:
                total_reserve_duration_in_seconds = CustomerBookingInfo.check_out_datetime.timestamp() - CustomerBookingInfo.check_in_datetime.timestamp()

            total_number_of_days_decimal = total_reserve_duration_in_seconds/(60*60*24)  # Divide by the total number of seconds in a day to get the total number of days.
            total_number_of_days = round(total_number_of_days_decimal + 0.5001)  # Calculate the ceiling function to get the total number of nights spent in the room.
            total_cost_of_reservation = total_number_of_days*CustomerBookingInfo.price_per_night  # Calculate the total cost accordingly.

            self._set_line_edit_in_table(row_index, 3, "$ " + str(total_cost_of_reservation), map[3])

            self._set_line_edit_in_table(row_index, 4, CustomerBookingInfo.number_of_occupants, map[4])

            self._set_line_edit_in_table(row_index, 5, CustomerBookingInfo.view_type, map[5])

            list_of_amenities_string = str(CustomerBookingInfo.list_of_amenities).replace("[", "").replace("]", "").replace("'", "")
            if list_of_amenities_string == "":
                list_of_amenities_string = "(None)"
            self._set_line_edit_in_table(row_index, 6, list_of_amenities_string, map[6])

            self._set_line_edit_in_table(row_index, 7, CustomerBookingInfo.room_number, map[7])

            self._set_line_edit_in_table(row_index, 8, CustomerBookingInfo.reserve_start_datetime.strftime("%c"), map[8])

            self._set_line_edit_in_table(row_index, 9, CustomerBookingInfo.reserve_end_datetime.strftime("%c"), map[9])

            if CustomerBookingInfo.check_in_datetime is not None:
                self._set_line_edit_in_table(row_index, 10, CustomerBookingInfo.check_in_datetime.strftime("%c"), map[10])
            else:
                self._set_line_edit_in_table(row_index, 10, CustomerBookingInfo.check_in_datetime, map[10])

            if CustomerBookingInfo.check_out_datetime is not None:
                self._set_line_edit_in_table(row_index, 11, CustomerBookingInfo.check_out_datetime.strftime("%c"), map[11])
            else:
                self._set_line_edit_in_table(row_index, 11, CustomerBookingInfo.check_out_datetime, map[11])

            if self.current_or_past_bookings == "Current":
                # Each row has a "Cancel Room" push button. The customer uses this to cancel any room in the table.
                current_cancel_button = QPushButton("Cancel")
                # Generate a special slot that takes into acoout which row was pressed to handle the customer pressing the "Cancel Room button.
                current_cancel_button.clicked.connect(self._generate_cancel_booking_button_slot(row_index))
                current_cancel_button.setFixedWidth(90)
                self._customer_view_bookings_table_widget.setCellWidget(row_index, 12, current_cancel_button)
                if CustomerBookingInfo.check_in_datetime is not None:
                    self._customer_view_bookings_table_widget.cellWidget(row_index, 12).setEnabled(False)

            row_index = row_index + 1


        if self.current_or_past_bookings == "Past":
            self._customer_view_bookings_table_widget.removeColumn(12)
            self._customer_view_bookings_table_widget.removeColumn(0)


        self._customer_view_bookings_table_widget.resizeColumnsToContents()



    def _close_button_clicked(self):
        self.close()

    def _generate_cancel_booking_button_slot(self, row_index):
        # Create a slot specifically for the given row.
        booking_id = self._customer_view_bookings_table_widget.cellWidget(row_index, 0).text()

        def booking_selection_made():
            confirmation = NotificationDialogsLibrary.show_yes_or_no_dialog("Booking Cancellation Confirmation", "Confirm cancel booking for booking ID: " + str(booking_id) + "?")

            if confirmation:

                cancel_customer_booking_data = CancelCustomerBookingStructure(booking_id)

                # Send a request to the database access module to book the customer.
                ThePubSubMachine.sendMessage(CANCEL_CUSTOMER_BOOKING_TOPIC, cancel_customer_booking_structure=cancel_customer_booking_data)

                if cancel_customer_booking_data.error_message is not None:
                    NotificationDialogsLibrary.show_error_dialog("Error", cancel_customer_booking_data.error_message)
                else:
                    NotificationDialogsLibrary.show_success_dialog("Cancellation Complete"," The booking with ID: " + str(booking_id) + " has been cancelled.")
                    # Also send the customer an email to give them their cancellation info.
                    customer_info_data = CustomerInformationStructure(self._customer_id)
                    # Send the request to the database access module for it to process and provide its response by placing it in this structure.
                    ThePubSubMachine.sendMessage(GET_CUSTOMER_INFO_TOPIC, customer_info_structure=customer_info_data)
                    hotel_brand_name = self._customer_view_bookings_table_widget.cellWidget(row_index, 1).text()
                    EmailLibrary.send_booking_cancellation_confirmation_email(customer_info_data.customer_name, customer_info_data.customer_email_address, booking_id, hotel_brand_name)
                    # Now re populate the table.
                    self._request_booking_info()

        return booking_selection_made


if __name__ == "__main__":
    app = QApplication(sys.argv)
    a = CustomerViewBookingsDialogController("Ziad Skaik", 103, "Current")
    a.show()
    app.exec_()
