"""
Created on : March 14 2021

Author : KD

This is the GUI both customers and employees to reset their forgotten passwords.
"""

import sys
import random

from PyQt5.QtWidgets import (QApplication, QWidget, QLineEdit, QPushButton)

from pubsub import pub as ThePubSubMachine

from GenericUtilities.MessageStructures import VerifyEmailExistenceStructure, ResetPasswordStructure
from GenericUtilities import NotificationDialogsLibrary
from GenericUtilities import UILoadingLibrary
from GenericUtilities import EmailLibrary
from GenericUtilities.PublishSubscribeTopics import *



class PasswordRecoveryController(QWidget):

    def __init__(self):

        QWidget.__init__(self)

        # Load the GUI using this generic function. This will look for the UI file to load first and if not found will look
        # for the generated equivalent python file. This allows for deployment but also does not require rebuilding each time the UI file is changed.
        self._register_new_customer_dialog = UILoadingLibrary.load_gui("PasswordRecoveryDialog.ui", self)

        # Get the widgets from the UI and connect their signals to the slots for them as required.

        self._email_line_edit = self.findChild(QLineEdit, "emailLineEdit")
        self._verification_line_edit = self.findChild(QLineEdit, "verificationCodeLineEdit")
        self._new_password_line_edit = self.findChild(QLineEdit, "newPasswordLineEdit")
        self._confirm_password_line_edit = self.findChild(QLineEdit, "confirmPasswordLineEdit")

        self._send_code_button = self.findChild(QPushButton, "sendCodePushButton")
        self._send_code_button.clicked.connect(self._send_code_button_clicked)

        self._reset_password_button = self.findChild(QPushButton, "resetPasswordPushButton")
        self._reset_password_button.clicked.connect(self._reset_password_button_clicked)

        self._cancel_button = self.findChild(QPushButton, "cancelPushButton")
        self._cancel_button.clicked.connect(self._cancel_button_clicked)

        # This is for the generated verification code sent to the person's email and to be verified here.
        self._verification_code = None
        # This is the email attributed for the password reset. It is necessary to store it here and NOT go to the line edit once the verification code has been verified.
        self._email_for_password_reset = None


    def _send_code_button_clicked(self):
        # The user has entered their email and wants the security verification code sent to them.

        email_entered = self._email_line_edit.text()

        if email_entered == "":
            NotificationDialogsLibrary.show_error_dialog("Email not provided", "An email address must be provided.")
            return

        if email_entered.find("'") != -1:
            NotificationDialogsLibrary.show_error_dialog("Invalid Character Error", "The following character is invalid for emails: '")
            return

        verify_email_data = VerifyEmailExistenceStructure(email_entered)
        # Send the request to the database access module for it to process and put its response within the message.
        ThePubSubMachine.sendMessage(VERIFY_EMAIL_EXISTENCE_TOPIC, verify_email_existence_structure=verify_email_data)

        if verify_email_data.email_exists:
            # Generate a code, email it to the person then store the code.
            self._send_verification_code()
            NotificationDialogsLibrary.show_success_dialog("Verification Code Sent", "A verification code was sent to the email provided.")
            # Keep track of which email the reset is for - otherwise the user could change the email in the line edit and use this to hack an account.
            self._email_for_password_reset = email_entered
        else:
            NotificationDialogsLibrary.show_error_dialog("Unrecognized email", "The email provided is not registered.")


    def _reset_password_button_clicked(self):
        # The user wants to reset their password to the new password they entered.

        if not self._verify_required_fields_filled():
            return

        if not self._verify_passwords():
            return False

        if self._email_for_password_reset is None:
            NotificationDialogsLibrary.show_error_dialog("No email provided", "Must provide an email for password reset.")
            return

        verification_code_entered = self._verification_line_edit.text()

        if verification_code_entered != self._verification_code:
            NotificationDialogsLibrary.show_error_dialog("Code does not match", "The verification code provided is incorrect.")
            return

        # The user has now been authenticated.
        reset_password_data = ResetPasswordStructure(self._email_for_password_reset, self._new_password_line_edit.text())

        # Send the request to the database access module for it to process and put its response within the message.
        ThePubSubMachine.sendMessage(RESET_PASSWORD_TOPIC, reset_password_structure=reset_password_data)

        if reset_password_data.password_update_successful:
            NotificationDialogsLibrary.show_success_dialog("Password reset", "The password for the provided email was successfully reset.")
            EmailLibrary.send_password_reset_confirmation_email(self._email_for_password_reset)
            self.close()
        elif reset_password_data.error_message is not None:
            NotificationDialogsLibrary.show_error_dialog("Password reset error", reset_password_data.error_message)
        else:
            NotificationDialogsLibrary.show_error_dialog("Error", "An unknown error occurred.")


    def _send_verification_code(self):
        # Generate a random verification code.
        verification_code = str(random.randint(100001, 999999))
        # Get the user's email.
        email_entered = self._email_line_edit.text()
        # Keep track of the verification code.
        self._verification_code = verification_code
        # Send an email to the user with the verification code.
        EmailLibrary.send_password_reset_email(email_entered, verification_code)


    def _cancel_button_clicked(self):
        self.close()


    def _verify_required_fields_filled(self):

        if self._verification_line_edit.text() == "":
            NotificationDialogsLibrary.show_error_dialog("Missing Field", "Must enter the verification code provided by email.")
            return False

        if self._new_password_line_edit.text() == "" or self._confirm_password_line_edit.text() == "":
            NotificationDialogsLibrary.show_error_dialog("Missing Field", "Must enter both the new password and its confirmation.")
            return False

        return True


    def _verify_passwords(self):

        if self._new_password_line_edit.text() != self._confirm_password_line_edit.text():
            NotificationDialogsLibrary.show_error_dialog("Inconsistency Error", "The passwords entered do not match.")
            return False


        if self._new_password_line_edit.text().find("'") != -1 or self._confirm_password_line_edit.text().find("'") != -1:
            NotificationDialogsLibrary.show_error_dialog("Invalid Character Error", "The following character is invalid for passwords: '")
            return

        return True


if __name__ == "__main__":
    app = QApplication(sys.argv)
    a = PasswordRecoveryController()
    a.show()
    app.exec_()
