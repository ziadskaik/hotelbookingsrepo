"""
Created on : March 14 2021

Author : ZS

This is the GUI that appears when the employee has selected to view personal information from the EmployeeMenuPage
"""

import sys
from PyQt5.QtWidgets import (QApplication, QWidget, QComboBox, QLabel, QTableWidget, QLineEdit, QPushButton)

from pubsub import pub as ThePubSubMachine

from GenericUtilities.MessageStructures import GetPersonalInformationStructure
from GenericUtilities import UILoadingLibrary
from GenericUtilities.PublishSubscribeTopics import *

class ViewPersonalInformationPageController(QWidget):

    def __init__(self, employee_name, employee_id):

        QWidget.__init__(self)

        # Load the GUI using this generic function. This will look for the UI file to load first and if not found will look
        # for the generated equivalent python file. This allows for deployment but also does not require rebuilding each time the UI file is changed.
        self._messenger_view_personal_information_page_window = UILoadingLibrary.load_gui("ViewPersonalInformationPage.ui", self)

        # Get the widgets from the UI and connect their signals to the slots for them as required.

        self._name_label = self.findChild(QLabel, "name")
        self._sin_label = self.findChild(QLabel, "sin")

        self._address_label = self.findChild(QLabel, "address")


        self._phone_number_label = self.findChild(QLabel, "phonenumber")
        self._email_label = self.findChild(QLabel, "email")
        self._salary_label = self.findChild(QLabel, "salary")
        self._date_of_hire_label = self.findChild(QLabel, "dateofhire")

        self._close_button = self.findChild(QPushButton, "closePushButton")
        self._close_button.clicked.connect(self._close_button_clicked)

        self.name = employee_name
        self.id = employee_id
        employee_personal_information_data = GetPersonalInformationStructure(self.name, self.id)
        ThePubSubMachine.sendMessage(GET_PERSONAL_INFO_TOPIC, get_personal_info_structure=employee_personal_information_data)

        self.sin = employee_personal_information_data.sin
        self.line1 = employee_personal_information_data.line1
        self.line2 = employee_personal_information_data.line2
        self.city = employee_personal_information_data.city
        self.prov_state = employee_personal_information_data.prov_state
        self.country = employee_personal_information_data.country
        self.postal_zip = employee_personal_information_data.postal_zip
        self.phone_number = employee_personal_information_data.phone_number
        self.email = employee_personal_information_data.email
        self.salary = employee_personal_information_data.salary
        self.date_of_hire = employee_personal_information_data.date_of_hire

        self._name_label.setText(self._name_label.text() + " " + self.name)
        self._sin_label.setText(self._sin_label.text() + " " + str(self.sin))

        self._address_label.setText(self._address_label.text() + " " + self.line1 + self.line2 + ", " + self.city + ", "
                                    + self.prov_state + ", " + self.country + ", " + self.postal_zip)
        if self.phone_number is None:
            self._phone_number_label.setText(self._phone_number_label.text() + " " + "N/A")
        else:
            self._phone_number_label.setText(self._phone_number_label.text() + " " + str(self.phone_number))

        if self.email == "":
            self._email_label.setText(self._email_label.text() + " " + "N/A")
        else:
            self._email_label.setText(self._email_label.text() + " " + self.email)


        self._salary_label.setText(self._salary_label.text() + " $ " + str(self.salary))
        self._date_of_hire_label.setText(self._date_of_hire_label.text() + " " + str(self.date_of_hire))


    def _close_button_clicked(self):
        self.close()


if __name__ == "__main__":
    # This can be used as a quick way to test this component.
    app = QApplication(sys.argv)
    controller = ViewPersonalInformationPageController("Ziad", 63)
    controller.show()
    app.exec_()
