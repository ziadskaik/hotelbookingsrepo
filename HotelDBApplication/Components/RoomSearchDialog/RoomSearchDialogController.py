"""
Created on : March 15 2021

Author : KD

This is the GUI for customers to select criteria for searching for a hotel room.
"""

import sys

from PyQt5.QtWidgets import (QApplication, QWidget, QLineEdit, QPushButton, QComboBox, QDateTimeEdit, QTableWidget, QCheckBox, QTableWidgetItem)
from PyQt5.QtGui import QDoubleValidator
from PyQt5.QtCore import Qt, QDate, QTime, QDateTime

from pubsub import pub as ThePubSubMachine

from GenericUtilities.MessageStructures import HotelRoomSearchStructure
from GenericUtilities import NotificationDialogsLibrary
from GenericUtilities import UILoadingLibrary
from GenericUtilities.PublishSubscribeTopics import *



class RoomSearchDialogController(QWidget):

    def __init__(self, customer_id):

        QWidget.__init__(self,)

        # Load the GUI using this generic function. This will look for the UI file to load first and if not found will look
        # for the generated equivalent python file. This allows for deployment but also does not require rebuilding each time the UI file is changed.
        self._room_search_dialog = UILoadingLibrary.load_gui("RoomSearchDialog.ui", self)

        self.customer_id = customer_id

        # Get the widgets from the UI and connect their signals to the slots for them as required.

        self._city_combo_box = self.findChild(QComboBox, "cityComboBox")
        self._city_combo_box.currentTextChanged.connect(self._city_combo_box_current_text_changed)


        self._reserve_start_date_time_edit = self.findChild(QDateTimeEdit, "reserveStartDateTimeEdit")
        self._reserve_start_date_time_edit.setMinimumDateTime(QDateTime.currentDateTime())  # Cannot reserve into the past.
        # self._reserve_start_date_time_edit.setDateTime(QDateTime.currentDateTime())
        self._reserve_start_date_time_edit.dateTimeChanged.connect(self._reserve_start_date_time_edit_changed)


        self._reserve_end_date_time_edit = self.findChild(QDateTimeEdit, "reserveEndDateTimeEdit")
        # Set the minimum date for the checkout to tomorrow and set the time to the classical checkout time of 11 a.m.
        self._reserve_end_date_time_edit.setMinimumDate(QDate.currentDate().addDays(1))
        self._reserve_end_date_time_edit.setTime(QTime(11,0))


        # Allow only for numbers between 0 and 100 000 and with 2 decimal places for minimum and maximum costs.
        self._minimum_cost_line_edit = self.findChild(QLineEdit, "minimumCostLineEdit")
        self._minimum_cost_line_edit.setValidator(QDoubleValidator(0, 100000, 2, notation=QDoubleValidator.StandardNotation))

        self._maximum_cost_line_edit = self.findChild(QLineEdit, "maximumCostLineEdit")
        self._maximum_cost_line_edit.setValidator(QDoubleValidator(0, 100000, 2, notation=QDoubleValidator.StandardNotation))


        self._number_of_occupants_combo_box = self.findChild(QComboBox, "numberOfOccupantsComboBox")

        self._view_combo_box = self.findChild(QComboBox, "viewComboBox")

        # Amenities are shown with a table of check boxes with a vertical scrollbar which gets activated if needed.
        self._amenities_table_widget = self.findChild(QTableWidget, "amenitiesTableWidget")
        # The Q table widget is used only to hold a list of widgets. It must not "look like" a widget.
        self._amenities_table_widget.horizontalHeader().hide()
        self._amenities_table_widget.verticalHeader().hide()
        self._amenities_table_widget.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)

        self._search_button = self.findChild(QPushButton, "searchPushButton")
        self._search_button.clicked.connect(self._search_button_clicked)

        self._cancel_button = self.findChild(QPushButton, "cancelPushButton")
        self._cancel_button.clicked.connect(self._cancel_button_clicked)

        # Keep track of all the possible hotel room for every city so the search dialog can be adjusted to offer only what the selected city has.
        self._city_to_hotel_room_options_map = {}
        self._obtain_city_to_hotel_room_options_map()


    def _reserve_start_date_time_edit_changed(self, start_date_time):
        # Make sure the end reserve time cannot be set to a time that comes before the begin reserve time.
        self._reserve_end_date_time_edit.setMinimumDate(start_date_time.date().addDays(1))
        self._reserve_end_date_time_edit.setTime(QTime(11,0))


    def _city_combo_box_current_text_changed(self, city_name):

        # The user has selected a different city. The rest of the search dialog must be adjusted according to what types
        # of room that city has to offer.

        # Get the highest room capacity among all hotel room types in the city.
        maximum_capacity = self._city_to_hotel_room_options_map[city_name].maximum_capacity

        # Get all possible view types among all hotel room types in the city and sort the view list alphabetically.
        list_of_view_types = self._city_to_hotel_room_options_map[city_name].list_of_views
        list_of_view_types.sort()

        # Get all possible amenity types among all hotel room types in the city and sort the view list alphabetically.
        list_of_amenities = self._city_to_hotel_room_options_map[city_name].list_of_amenities
        list_of_amenities.sort()
        # Keep track of how many different amenities are available. This is used to populate the table.
        number_of_amenities = len(list_of_amenities)

        # Reset the view combo box to the available view values for the new city.
        self._view_combo_box.clear()
        self._view_combo_box.addItems(list_of_view_types)

        # Fill in the combo box with capacity values ranging from 1 to the capacity of the largest room in the entire city.
        range_of_capacities = list(range(1, maximum_capacity + 1))
        range_of_capacities = [str(i) for i in range_of_capacities]
        self._number_of_occupants_combo_box.clear()
        self._number_of_occupants_combo_box.addItems(range_of_capacities)

        self._amenities_table_widget.setColumnCount(1)  # This is a widget list so there is only 1 column.
        self._amenities_table_widget.setRowCount(number_of_amenities)

        # Set the Q table to have a grey background.
        style_sheet_for_a_grey_background = "background-color: rgb(240, 240, 240)"
        self._amenities_table_widget.setStyleSheet(style_sheet_for_a_grey_background)

        # This is the exact width needed to make the dialog look good.
        self._amenities_table_widget.setColumnWidth(0,232)

        # Create a Q check box for every available amenity and place the checkbox in the Q table widget.
        for i in range(0,number_of_amenities):
            current_amenity = list_of_amenities[i]
            current_check_box = QCheckBox(current_amenity)
            current_check_box.setStyleSheet(style_sheet_for_a_grey_background)  # Make the background grey as well.
            self._amenities_table_widget.setCellWidget(i,0, current_check_box)



    def _obtain_city_to_hotel_room_options_map(self):
        # Go fetch all the available room type options for every room in every city,


        # Send the request to the database access module for it to process and provide its response by filling in the map provided to it.
        ThePubSubMachine.sendMessage(OBTAIN_HOTEL_ROOM_OPTIONS_TOPIC, city_to_hotel_room_options_map=self._city_to_hotel_room_options_map)

        # The keys of the map are the city names. So the combo box can be populate with these.
        all_city_names = list(self._city_to_hotel_room_options_map.keys())
        self._city_combo_box.addItems(all_city_names)


    def _search_button_clicked(self):

        if not self._verify_minimum_and_maximum_costs():
            # The minimum and/or maximum cost entries are invalid.
            return

        # Get a structure holding the room search criteria.
        hotel_room_search_data = self._create_hotel_room_search_structure()

        # Send the request to the database access module for it to process and put its response within the message.
        ThePubSubMachine.sendMessage(SEARCH_HOTEL_ROOMS_TOPIC, hotel_room_search_structure=hotel_room_search_data)

        hotel_room_search_results = hotel_room_search_data.list_of_hotel_room_search_results

        if len(hotel_room_search_results) == 0:
            NotificationDialogsLibrary.show_success_dialog("No Match", "No results matched the search criteria provided.")
            return

        # The results have been obtained and are now ready to be presented to the customer in a new dialog.
        params = {"hotel_room_search_results": hotel_room_search_results, "customer_id": self.customer_id, "hotel_room_search_structure" : hotel_room_search_data}
        ThePubSubMachine.sendMessage(GENERATE_NEW_COMPONENT_TOPIC, component_name="CustomerRoomSearchResultsDialog", kwargs=params)


    def _verify_minimum_and_maximum_costs(self):

        minimum_cost_entered = self._minimum_cost_line_edit.text()
        maximum_cost_entered = self._maximum_cost_line_edit.text()

        if minimum_cost_entered == "" or maximum_cost_entered == "":
            # These fields are not mandatory. If left empty. it means no constraint based on price is included in the search criteria.
            return True

        if float(minimum_cost_entered) > float(maximum_cost_entered):
            NotificationDialogsLibrary.show_error_dialog("Invalid cost range", "The minimum cost cannot exceed the maximum cost!")
            return False

        return True


    def _cancel_button_clicked(self):
        self.close()



    def _create_hotel_room_search_structure(self):

        # Obtain all the search criteria from the GUI widgets and create a search request structure.

        city = self._city_combo_box.currentText()
        reserve_start_unix_epoch = self._reserve_start_date_time_edit.dateTime().toSecsSinceEpoch()
        reserve_end_unix_epoch = self._reserve_end_date_time_edit.dateTime().toSecsSinceEpoch()

        if self._minimum_cost_line_edit.text() == "":
            minimum_cost = 0
        else:
            minimum_cost = float(self._minimum_cost_line_edit.text())

        if self._maximum_cost_line_edit.text() == "":
            maximum_cost = 100000  # The maximum price search criteria.
        else:
            maximum_cost = float(self._maximum_cost_line_edit.text())

        number_of_occupants = int(self._number_of_occupants_combo_box.currentText())

        view_type = self._view_combo_box.currentText()

        # Iterate through the q table widgets for the amenity and include the amenity of the check box is checked.
        list_of_selected_amenities = []
        for i in range(0,self._amenities_table_widget.rowCount()):
            # Grab the checkbox out of the table widget.
            current_check_box = self._amenities_table_widget.cellWidget(i,0)
            if current_check_box.isChecked():
                list_of_selected_amenities.append(current_check_box.text())


        hotel_room_search_structure = HotelRoomSearchStructure(city, reserve_start_unix_epoch, reserve_end_unix_epoch, minimum_cost,
                                                               maximum_cost, number_of_occupants, view_type, list_of_selected_amenities)

        return hotel_room_search_structure



if __name__ == "__main__":
    app = QApplication(sys.argv)
    a = RoomSearchDialogController()
    a.show()
    app.exec_()
