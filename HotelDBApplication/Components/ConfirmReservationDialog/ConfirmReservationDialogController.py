"""
Created on : March 14 2021

Author : ZS

This is the GUI that appears when an employee is about to make a reservation for a customer.
"""

import sys
from PyQt5.QtWidgets import (QApplication, QWidget, QComboBox, QLabel, QTableWidget, QLineEdit, QPushButton)

from pubsub import pub as ThePubSubMachine

from GenericUtilities import UILoadingLibrary
from GenericUtilities.PublishSubscribeTopics import *
from GenericUtilities.MessageStructures import BookCustomerStructure
from GenericUtilities import NotificationDialogsLibrary


class ConfirmReservationDialogController(QWidget):

    def __init__(self, hotel_chain_contact_id, room_number, reserve_start_datetime, reserve_end_datetime, total_capacity):

        QWidget.__init__(self)

        # Load the GUI using this generic function. This will look for the UI file to load first and if not found will look
        # for the generated equivalent python file. This allows for deployment but also does not require rebuilding each time the UI file is changed.
        self._messenger_confirm_reservation_dialog = UILoadingLibrary.load_gui("ConfirmReservationDialog.ui", self)

        # Get the widgets from the UI and connect their signals to the slots for them as required.

        self._customer_identification_label = self.findChild(QLabel, "customerIdentificationLabel")
        self._customer_identification_lineEdit = self.findChild(QLineEdit, "customerIdentificationLineEdit")
        self._number_of_occupants_label = self.findChild(QLabel, "numberOfOccupantsLabel")
        self._number_of_occupants_combobox = self.findChild(QComboBox, "numberOfOccupantsComboBox")

        self._confirm_reservation_button = self.findChild(QPushButton, "confirmReservationPushButton")
        self._confirm_reservation_button.clicked.connect(self._confirm_reservation_button_clicked)

        self._cancel_button = self.findChild(QPushButton, "cancelPushButton")
        self._cancel_button.clicked.connect(self._cancel_button_clicked)

        self.hotel_chain_contact_id = hotel_chain_contact_id
        self.room_number = room_number
        self.reserve_start_datetime = reserve_start_datetime
        self.reserve_end_datetime = reserve_end_datetime
        self.total_capacity = total_capacity

        occupants_list_integers = range(1, self.total_capacity + 1)
        self._number_of_occupants_combobox.addItems([str(i) for i in occupants_list_integers])

    def _confirm_reservation_button_clicked(self):
        self.customer_identification = self._customer_identification_lineEdit.text()
        if not self.customer_identification:
            NotificationDialogsLibrary.show_error_dialog("Missing Field",
                                                         "The following field is required : Customer Identification")
            return

        self.number_of_occupants = int(self._number_of_occupants_combobox.currentText())

        book_customer_data = self._create_book_customer_structure()

        ThePubSubMachine.sendMessage(BOOK_CUSTOMER_TOPIC, book_customer_structure=book_customer_data)
        if book_customer_data.error_message is not None:
            NotificationDialogsLibrary.show_error_dialog("Error", book_customer_data.error_message)
        elif book_customer_data.booking_id is None:
            NotificationDialogsLibrary.show_error_dialog("Booking Failed!", "The booking could not be completed.")
            ThePubSubMachine.sendMessage(EMPLOYEE_BOOKING_ABORTED_TOPIC)
            self.close()
        else:
            NotificationDialogsLibrary.show_success_dialog("Booking Complete", "The customer booking ID is " + str(book_customer_data.booking_id) + ".")
            ThePubSubMachine.sendMessage(EMPLOYEE_BOOKING_COMPLETION_TOPIC)
            self.close()


    def _cancel_button_clicked(self):
        ThePubSubMachine.sendMessage(EMPLOYEE_BOOKING_ABORTED_TOPIC)
        self.close()

    def _create_book_customer_structure(self):
        # Create the message structure for all data required to book a customer.

        book_customer_structure = BookCustomerStructure(hotel_chain_contact_id=self.hotel_chain_contact_id,
                                                        room_number=self.room_number, reserve_start_datetime=self.reserve_start_datetime,
                                                        reserve_end_datetime=self.reserve_end_datetime, customer_identification=self.customer_identification,
                                                        number_of_occupants=self.number_of_occupants)
        return book_customer_structure


if __name__ == "__main__":
    # This can be used as a quick way to test this component.
    app = QApplication(sys.argv)
    controller = ConfirmReservationDialogController(1,214,"2021-03-17", "2021-03-21", 5)
    controller.show()
    app.exec_()
