"""
Created on : March 28 2021

Author : KD

This is the GUI for employees to check-out clients with.
"""

import sys

from PyQt5.QtWidgets import (QApplication, QWidget, QLineEdit, QPushButton, QLabel)
from PyQt5.QtGui import QDoubleValidator


from pubsub import pub as ThePubSubMachine

from GenericUtilities.MessageStructures import CheckOutCustomerStructure
from GenericUtilities import NotificationDialogsLibrary
from GenericUtilities import UILoadingLibrary
from GenericUtilities.PublishSubscribeTopics import *



class CheckOutConfirmationController(QWidget):

    def __init__(self, room_number, booking_id, total_cost_of_stay):

        QWidget.__init__(self)

        self._booking_id = booking_id
        self._room_number = room_number

        # This is the calculate value for the stay based on the total number of nights the customer was in the hotel for.
        self._total_cost_of_stay = float(total_cost_of_stay)

        # Load the GUI using this generic function. This will look for the UI file to load first and if not found will look
        # for the generated equivalent python file. This allows for deployment but also does not require rebuilding each time the UI file is changed.
        self._check_out_confirmation_dialog = UILoadingLibrary.load_gui("CheckOutConfirmationDialog.ui", self)

        # Get the widgets from the UI and connect their signals to the slots for them as required.

        self._customer_payment_line_edit = self.findChild(QLineEdit, "customerPaymentLineEdit")
        self._customer_payment_line_edit.setValidator(QDoubleValidator(0, 100000, 2, notation=QDoubleValidator.StandardNotation))

        self._checkout_title_label = self.findChild(QLabel, "checkoutTitleLabel")
        self._checkout_title_label.setText("Checkout for room {0} - total cost ${1}".format(room_number, total_cost_of_stay))

        self._confirm_check_out_button = self.findChild(QPushButton, "confirmCheckoutPushButton")
        self._confirm_check_out_button.clicked.connect(self._confirm_check_out_button_clicked)

        self._cancel_button = self.findChild(QPushButton, "cancelPushButton")
        self._cancel_button.clicked.connect(self._cancel_button_clicked)



    def _confirm_check_out_button_clicked(self):
        # The employee has entered the customer's payment amount and is requesting to check-out the customer.

        customer_payment_entry_text = self._customer_payment_line_edit.text()

        if customer_payment_entry_text == "":
            NotificationDialogsLibrary.show_error_dialog("Missing Field", "The customer payment is required.")
            return

        customer_payment_entry = float(customer_payment_entry_text)


        if customer_payment_entry > self._total_cost_of_stay:
            NotificationDialogsLibrary.show_error_dialog("Payment Exceeded", "The payment entered exceed the total cost of the hotel stay.")
            return

        if customer_payment_entry < self._total_cost_of_stay:
            confirmation = NotificationDialogsLibrary.show_yes_or_no_dialog("Warning", "The payment entered is less than the total cost of the hotel stay. Continue Anyway?")
            if not confirmation:
                return

        # Create a structure holding the information for check-out request.
        checkout_customer_data = CheckOutCustomerStructure(self._booking_id)

        # Send the request to the database access module for it to process and put its response within the message.
        ThePubSubMachine.sendMessage(CHECK_OUT_CUSTOMER_TOPIC, checkout_customer_structure=checkout_customer_data)

        if checkout_customer_data.error_message is not None:
            NotificationDialogsLibrary.show_error_dialog("Checkout Failure", checkout_customer_data.error_message)
        elif checkout_customer_data.check_out_successful:
            NotificationDialogsLibrary.show_success_dialog("Checkout Successful", "The checkout for room {0} was successful".format(self._room_number))
            # Let the employee room search dialog know that the room has been checked out so it can adjust itself accordingly.
            ThePubSubMachine.sendMessage(CHECK_OUT_CUSTOMER_COMPLETION_TOPIC, checked_out_room_number=self._room_number)
            self.close()
        else:
            # An unknown error has occurred.
            NotificationDialogsLibrary.show_success_dialog("Checkout Failure", "The checkout could not be completed.")


    def _cancel_button_clicked(self):
        self.close()




if __name__ == "__main__":
    app = QApplication(sys.argv)
    a = CheckOutConfirmationController()
    a.show()
    app.exec_()
