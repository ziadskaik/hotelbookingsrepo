"""
Created on : March 22 2021

Author : KD

This is the GUI for employees to search hotel rooms in the specific hotel they are working for and book rooms for customers.
"""

import sys
from datetime import datetime
import time
from enum import Enum


from PyQt5.QtWidgets import (QApplication, QWidget, QLineEdit, QComboBox, QPushButton, QLabel, QDateTimeEdit, QTableWidget, QCheckBox)
from PyQt5.QtCore import Qt, QDate, QTime, QDateTime

from pubsub import pub as ThePubSubMachine

from GenericUtilities.MessageStructures import AllHotelRoomsStructure, HotelChainCharacteristicsStructure, EmployeeHotelRoomSearchStructure, \
    AllBookedRoomsStructure, CheckInCustomerStructure
from GenericUtilities import NotificationDialogsLibrary
from GenericUtilities import UILoadingLibrary
from GenericUtilities.PublishSubscribeTopics import *


class SearchTypes(Enum):
   ALL_ROOMS = 1
   AVAILABLE_ROOMS = 2
   BOOKED_ROOMS = 3


# Store the column titles and their widths for each mode of this search dialog.
SEARCH_TYPE_TO_COLUMN_HEADERS_MAP = {
    SearchTypes.ALL_ROOMS : [("Room #", 130), ("Price Per Night", 160), ("Capacity", 150), ("Extendable", 150), ("View", 150), ("Amenities", 784-130)],
    SearchTypes.AVAILABLE_ROOMS: [("Room #", 110), ("Price Per Night", 150), ("Capacity", 150), ("Extendable", 120), ("View", 150), ("Amenities", 664-100), ("", 150)],
    SearchTypes.BOOKED_ROOMS: [("Booking ID", 80), ("Room #", 100-15), ("Price Per Night", 100), ("Capacity", 100-15), ("Extendable", 100), ("View", 150-55),
                               ("Amenities", 264-125), ("Customer Name", 115), ("Reservation Start", 150), ("Reservation End", 150), ("Check-In", 150), ("Check-Out", 150)],
}


class EmployeeRoomSearchPageController(QWidget):

    def __init__(self, hotel_chain_contact_id, hotel_name, hotel_full_address):

        QWidget.__init__(self,)

        self._hotel_chain_contact_id = hotel_chain_contact_id
        self._hotel_name = hotel_name
        self._hotel_full_address = hotel_full_address

        # Load the GUI using this generic function. This will look for the UI file to load first and if not found will look
        # for the generated equivalent python file. This allows for deployment but also does not require rebuilding each time the UI file is changed.
        self._employee_room_search_page = UILoadingLibrary.load_gui("EmployeeRoomSearchPage.ui", self)

        # Get the widgets from the UI and connect their signals to the slots for them as required.

        self._close_button = self.findChild(QPushButton, "closeButton")
        self._close_button.clicked.connect(self._close_button_clicked)

        # Flag to keep track of whether or not the user just pressed the refresh button.
        self._used_refresh_search_button = False

        self._refresh_search_button = self.findChild(QPushButton, "refreshSearchPushButton")
        self._refresh_search_button.clicked.connect(self._refresh_search_button_clicked)

        self._hotel_name_label = self.findChild(QLabel, "hotelNameLabel")
        self._hotel_name_label.setText(hotel_name)

        self._hotel_address_label = self.findChild(QLabel, "hotelAddressLabel")
        self._hotel_address_label.setText(hotel_full_address)

        self._search_results_table_widget = self.findChild(QTableWidget, "searchResultsTableWidget")

        # Get the amenities into the Q table widget.
        self._amenities_table_widget = self.findChild(QTableWidget, "amenitiesTableWidget")
        # Amenities are shown with a table of check boxes with a vertical scrollbar which gets activated if needed.
        # The Q table widget is used only to hold a list of widgets. It must not "look like" a widget.
        self._amenities_table_widget.setColumnCount(1)  # This is a widget list so there is only 1 column.
        self._amenities_table_widget.horizontalHeader().hide()
        self._amenities_table_widget.verticalHeader().hide()
        self._amenities_table_widget.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        # Set the Q table to have a grey background.
        self._amenities_table_widget.setStyleSheet("background-color: rgb(240, 240, 240)")


        self._reserve_start_date_time_edit = self.findChild(QDateTimeEdit, "reserveStartDateTimeEdit")
        self._reserve_start_date_time_edit.setMinimumDateTime(QDateTime.currentDateTime())  # Cannot reserve into the past.
        self._reserve_start_date_time_edit.dateTimeChanged.connect(self._reserve_start_date_time_edit_changed)

        self._reserve_end_date_time_edit = self.findChild(QDateTimeEdit, "reserveEndDateTimeEdit")
        # Set the minimum date for the checkout to tomorrow and set the time to the classical checkout time of 11 a.m.
        self._reserve_end_date_time_edit.setMinimumDate(QDate.currentDate().addDays(1))
        self._reserve_end_date_time_edit.setTime(QTime(11,0))

        self._room_status_combo_box = self.findChild(QComboBox, "roomStatusComboBox")
        self._view_combo_box = self.findChild(QComboBox, "viewComboBox")
        self._capacity_combo_box = self.findChild(QComboBox, "capacityComboBox")
        self._cost_range_combo_box = self.findChild(QComboBox, "costRangeComboBox")

        # A map to keep track of the search modes and their respective string representations.
        self._room_search_string_to_enum_type_map = {}

        for search_type_reference in SearchTypes:
            search_type_string = search_type_reference.name.split("_")[0].capitalize() + " " + search_type_reference.name.split("_")[1].capitalize()
            self._room_search_string_to_enum_type_map[search_type_string] = search_type_reference

        # Must populate before connecting the signals to the slots to prevent triggers in those at the beginning.
        self._populate_search_widgets()

        # Connect to get the changes in the combo box search criteria.
        self._room_status_combo_box.currentTextChanged.connect(self._room_status_combo_box_current_text_changed)
        self._view_combo_box.currentTextChanged.connect(self._view_combo_box_current_text_changed)
        self._capacity_combo_box.currentTextChanged.connect(self._capacity_combo_box_current_text_changed)
        self._cost_range_combo_box.currentTextChanged.connect(self._cost_range_combo_box_current_text_changed)

        # Trigger the initial search for all rooms in the hotel.
        self._update_search_results()

        # Keep track of index so the row can be un-highlighted if employee aborts a booking reservation.
        self._currently_highlighted_row_index = None

        # Keep track of index for which row the employee may be completing a check-out for.
        self._current_row_index_for_checkout_processing = None

        # Get notifications for when a booking reservation is made.
        ThePubSubMachine.subscribe(self._update_search_results, EMPLOYEE_BOOKING_COMPLETION_TOPIC)
        # Get notifications for when a booking reservation is aborted.
        ThePubSubMachine.subscribe(self._un_highlight_rows, EMPLOYEE_BOOKING_ABORTED_TOPIC)
        # Get notifications for when a renting is over as a customer has just checked out.
        ThePubSubMachine.subscribe(self._update_from_checkout_complete, CHECK_OUT_CUSTOMER_COMPLETION_TOPIC)


    def _reserve_start_date_time_edit_changed(self, start_date_time):
        # Make sure the end reserve time cannot be set to a time that comes before the begin reserve time.
        self._reserve_end_date_time_edit.setMinimumDate(start_date_time.date().addDays(1))
        self._reserve_end_date_time_edit.setTime(QTime(11,0))


    def _room_status_combo_box_current_text_changed(self, room_status):
        self._update_search_results()

    def _view_combo_box_current_text_changed(self, view):
        self._update_search_results()

    def _capacity_combo_box_current_text_changed(self, capacity):
        self._update_search_results()

    def _cost_range_combo_box_current_text_changed(self, cost_range):
        self._update_search_results()

    def _refresh_search_button_clicked(self):
        self._used_refresh_search_button = True
        self._update_search_results()

    def _un_highlight_rows(self):
        if self._currently_highlighted_row_index is not None:
            self._set_row_highlighted(self._currently_highlighted_row_index, False)


    def _update_search_results(self):
        # Obtain the main search results table widget depending on the current room search mode.

        room_status = self._room_status_combo_box.currentText()

        room_type_reference = self._room_search_string_to_enum_type_map[room_status]

        # Reset the entire table
        self._search_results_table_widget.clear()
        self._search_results_table_widget.setRowCount(0)

        # First we need to set all the column in the table accordingly. Both the column titles and their sizes.
        table_headers_info = SEARCH_TYPE_TO_COLUMN_HEADERS_MAP[room_type_reference]
        self._search_results_table_widget.setColumnCount(len(table_headers_info))

        table_headers = []
        column_index = 0
        # Set the width of every column as it is predefined.
        for column_title, column_width in table_headers_info:
            table_headers.append(column_title)
            self._search_results_table_widget.setColumnWidth(column_index, int(column_width))
            column_index = column_index + 1

        self._search_results_table_widget.setHorizontalHeaderLabels(table_headers)


        if room_type_reference == SearchTypes.ALL_ROOMS:
            self._set_search_widget_enabled_states(False)
            self._search_and_populate_all_rooms()
        elif room_type_reference == SearchTypes.AVAILABLE_ROOMS:
            # The search widgets are only accessible when employee is searching for a potential room for a client.
            self._set_search_widget_enabled_states(True)
            self._search_and_populate_all_available_rooms()
        elif room_type_reference == SearchTypes.BOOKED_ROOMS:
            self._set_search_widget_enabled_states(False)
            self._search_and_populate_all_booked_rooms()


    def _search_and_populate_all_rooms(self):

        # Get a structure holding the information about all the rooms in the hotel.
        all_hotel_rooms_data = AllHotelRoomsStructure(self._hotel_chain_contact_id)

        # Send the request to the database access module for it to process and put its response within the message.
        ThePubSubMachine.sendMessage(ALL_HOTEL_ROOMS_IN_CHAIN_TOPIC, all_hotel_rooms_structure=all_hotel_rooms_data)

        row_index = 0
        for specific_hotel_room_info in all_hotel_rooms_data.list_of_specific_hotel_room_info:

            # Add a new row at the bottom of the Q table.
            self._search_results_table_widget.insertRow(row_index)

            # The first column in the table is for the hotel room number.
            self._set_line_edit_in_table(row_index, 0, specific_hotel_room_info.room_number)

            # The second column in the table is for the price per night.
            self._set_line_edit_in_table(row_index, 1, "$ " + str(specific_hotel_room_info.price))

            # The third column in the table is for the room capacity.
            self._set_line_edit_in_table(row_index, 2, specific_hotel_room_info.capacity)

            # The fourth column in the table is for the whether or not the room can be extended.
            self._set_line_edit_in_table(row_index, 3, specific_hotel_room_info.extendable)

            # The fifth column in the table is for the view type.
            self._set_line_edit_in_table(row_index, 4, specific_hotel_room_info.view_type)

            # The sixth column in the table is for the list of amenities.
            # Convert the list to a human readable string with commas.
            list_of_amenities_string = str(specific_hotel_room_info.list_of_amenities).replace("[", "").replace("]", "").replace("'", "")
            if list_of_amenities_string == "":
                list_of_amenities_string = "(None)"
            self._set_line_edit_in_table(row_index, 5, list_of_amenities_string)

            row_index = row_index + 1


    def _search_and_populate_all_available_rooms(self):

        # Get a structure holding the information about all the rooms in the hotel.
        employee_hotel_room_search_data = self._create_employee_hotel_room_search_structure()

        # Send the request to the database access module for it to process and put its response within the message.
        ThePubSubMachine.sendMessage(AVAILABLE_HOTEL_ROOMS_IN_CHAIN_TOPIC, employee_hotel_room_search_structure=employee_hotel_room_search_data)

        hotel_rooms_found = employee_hotel_room_search_data.list_of_hotel_room_search_results
        number_of_rooms_found = len(hotel_rooms_found)

        if number_of_rooms_found == 0 and self._used_refresh_search_button:
            NotificationDialogsLibrary.show_success_dialog("No Match", "No results matched the search criteria provided.")
            self._used_refresh_search_button = False
            return


        row_index = 0
        for specific_hotel_room_info in hotel_rooms_found:

            # Add a new row at the bottom of the Q table.
            self._search_results_table_widget.insertRow(row_index)

            # The first column in the table is for the hotel room number.
            self._set_line_edit_in_table(row_index, 0, specific_hotel_room_info.room_number)

            # The second column in the table is for the price per night.
            self._set_line_edit_in_table(row_index, 1, "$ " + str(specific_hotel_room_info.price))

            # The third column in the table is for the room capacity.
            self._set_line_edit_in_table(row_index, 2, specific_hotel_room_info.capacity)

            # The fourth column in the table is for the whether or not the room can be extended.
            self._set_line_edit_in_table(row_index, 3, specific_hotel_room_info.extendable)

            # The fifth column in the table is for the view type.
            self._set_line_edit_in_table(row_index, 4, specific_hotel_room_info.view_type)

            # The sixth column in the table is for the list of amenities.
            # Convert the list to a human readable string with commas.
            list_of_amenities_string = str(specific_hotel_room_info.list_of_amenities).replace("[", "").replace("]", "").replace("'", "")
            if list_of_amenities_string == "":
                list_of_amenities_string = "(None)"
            self._set_line_edit_in_table(row_index, 5, list_of_amenities_string)

            # Each row has a "Book Room" push button. The customer uses this to book any room in the table.
            current_booking_button = QPushButton("Book Room")
            # Generate a special slot that takes into account which row was pressed to handle the customer pressing the "Book Room button.
            current_booking_button.clicked.connect(self._generate_booking_button_slot(row_index))
            self._search_results_table_widget.setCellWidget(row_index, 6, current_booking_button)

            row_index = row_index + 1


    def _search_and_populate_all_booked_rooms(self):

        # Create a structure holding the information about all the booked rooms in the hotel.
        all_booked_hotel_rooms_data = AllBookedRoomsStructure(self._hotel_chain_contact_id)

        # Send the request to the database access module for it to process and put its response within the message.
        ThePubSubMachine.sendMessage(ALL_BOOKED_HOTEL_ROOMS_IN_CHAIN_TOPIC, all_booked_hotel_rooms_structure=all_booked_hotel_rooms_data)

        hotel_bookings_found = all_booked_hotel_rooms_data.list_of_specific_hotel_room_bookings

        row_index = 0
        for specific_hotel_room_booking in hotel_bookings_found:

            # Add a new row at the bottom of the Q table.
            self._search_results_table_widget.insertRow(row_index)

            # The first column in the table is for the booking ID.
            self._set_line_edit_in_table(row_index, 0, specific_hotel_room_booking.booking_id)

            # The second column in the table is for the hotel room number.
            self._set_line_edit_in_table(row_index, 1, specific_hotel_room_booking.room_number)

            # The third column in the table is for the price per night.
            self._set_line_edit_in_table(row_index, 2, "$ " + str(specific_hotel_room_booking.price))

            # The fourth column in the table is for the room capacity.
            self._set_line_edit_in_table(row_index, 3, specific_hotel_room_booking.capacity)

            # The fifth column in the table is for the whether or not the room can be extended.
            self._set_line_edit_in_table(row_index, 4, specific_hotel_room_booking.extendable)

            # The sixth column in the table is for the view type.
            self._set_line_edit_in_table(row_index, 5, specific_hotel_room_booking.view_type)

            # The seventh column in the table is for the list of amenities.
            # Convert the list to a human readable string with commas.
            list_of_amenities_string = str(specific_hotel_room_booking.list_of_amenities).replace("[", "").replace("]", "").replace("'", "")
            if list_of_amenities_string == "":
                list_of_amenities_string = "(None)"
            self._set_line_edit_in_table(row_index, 6, list_of_amenities_string)

            # The eight column in the table is for the customer's full name.
            self._set_line_edit_in_table(row_index, 7, specific_hotel_room_booking.customer_full_name)

            # The ninth column in the table is for the reservation start date time.
            more_readable_start_time = specific_hotel_room_booking.reserve_start_datetime.strftime("%c")
            self._set_line_edit_in_table(row_index, 8, more_readable_start_time)

            # The tenth column in the table is for the reservation end date time.
            more_readable_end_time = specific_hotel_room_booking.reserve_end_datetime.strftime("%c")
            self._set_line_edit_in_table(row_index, 9, more_readable_end_time)

            # The eleventh row is either the check-in datetime if there is one or a button that offers check-in capability.
            check_in_date_time = specific_hotel_room_booking.check_in_datetime
            if check_in_date_time is not None:
                more_readable_check_in_time = check_in_date_time.strftime("%c")
                self._set_line_edit_in_table(row_index, 10, more_readable_check_in_time)
            else:
                current_check_in_button = QPushButton("Check-In Customer")
                # Generate a special slot that takes into account which row was pressed to handle the employee pressing the button to check-in the customer.
                current_check_in_button.clicked.connect(self._generate_check_in_button_slot(row_index))
                self._search_results_table_widget.setCellWidget(row_index, 10, current_check_in_button)

            # The twelfth row is either the check-in datetime if there is one or a button that offers check-out capability.
            check_out_date_time = specific_hotel_room_booking.check_out_datetime
            if check_out_date_time is not None:
                more_readable_check_out_time = check_out_date_time.strftime("%c")
                self._set_line_edit_in_table(row_index, 11, more_readable_check_out_time)
            elif check_in_date_time is None:
                self._set_line_edit_in_table(row_index, 11, check_in_date_time)
            else:
                current_check_out_button = QPushButton("Check-Out Customer")
                # Generate a special slot that takes into account which row was pressed to handle the employee pressing the button to check-in the customer.
                current_check_out_button.clicked.connect(self._generate_check_out_button_slot(row_index))
                self._search_results_table_widget.setCellWidget(row_index, 11, current_check_out_button)

            row_index = row_index + 1


    def _create_employee_hotel_room_search_structure(self):
        # Create a structure to hold the search request and search results of a room (in the current specific hotel).

        reserve_start_unix_epoch = self._reserve_start_date_time_edit.dateTime().toSecsSinceEpoch()
        reserve_end_unix_epoch = self._reserve_end_date_time_edit.dateTime().toSecsSinceEpoch()

        cost_range_selection = self._cost_range_combo_box.currentText()
        if cost_range_selection == "Any":
            minimum_cost = 0
            maximum_cost = 100000  # The maximum price search criteria.
        elif cost_range_selection.find("-") != -1:    # "$ {0} - {1}"   "$ {0}+"
            minimum_cost = int(cost_range_selection.split("-")[0].replace("$","").replace(" ",""))
            maximum_cost = int(cost_range_selection.split("-")[1].replace(" ",""))
        else:
            minimum_cost = int(cost_range_selection.replace("$","").replace("+","").replace(" ",""))
            maximum_cost = 100000  # The maximum price search criteria.

        capacity = self._capacity_combo_box.currentText()

        view_selection = self._view_combo_box.currentText()
        if view_selection == "Any":
            view_types = []
            for index in range(1, self._view_combo_box.count()):
                view_types.append( self._view_combo_box.itemText(index))
        else:
            view_types = [view_selection]

        # Iterate through the q table widgets for the amenity and include the amenity of the check box is checked.
        list_of_selected_amenities = []
        for i in range(0,self._amenities_table_widget.rowCount()):
            # Grab the checkbox out of the table widget.
            current_check_box = self._amenities_table_widget.cellWidget(i,0)
            if current_check_box.isChecked():
                list_of_selected_amenities.append(current_check_box.text())

        # Get a structure holding the information about all the rooms in the hotel.
        employee_hotel_room_search_struct = EmployeeHotelRoomSearchStructure(self._hotel_chain_contact_id, reserve_start_unix_epoch, reserve_end_unix_epoch,
                                                                             minimum_cost, maximum_cost, capacity, view_types, list_of_selected_amenities)

        return employee_hotel_room_search_struct


    def _populate_search_widgets(self):

        # Get a structure holding the available characteristics for the specific hotel chain.
        hotel_chain_characteristics_data = HotelChainCharacteristicsStructure(self._hotel_chain_contact_id)

        # Send the request to the database access module for it to process and put its response within the message.
        ThePubSubMachine.sendMessage(HOTEL_CHAIN_CHARACTERISTICS_TOPIC, hotel_chain_characteristics_structure=hotel_chain_characteristics_data)

        # All the room characteristics for the specific hotel have now been obtained.

        # Include the option to search for any view type.
        self._view_combo_box.addItem("Any")
        self._view_combo_box.addItems(hotel_chain_characteristics_data.list_of_views)

        # Fill in the combo box with capacity values ranging from 1 to the capacity of the largest room in the entire city.
        range_of_capacities = list(range(1, hotel_chain_characteristics_data.maximum_capacity + 1))
        range_of_capacities = [str(i) for i in range_of_capacities]
        self._capacity_combo_box.clear()
        self._capacity_combo_box.addItems(range_of_capacities)

        self._cost_range_combo_box.addItem("Any")
        quarter_maximum_cost = round(hotel_chain_characteristics_data.maximum_cost / 4)
        self._cost_range_combo_box.addItem("$ {0} - {1}".format(0, quarter_maximum_cost))
        self._cost_range_combo_box.addItem("$ {0} - {1}".format(quarter_maximum_cost, quarter_maximum_cost*2))
        self._cost_range_combo_box.addItem("$ {0} - {1}".format(quarter_maximum_cost*2, quarter_maximum_cost*3))
        self._cost_range_combo_box.addItem("$ {0}+".format(quarter_maximum_cost*3))


        number_of_amenities = len(hotel_chain_characteristics_data.list_of_amenities)

        self._amenities_table_widget.setColumnCount(1)  # This is a widget list so there is only 1 column.
        self._amenities_table_widget.setRowCount(number_of_amenities)
        self._amenities_table_widget.setColumnWidth(0, 235)

        # Create a Q check box for every available amenity and place the checkbox in the Q table widget.
        for i in range(0, number_of_amenities):
            current_amenity = hotel_chain_characteristics_data.list_of_amenities[i]
            current_check_box = QCheckBox(current_amenity)
            current_check_box.stateChanged.connect(self._update_search_results)
            # current_check_box.setStyleSheet(self._style_sheet_for_a_grey_background)  # Make the background grey as well.
            self._amenities_table_widget.setCellWidget(i,0, current_check_box)

        # Populate the room status combo-box
        for search_type_string in self._room_search_string_to_enum_type_map.keys():
            self._room_status_combo_box.addItem(search_type_string)



    def _set_line_edit_in_table(self, row_index, column_index, line_edit_content):
        # Fill the content of 1 particular cell in the search results table.
        line_edit = QLineEdit(str(line_edit_content))
        line_edit.setAlignment(Qt.AlignCenter)
        line_edit.setFixedWidth(self._search_results_table_widget.columnWidth(column_index))
        line_edit.setReadOnly(True)
        self._search_results_table_widget.setCellWidget(row_index, column_index, line_edit)


    def _close_button_clicked(self):
        self.close()

    def _generate_booking_button_slot(self, row_index):
        # Create a slot specifically for the given row.

        def booking_selection_made():

            # This is for if the user closes the confirmation dialog by the X button rather than the close button.
            self._un_highlight_rows()

            # Highlight the selected row so the user can clearly see it.
            # self._search_results_table_widget.selectRow(row_index)
            self._set_row_highlighted(row_index, True)
            self._currently_highlighted_row_index = row_index

            # Get the necessary values right out of the table widget rather than duplicating the data.
            room_number = self._search_results_table_widget.cellWidget(row_index, 0).text()
            reserve_start_date_timestamp = self._reserve_start_date_time_edit.dateTime().toString("yyyy-MM-dd hh:mm:ss")
            reserve_end_date_timestamp = self._reserve_end_date_time_edit.dateTime().toString("yyyy-MM-dd hh:mm:ss")

            capacity = self._search_results_table_widget.cellWidget(row_index, 2).text()
            extendable = self._search_results_table_widget.cellWidget(row_index, 3).text()

            if extendable == 'True':
                effective_capacity = int(capacity) + 1
            else:
                effective_capacity = int(capacity)

            params = {"hotel_chain_contact_id": self._hotel_chain_contact_id,
                      "room_number": room_number,
                      "reserve_start_datetime": reserve_start_date_timestamp, "reserve_end_datetime": reserve_end_date_timestamp, "total_capacity": effective_capacity}
            ThePubSubMachine.sendMessage(GENERATE_NEW_COMPONENT_TOPIC, component_name="ConfirmReservationDialog", kwargs=params)

        return booking_selection_made


    def _generate_check_in_button_slot(self, row_index):
        # Create a slot specifically for the given row.

        def process_check_in_request():

            # Get the necessary values right out of the table widget rather than duplicating the data.
            room_number = self._search_results_table_widget.cellWidget(row_index, 1).text()
            booking_id = self._search_results_table_widget.cellWidget(row_index, 0).text()

            confirmation = NotificationDialogsLibrary.show_yes_or_no_dialog("Check-In Confirmation", "Confirm check-in for room " + str(room_number) + "?")

            if confirmation:
                check_in_customer_data = CheckInCustomerStructure(booking_id)
                ThePubSubMachine.sendMessage(CHECK_IN_CUSTOMER_TOPIC, check_in_customer_structure=check_in_customer_data)

                if check_in_customer_data.error_message is not None:
                    NotificationDialogsLibrary.show_error_dialog("Error", check_in_customer_data.error_message)
                    return

                if check_in_customer_data.check_in_date_time is not None:
                    NotificationDialogsLibrary.show_success_dialog("Check-In Successful", "Check-In for room {0} successful!".format(room_number))
                    # Replace the 'Check-In' button by the datetime the checking was made
                    self._set_line_edit_in_table(row_index, 10, check_in_customer_data.check_in_date_time.strftime("%c"))
                    #Provide a 'Check-out' button.
                    current_check_out_button = QPushButton("Check-Out Customer")
                    # Generate a special slot that takes into account which row was pressed to handle the employee pressing the button to check-in the customer.
                    current_check_out_button.clicked.connect(self._generate_check_out_button_slot(row_index))
                    self._search_results_table_widget.setCellWidget(row_index, 11, current_check_out_button)
                else:
                    NotificationDialogsLibrary.show_error_dialog("Error", "Check-In for room {0} could not be completed.".format(room_number))

        return process_check_in_request


    def _generate_check_out_button_slot(self, row_index):
        # Create a slot specifically for the given row.

        def process_check_out_request():

            booking_id = self._search_results_table_widget.cellWidget(row_index, 0).text()
            self._current_row_index_for_checkout_processing = row_index

            # Get the necessary values right out of the table widget rather than duplicating the data.
            room_number = self._search_results_table_widget.cellWidget(row_index, 1).text()
            price_per_night = self._search_results_table_widget.cellWidget(row_index, 2).text().replace("$ ", "")
            check_in_date_time = self._search_results_table_widget.cellWidget(row_index, 10).text()

            check_in_time_unix_epoch = datetime.strptime(check_in_date_time, "%a %b %d %H:%M:%S %Y").timestamp()
            current_time_unix_epoch = time.time()

            # Calculate the cost for the total number of nights the customer stayed in the room for.
            total_reserve_duration_in_seconds = current_time_unix_epoch - check_in_time_unix_epoch
            total_number_of_days_decimal = total_reserve_duration_in_seconds/(60*60*24)  # Divide by the total number of seconds in a day to get the total number of days.
            total_number_of_days = round(total_number_of_days_decimal + 0.5001)  # Calculate the ceiling function to get the total number of nights spent in the room.
            total_cost_of_stay = total_number_of_days*float(price_per_night)  # Calculate the total cost accordingly.

            # Send a request to instantiate the next component the component factory.
            params = {"room_number": room_number, "booking_id" : booking_id, "total_cost_of_stay" : total_cost_of_stay}
            ThePubSubMachine.sendMessage(GENERATE_NEW_COMPONENT_TOPIC, component_name="CheckOutConfirmationDialog", kwargs=params)

        return process_check_out_request


    def _update_from_checkout_complete(self, checked_out_room_number):
        # The given room was just checked out so the search table can no longer display this room since it is now in the archives.

        # table_room_number = self._search_results_table_widget.cellWidget(self._current_row_index_for_checkout_processing, 1).text()
        #
        # if table_room_number == checked_out_room_number:
        #     self._search_results_table_widget.removeRow(self._current_row_index_for_checkout_processing)

        # Must update completely otherwise row numbers will no longer match up - so we just refresh the whole table.
        self._update_search_results()


    def _set_row_highlighted(self, row_index, is_highlight):
        # Highlight or un-highlight a specific row in green to show it is the selected row.

        number_of_columns = self._search_results_table_widget.columnCount()

        for column_index in range(0, number_of_columns):
            current_cell_widget = self._search_results_table_widget.cellWidget(row_index, column_index)

            if type(current_cell_widget) != QLineEdit:
                # Skip over the push buttons, if any.
                continue

            if is_highlight:
                current_cell_widget.setStyleSheet("QLineEdit {background-color: green;}")
            else:
                current_cell_widget.setStyleSheet("QLineEdit {background-color: white;}")


    def _set_search_widget_enabled_states(self, enabled_state):
        # These widgets are only available when the employee is in the mode to search for a room for a customer.
        self._view_combo_box.setEnabled(enabled_state)
        self._capacity_combo_box.setEnabled(enabled_state)
        self._cost_range_combo_box.setEnabled(enabled_state)
        self._amenities_table_widget.setEnabled(enabled_state)
        self._refresh_search_button.setEnabled(enabled_state)
        self._reserve_start_date_time_edit.setEnabled(enabled_state)
        self._reserve_end_date_time_edit.setEnabled(enabled_state)



if __name__ == "__main__":
    app = QApplication(sys.argv)
    a = EmployeeRoomSearchPageController()
    a.show()
    app.exec_()
