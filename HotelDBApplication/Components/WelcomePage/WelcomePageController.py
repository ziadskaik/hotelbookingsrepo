"""
Created on : March 6 2021

Author : KD

This is the first GUI that comes up when the application is launched.
"""

import sys
from PyQt5.QtWidgets import (QApplication, QWidget, QComboBox, QLabel, QTableWidget, QLineEdit, QPushButton)

from pubsub import pub as ThePubSubMachine

from GenericUtilities import UILoadingLibrary
from GenericUtilities.PublishSubscribeTopics import *



class WelcomePageController(QWidget):

    def __init__(self):

        QWidget.__init__(self)

        # Load the GUI using this generic function. This will look for the UI file to load first and if not found will look
        # for the generated equivalent python file. This allows for deployment but also does not require rebuilding each time the UI file is changed.
        self._messenger_main_menu_window = UILoadingLibrary.load_gui("WelcomePage.ui", self)

        # Get the widgets from the UI and connect their signals to the slots for them as required.

        self._employee_login_button = self.findChild(QPushButton, "employeeLoginPushButton")
        self._employee_login_button.clicked.connect(self._employee_login_button_clicked)

        self._customer_login_button = self.findChild(QPushButton, "customerLoginPushButton")
        self._customer_login_button.clicked.connect(self._customer_login_button_clicked)


    def _employee_login_button_clicked(self):
        # Send a request to instantiate the next component the component factory.
        ThePubSubMachine.sendMessage(GENERATE_NEW_COMPONENT_TOPIC, component_name="EmployeeSignInPage")
        self.close()  # This component is no longer necessary so it gets closed.

    def _customer_login_button_clicked(self):
        # Send a request to instantiate the next component the component factory.
        ThePubSubMachine.sendMessage(GENERATE_NEW_COMPONENT_TOPIC, component_name="CustomerSignInPage")
        self.close()  # This component is no longer necessary so it gets closed.





if __name__ == "__main__":
    # This can be used as a quick way to test this component.
    app = QApplication(sys.argv)
    controller = WelcomePageController()
    controller.show()
    app.exec_()
