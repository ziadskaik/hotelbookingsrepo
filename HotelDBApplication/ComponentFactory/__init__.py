"""
Created on : March 2 2021

Author : KD

This is the factory responsible for instantiating new components. This is the only way new components should be instantiated.
This way components and service modules remain modular and independent - components should NEVER import one another directly.
"""
