"""
Created on : March 2 2021

Author : KD

This is the factory responsible for instantiating new components. This is the only way new components should be instantiated.
This way components and service modules remain modular and independent - components should NEVER import one another directly.
"""


from pubsub import pub as ThePubSubMachine
from GenericUtilities.PublishSubscribeTopics import *

# All component controller classes must be listed here so this component factory "knows" about them.
from Components.WelcomePage.WelcomePageController import WelcomePageController
from Components.CustomerSignInPage.CustomerSignInPageController import CustomerSignInPageController
from Components.RegisterNewCustomerDialog.RegisterNewCustomerController import RegisterNewCustomerController
from Components.CustomerMenuPage.CustomerMenuPageController import CustomerMenuPageController
from Components.PasswordRecoveryDialog.PasswordRecoveryController import PasswordRecoveryController
from Components.EmployeeMenuPage.EmployeeMenuPageController import EmployeeMenuPageController
from Components.EmployeeSignInPage.EmployeeSignInPageController import EmployeeSignInPageController
from Components.ViewPersonalInformationPage.ViewPersonalInformationPageController import ViewPersonalInformationPageController
from Components.SelectHotelDialog.SelectHotelDialogController import SelectHotelDialogController
from Components.ConfirmReservationDialog.ConfirmReservationDialogController import ConfirmReservationDialogController
from Components.RoomSearchDialog.RoomSearchDialogController import RoomSearchDialogController
from Components.CustomerRoomSearchResultsDialog.CustomerRoomSearchResultsDialogController import CustomerRoomSearchResultsDialogController
from Components.SQLQueryPage.SQLQueryPageController import SQLQueryPageController
from Components.EmployeeRoomSearchPage.EmployeeRoomSearchPageController import EmployeeRoomSearchPageController
from Components.CheckOutConfirmationDialog.CheckOutConfirmationController import CheckOutConfirmationController
from Components.CustomerViewBookingsDialog.CustomerViewBookingsDialogController import CustomerViewBookingsDialogController




# Component names and their respective controller classes should also be listed here so the factory can easily map them upon receiving instantiation requests.
COMPONENT_NAME_TO_CLASS_MAP = {
    "WelcomePage" : WelcomePageController,
    "CustomerSignInPage" : CustomerSignInPageController,
    "RegisterNewCustomerDialog" : RegisterNewCustomerController,
    "CustomerMenuPage" : CustomerMenuPageController,
    "PasswordRecoveryDialog" : PasswordRecoveryController,
    "EmployeeMenuPage" : EmployeeMenuPageController,
    "EmployeeSignInPage" : EmployeeSignInPageController,
    "ViewPersonalInformationPage" : ViewPersonalInformationPageController,
    "SelectHotelDialog" : SelectHotelDialogController,
    "ConfirmReservationDialog" : ConfirmReservationDialogController,
    "RoomSearchDialog" : RoomSearchDialogController,
    "CustomerRoomSearchResultsDialog" : CustomerRoomSearchResultsDialogController,
    "SQLQueryPage": SQLQueryPageController,
    "EmployeeRoomSearchPage": EmployeeRoomSearchPageController,
    "CustomerViewBookingsDialog": CustomerViewBookingsDialogController,
    "CheckOutConfirmationDialog": CheckOutConfirmationController,
}


# Must keep a list of references to each instantiated component otherwise the instance has no references and is destroyed by the garbage collector.
m_active_component_instances = []


def generate_new_component_instance(component_name, kwargs=None):
    # Instantiate a new component of the given name and with the given parameters (if any).

    if component_name in COMPONENT_NAME_TO_CLASS_MAP:
        # This factory "recognises" the requested component.

        # Get the reference to the class so it can be instantiated and hence the component be instantiated.
        reference_to_class_to_instantiate = COMPONENT_NAME_TO_CLASS_MAP[component_name]

        if kwargs is None:
            # No parameters provided so just use an empty dictionary since a dictionary is required.
            kwargs = {}

        # The component's controller class can now be instantiated with its parameters.
        component_class_instance = reference_to_class_to_instantiate(**kwargs)

        # Keep a reference to the component instance in memory to prevent garbage collection from destroying it.
        m_active_component_instances.append(component_class_instance)

    else:
        # The component does not exist. It likely just has not been developed yet.
        print("Unknown component : " + str(component_name))



# Subscribe to receive all requests to instantiate new components.
ThePubSubMachine.subscribe(generate_new_component_instance, GENERATE_NEW_COMPONENT_TOPIC)