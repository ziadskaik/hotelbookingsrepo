"""
Created on : March 9 2021

Author : KD
"""


import os
import shutil
import time

# This value varies depending on which user machine this runs on.
PATH_TO_GUI_BUILD_SCRIPT = "C:\\Users\\Home\\AppData\\Local\\Programs\\Python\\Python39\\Scripts\\pyuic5"

GENERATED_GUI_PACKAGE_NAME = "PyEquivalentUIFiles"
PATH_TO_GENERATED_GUI_PACKAGE = os.getcwd() + os.path.sep + GENERATED_GUI_PACKAGE_NAME

COMPONENT_DIRECTORY_NAME = "Components"
ABSOLUTE_PATH_TO_COMPONENT_PACKAGE = os.path.abspath(COMPONENT_DIRECTORY_NAME)

RUNTIME_LOOKUP_MODULE_NAME = "gui_name_to_class_reference_map"
RUNTIME_LOOKUP_MAP_NAME = RUNTIME_LOOKUP_MODULE_NAME.upper()
PATH_TO_RUNTIME_LOOKUP_MODULE = PATH_TO_GENERATED_GUI_PACKAGE + os.path.sep + RUNTIME_LOOKUP_MODULE_NAME + ".py"


def get_paths_to_all_ui_files_in_project():

    list_of_paths = []

    for root, dirs, files in os.walk(ABSOLUTE_PATH_TO_COMPONENT_PACKAGE):
        for file_name in files:
            if file_name.split(".")[1] == "ui":
                relative_path = os.path.join(root, file_name)
                absolute_path = os.path.abspath(relative_path)
                list_of_paths.append(absolute_path)

    return list_of_paths


def _clear_generated_ui_package():

    if os.path.exists(GENERATED_GUI_PACKAGE_NAME) and os.path.isdir(GENERATED_GUI_PACKAGE_NAME):
        shutil.rmtree(GENERATED_GUI_PACKAGE_NAME)

    os.mkdir(GENERATED_GUI_PACKAGE_NAME)


def _generate_python_equivalent_gui_files(list_of_ui_file_paths):

    for ui_file_path in list_of_ui_file_paths:
        ui_file_name = ui_file_path.split(os.path.sep).pop()
        ui_file_extensionless_name = ui_file_name.split(".")[0]
        equivalent_python_file_name = ui_file_extensionless_name + ".py"
        path_to_generated_equivalent_file = PATH_TO_GENERATED_GUI_PACKAGE + os.path.sep + equivalent_python_file_name

        os.system(PATH_TO_GUI_BUILD_SCRIPT + " -x " + ui_file_path + " -o " + path_to_generated_equivalent_file)


def _generate_run_time_map_lookup(list_of_ui_file_paths):

    f = open(PATH_TO_RUNTIME_LOOKUP_MODULE, "w")

    for ui_file_path in list_of_ui_file_paths:
        ui_file_name = ui_file_path.split(os.path.sep).pop()
        ui_file_extensionless_name = ui_file_name.split(".")[0]
        f.write("from " + GENERATED_GUI_PACKAGE_NAME + "." + ui_file_extensionless_name + " import " + "Ui_" + ui_file_extensionless_name + "\n")

    f.write("\n")
    f.write(RUNTIME_LOOKUP_MAP_NAME + " = {\n")


    for ui_file_path in list_of_ui_file_paths:
        ui_file_name = ui_file_path.split(os.path.sep).pop()
        ui_file_extensionless_name = ui_file_name.split(".")[0]

        f.write("\t" + '"' + ui_file_extensionless_name + '"' + ": " + "Ui_" + ui_file_extensionless_name + ",\n")

    f.write("}\n")

    f.close()



def _generate_package_init_file():

    path_to_init_file = PATH_TO_GENERATED_GUI_PACKAGE + os.path.sep + "__init__.py"
    f = open(path_to_init_file, "w")
    f.write('"""\n')
    f.write("Generated on " + time.ctime() + ".\n\n")
    f.write("This package contains all automatically generated Python equivalents to component UI files.")
    f.write('\n"""\n')
    f.close()



if __name__ == "__main__":
    _clear_generated_ui_package()
    all_ui_file_paths = get_paths_to_all_ui_files_in_project()
    _generate_python_equivalent_gui_files(all_ui_file_paths)

    _generate_run_time_map_lookup(all_ui_file_paths)

    _generate_package_init_file()

    print("\n\nAll .ui files have been converted into equivalent .py files.\n")


