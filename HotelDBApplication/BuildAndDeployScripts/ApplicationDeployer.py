"""
Created on : March 9 2021

Author : KD
"""


import os
import shutil

APPLICATION_MAIN_FILE_NAME = "HotelGUI.py"

# This value varies depending on which user machine this runs on.
PATH_TO_DEPLOY_SCRIPT = "C:\\Users\\Home\\AppData\\Local\\Programs\\Python\\Python39\\Scripts\\pyinstaller"



if __name__ == "__main__":

    application_executable_name = APPLICATION_MAIN_FILE_NAME.replace(".py", ".exe")

    os.system(PATH_TO_DEPLOY_SCRIPT + " -F " + APPLICATION_MAIN_FILE_NAME)

    shutil.move("dist" + os.path.sep + application_executable_name, application_executable_name)

