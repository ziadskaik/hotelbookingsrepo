"""
Created on : March 9 2021

Author : KD
"""


import os
import shutil

APPLICATION_MAIN_FILE_NAME = "HotelGUI.py"


def _delete_directory(directory_name):

    if os.path.exists(directory_name) and os.path.isdir(directory_name):
        shutil.rmtree(directory_name)


def _delete_file(file_name):

    if os.path.exists(file_name) and os.path.isfile(file_name):
        os.remove(file_name)


if __name__ == "__main__":

    application_spec_name = APPLICATION_MAIN_FILE_NAME.replace(".py", ".spec")

    _delete_file(application_spec_name)
    _delete_directory("dist")
    _delete_directory("build")
    _delete_directory("__pycache__")

