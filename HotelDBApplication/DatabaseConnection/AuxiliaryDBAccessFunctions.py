"""
Created on : March 9 2021

Author : KD

This module is a library for various supporting functions for database access and processing requests.
"""


SCHEMA_NAME = "project"



def insert_into_table(database_connection, table_name, column_names, column_values):

    insert_query = "INSERT INTO " + SCHEMA_NAME + "." + table_name + " "
    insert_query += column_names + " VALUES "
    insert_query += column_values + ";"

    db_cursor = database_connection.cursor()

    db_cursor.execute(insert_query)
    database_connection.commit()



def extract_key_name_from_psycopg2_duplication_error(duplication_error):
    # When an attempt is made to insert a tuple with a forbidden duplicate into a table this exception type is raised.

    # Get the name of the key (column) and the duplicate value that was attempted.
    duplication_error_message = str(duplication_error)

    key_name_lookup_flag = "DETAIL:  Key ("
    key_name = duplication_error_message.split(key_name_lookup_flag)[1].split(")")[0]

    key_value_lookup_flag = "=("
    key_value = duplication_error_message.split(key_value_lookup_flag)[1].split(")")[0]

    return key_name, key_value

