"""
Created on : March 9 2021

Author : KD

This module provides the main services to provide database access.
"""

import psycopg2
from psycopg2.errors import UniqueViolation as Unique_violation_error
from datetime import datetime

from pubsub import pub as ThePubSubMachine

from DatabaseConnection.AuxiliaryDBAccessFunctions import SCHEMA_NAME
from DatabaseConnection import AuxiliaryDBAccessFunctions
from GenericUtilities.PublishSubscribeTopics import *

from GenericUtilities.MessageStructures import HotelRoomOptions, HotelRoomSearchResult, SpecificHotelRoomInfo, SpecificHotelBookings, CustomerBookingInfo


# These are the database credentials to establish the connection.
DB_HOST = "web0.eecs.uottawa.ca"
DB_NAME = "group_a04_g43"
DB_USER = "kdahe094"
DB_PASS = "WaynePalmer2006!"
DB_PORT = 15432

# Create a connection and obtain a cursor to the database.
m_database_connection =  psycopg2.connect(dbname=DB_NAME, user=DB_USER, password=DB_PASS, host=DB_HOST, port=DB_PORT)
m_database_cursor = m_database_connection.cursor()



def _process_register_new_customer_request(register_new_customer_structure):
    # This is a request to register a new customer into the system.

    # Start by executing an SQL query to get the next highest contactable entity ID which will be the ID for the new customer.
    query = "SELECT MAX(ContactID)+1 as new_contactable_id_value FROM project.ContactableEntity;"
    m_database_cursor.execute(query)
    new_ids = m_database_cursor.fetchall()
    # Grab the data out of the list of tuples and convert it to an integer.
    new_contactable_entity_id = int(new_ids[0][0])


    # Now attempt to add the data in each of the 5 tables one at a time. This is a transaction so if any step fails, rollback must be done.
    try:
        table_name = "ContactableEntity"
        column_names = "(ContactID, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip)"
        column_values = "(" + str(new_contactable_entity_id) \
        + ", " + "'" + register_new_customer_structure.address_line_1 + "'" + ", " + "'" + register_new_customer_structure.address_line_2 + "'" \
        + ", " + "'" + register_new_customer_structure.city + "'" + ", " + "'" + register_new_customer_structure.prov_state + "'" \
        + ", " + "'" + register_new_customer_structure.country + "'" + ", " + "'" + register_new_customer_structure.postal_zip + "'" + ")"
        insert_query = "INSERT INTO " + SCHEMA_NAME + "." + table_name + " " + column_names + " VALUES " + column_values + ";"
        m_database_cursor.execute(insert_query)


        table_name = "Person"
        column_names = "(ContactID, SIN, FirstName, LastName, Password)"
        column_values = "(" + str(new_contactable_entity_id) \
                + ", " + "'" + register_new_customer_structure.sin + "'" + ", " + "'" + register_new_customer_structure.first_name + "'" \
                + ", " + "'" + register_new_customer_structure.last_name + "'" + ", " + "MD5('" + register_new_customer_structure.plain_text_password + "')"+ ")"
        insert_query = "INSERT INTO " + SCHEMA_NAME + "." + table_name + " " + column_names + " VALUES " + column_values + ";"
        m_database_cursor.execute(insert_query)


        table_name = "Customer"
        column_names = "(PersonContactID, DateOfRegistration)"
        current_date = datetime.today().strftime('%Y-%m-%d')
        column_values = "(" + str(new_contactable_entity_id) \
                + ", " + "'" + current_date\
                        + "'" + ")"
        insert_query = "INSERT INTO " + SCHEMA_NAME + "." + table_name + " " + column_names + " VALUES " + column_values + ";"
        m_database_cursor.execute(insert_query)


        table_name = "PhoneInformation"
        column_names = "(PhoneNumber, Description, ContactID)"
        column_values = "(" + "'" + register_new_customer_structure.phone + "'" + ", " + "'" + "Main Phone" + "'" \
                + ", " + "'" + str(new_contactable_entity_id) \
                        + "'" + ")"
        insert_query = "INSERT INTO " + SCHEMA_NAME + "." + table_name + " " + column_names + " VALUES " + column_values + ";"
        m_database_cursor.execute(insert_query)


        table_name = "EmailInformation"
        column_names = "(EmailAddress, Description, ContactID)"
        column_values = "(" + "'" + register_new_customer_structure.email + "'" + ", " + "'" + "Main Email" + "'" \
                + ", " + "'" + str(new_contactable_entity_id) \
                        + "'" + ")"
        insert_query = "INSERT INTO " + SCHEMA_NAME + "." + table_name + " " + column_names + " VALUES " + column_values + ";"
        m_database_cursor.execute(insert_query)

        # All 5 tables have successfully been updated with the new data so the query can now be committed.
        m_database_connection.commit()
        # Provide the new customer's ID as a response to the component that made the request (this object still has a reference to this structure).
        register_new_customer_structure.new_customer_id = new_contactable_entity_id


    except Unique_violation_error as duplication_error:
        # An entry for a field that does not permit duplicates was made. Find out which field this is and what the duplicate value is.
        key_name, key_value= AuxiliaryDBAccessFunctions.extract_key_name_from_psycopg2_duplication_error(duplication_error)
        # Provide an error message as a response to the component that made the request.
        register_new_customer_structure.error_message = "The " + key_name + " " + key_value + " is already registered by another user."
        # Rollback the entire transaction so none of it happens.
        m_database_connection.rollback()



def _verify_admin_authentication(potential_admin_user_name, potential_admin_password):

    admin_authentication_query = "SELECT COUNT(*) FROM project.DatabaseAdministrator " \
                                 "WHERE username='{0}' and password=MD5('{1}');".format(potential_admin_user_name, potential_admin_password)

    # Execute the query and get the count value.
    m_database_cursor.execute(admin_authentication_query)
    matches = m_database_cursor.fetchall()
    number_of_matches = matches[0][0]

    return number_of_matches > 0



def _process_authenticate_customer_request(authentication_customer_structure):
    # This is a request to authenticate an existing customer.

    # The customer can sign in either with their ID, their phone number, or their email.
    customer_id_or_email_or_phone = authentication_customer_structure.customer_id_or_email_or_phone
    plain_text_password = authentication_customer_structure.plain_text_password

    if _verify_admin_authentication(customer_id_or_email_or_phone, plain_text_password):
        # First check to see if these are admin credentials. DB admins use this backdoor as a path to the SQL query page.
        authentication_customer_structure.special_admin_login = True
        return

    # SQL does not allow comparison between strings and integers so we must avoid comparing and email to a contactable ID.
    if customer_id_or_email_or_phone.isdigit():
        # The user has entered their ID (or phone number).
        authenticator_id_input = customer_id_or_email_or_phone
    else:
        # The user is choosing to sign in using their email.
        authenticator_id_input = -1

    # Run an SQL query to find out how many (there should be at most 1) customers have the given credentials.
    authentication_query = "SELECT Person.ContactID, FirstName, LastName " \
                           "FROM project.ContactableEntity " \
                           "NATURAL JOIN project.Person " \
                           "JOIN project.Customer ON Customer.PersonContactID = Person.contactid " \
                           "LEFT JOIN project.EmailInformation ON EmailInformation.ContactID = ContactableEntity.ContactID " \
                           "LEFT JOIN project.PhoneInformation ON PhoneInformation.ContactID = ContactableEntity.ContactID " \
                           "WHERE Customer.PersonContactID = '{0}' OR PhoneInformation.PhoneNumber = '{1}' OR EmailInformation.EmailAddress = '{1}' " \
                           "AND	Person.Password = MD5('{2}');" \
                           "".format(authenticator_id_input, customer_id_or_email_or_phone, plain_text_password)


    # Execute the query and get the count value.
    try:
        m_database_cursor.execute(authentication_query)
        matches = m_database_cursor.fetchall()
        authentication_success = len(matches) > 0

        # Authentication is valid so long as a match was found within the database.
        authentication_customer_structure.authentication_success = authentication_success
    except Exception:
        authentication_customer_structure.authentication_success = False
        m_database_connection.rollback()
        return


    if not authentication_success:
        # The authentication failed so no need to do anything else.
        return

    authentication_customer_structure.customer_id = matches[0][0]
    authentication_customer_structure.customer_full_name = matches[0][1] + " " + matches[0][2]



def _process_verify_person_email_existence_request(verify_email_existence_structure):
    # This is a request to verify that an email address does in fact belong to a customer or employee.

    email_to_verify = verify_email_existence_structure.email_address

    # Run an SQL query to find out how many (there should be at most 1) persons have the given email address.
    verification_query = "SELECT Count(*) FROM project.Person NATURAL JOIN project.ContactableEntity NATURAL JOIN project.EmailInformation " \
                         "WHERE EmailAddress='{}';".format(email_to_verify)

    # Execute the query and get the count value.
    m_database_cursor.execute(verification_query)
    matches = m_database_cursor.fetchall()
    number_of_matches = matches[0][0]

    # The email exists so long as a match was found within the database.
    verify_email_existence_structure.email_exists = number_of_matches > 0



def _process_reset_password_request(reset_password_structure):
    # This is a request to reset a person's password

    email = reset_password_structure.person_email
    new_password = reset_password_structure.new_plain_text_password

    # Run an SQL query to update the password for the given person.
    password_update_query = "UPDATE project.Person Set password=MD5('{0}') " \
                            "WHERE contactid IN (SELECT contactid FROM project.Person NATURAL JOIN project.ContactableEntity NATURAL JOIN project.EmailInformation " \
                            "WHERE EmailAddress='{1}');".format(new_password, email)

    try:
        m_database_cursor.execute(password_update_query)
        m_database_connection.commit()
    except Exception as update_error:
        reset_password_structure.error_message = str(update_error)
        # Rollback the entire transaction so none of it happens.
        m_database_connection.rollback()

    # The password was successfully updated.
    reset_password_structure.password_update_successful = True



def _process_get_customer_information_request(customer_info_structure):

    customer_id = customer_info_structure.customer_id

    get_customer_info_query = "SELECT FirstName, LastName, EmailAddress " \
                              "FROM project.ContactableEntity NATURAL LEFT JOIN project.EmailInformation NATURAL LEFT JOIN project.Person " \
                              "WHERE ContactID={0};".format(customer_id)


    m_database_cursor.execute(get_customer_info_query)
    customer_information_results = m_database_cursor.fetchall()
    customer_info_structure.customer_name = customer_information_results[0][0] + " " + customer_information_results[0][1]
    customer_info_structure.customer_email_address = customer_information_results[0][2]




def _process_get_personal_information_request(get_personal_info_structure):

    # This is a request to get an employee's personal information
    id = get_personal_info_structure.id

    # Run an SQL query to get all the personal information for the employee
    get_personal_info_query =  "SELECT sin, physicaladdress_line1, physicaladdress_line2, city, prov_state, country, " \
                               "postal_zip, phonenumber, emailaddress, salary, dateofhire FROM project.person  JOIN " \
                               "project.employee ON employee.personcontactid=person.contactid NATURAL JOIN project.contactableentity " \
                               "LEFT JOIN project.EmailInformation ON EmailInformation.ContactID = ContactableEntity.ContactID " \
                               "LEFT JOIN project.PhoneInformation ON PhoneInformation.ContactID = ContactableEntity.ContactID " \
                               "WHERE project.employee.personcontactid='{0}';".format(id)



    # Execute the query and get the personal info.
    m_database_cursor.execute(get_personal_info_query)
    matches = m_database_cursor.fetchall()
    get_personal_info_structure.sin = matches[0][0]

    get_personal_info_structure.line1 = matches[0][1]
    get_personal_info_structure.line2 = matches[0][2]
    get_personal_info_structure.city = matches[0][3]
    get_personal_info_structure.prov_state = matches[0][4]
    get_personal_info_structure.country = matches[0][5]
    get_personal_info_structure.postal_zip = matches[0][6]

    get_personal_info_structure.phone_number = matches[0][7]
    get_personal_info_structure.email_address = matches[0][8]
    get_personal_info_structure.salary = matches[0][9]
    get_personal_info_structure.date_of_hire = matches[0][10]


def _process_authenticate_employee_request(authentication_employee_structure):
    # This is a request to authenticate an existing employee and find out all the hotels for which the employee works for.

    # The employee can sign in either with their ID, their phone number, or their email.
    employee_id_or_email_or_phone = authentication_employee_structure.employee_id_or_email_or_phone
    plain_text_password = authentication_employee_structure.plain_text_password

    if _verify_admin_authentication(employee_id_or_email_or_phone, plain_text_password):
        # First check to see if these are admin credentials. DB admins use this backdoor as a path to the SQL query page.
        authentication_employee_structure.special_admin_login = True
        return


    # SQL does not allow comparison between strings and integers so we must avoid comparing and email to a contactable ID.
    if employee_id_or_email_or_phone.isdigit():
        # The user has entered their ID (or phone number).
        authenticator_id_input = employee_id_or_email_or_phone
    else:
        # The user is choosing to sign in using their email.
        authenticator_id_input = -1

    # Run an SQL query to get the employee information if one matches the given credentials.
    authentication_query = "SELECT Employee.PersonContactID, firstname, lastname " \
                           "FROM project.ContactableEntity NATURAL LEFT JOIN project.EmailInformation NATURAL JOIN project.Person " \
                           "JOIN project.Employee ON Employee.PersonContactID=Person.ContactID " \
                           "LEFT JOIN project.PhoneInformation ON PhoneInformation.ContactID=ContactableEntity.ContactID " \
                           "WHERE (phonenumber='{1}' OR emailaddress='{1}' OR Employee.PersonContactID='{0}') " \
                           "AND password=MD5('{2}');".format(authenticator_id_input, employee_id_or_email_or_phone, plain_text_password)

    # Execute the query and get the count value.
    try:
        m_database_cursor.execute(authentication_query)
        matches = m_database_cursor.fetchall()
        authentication_success = len(matches) > 0

        # Authentication is valid so long as a match was found within the database.
        authentication_employee_structure.authentication_success = authentication_success
    except Exception:
        authentication_employee_structure.authentication_success = False
        m_database_connection.rollback()
        return


    if not authentication_success:
        # The authentication failed so no need to do anything else.
        return

    # Set the results accordingly.
    employee_id = matches[0][0]
    authentication_employee_structure.employee_id = employee_id
    authentication_employee_structure.employee_full_name = matches[0][1] + " " + matches[0][2]

    # Run an SQL query to get all the needed hotel information for the hotels the employee works at.
    get_hotels_data_query = "SELECT DISTINCT HotelChain.ContactID, HotelBrand.brandname, PhysicalAddress_Line1, City, Country, Postal_zip " \
                            "FROM project.HotelChain JOIN project.HotelBrand ON HotelChain.HotelBrandContactID=HotelBrand.ContactID " \
                            "LEFT JOIN project.worksFor ON HotelChain.ContactID=worksFor.HotelChainContactID " \
                            "LEFT JOIN project.Manager ON Manager.EmployeeContactID=HotelChain.ManagerContactID " \
                            "JOIN project.ContactableEntity ON HotelChain.ContactID=ContactableEntity.ContactID " \
                            "WHERE HotelChain.ManagerContactID='{0}' OR worksFor.NonManagerContactID='{0}';".format(employee_id)

    m_database_cursor.execute(get_hotels_data_query)
    hotels_info = m_database_cursor.fetchall()
    if len(hotels_info) == 0:
        authentication_employee_structure.error_message = "The employee exists but does not work for any hotels!"
        return

    # Pick up all the data for the hotels and place them in the given structure for the employee sign in page to get.
    for hotel_information in hotels_info:
        hotel_chain_id = hotel_information[0]
        hotel_name = hotel_information[1]
        hotel_address = hotel_information[2] + ", " + hotel_information[3] + ", " + hotel_information[4] + ", " + hotel_information[5]
        authentication_employee_structure.list_of_hotel_id_name_and_full_address_tuples.append((hotel_chain_id, hotel_name, hotel_address))


def _process_process_book_customer_request(book_customer_structure):

    # This is a request to book the customer in a room at a hotel.

    hotel_chain_contact_id = book_customer_structure.hotel_chain_contact_id
    room_number = book_customer_structure.room_number
    reserve_start_datetime = book_customer_structure.reserve_start_datetime
    reserve_end_datetime = book_customer_structure.reserve_end_datetime

    customer_identification = str(book_customer_structure.customer_identification)
    number_of_occupants = str(book_customer_structure.number_of_occupants)

    # Start by executing  an SQL query to get the customer id from the customer identification

    # SQL does not allow comparison between strings and integers so we must avoid comparing and email to a contactable ID.

    customer_phone_or_email = ""

    if customer_identification.isdigit():
        # The user has entered their ID
        customer_id = customer_identification
    else:
        # The user provided phone or email information.
        customer_id = -1
        customer_phone_or_email = customer_identification

    customer_info_query = "SELECT C.personcontactid from project.customer as C, project.person as P, project.contactableentity as CE" \
                          " LEFT JOIN project.emailinformation on CE.contactid = project.emailinformation.contactid" \
                          " LEFT JOIN project.phoneinformation on CE.contactid = project.phoneinformation.contactid" \
                          " WHERE C.personcontactid = P.contactid AND P.contactid = CE.contactid AND" \
                          " ((C.personcontactid='{0}') OR" \
                          " (project.phoneinformation.phonenumber = '{1}') OR" \
                          " (project.emailinformation.emailaddress = '{1}'));".format(customer_id, customer_phone_or_email)


    m_database_cursor.execute(customer_info_query)
    customer_info_query_result = m_database_cursor.fetchall()

    if len(customer_info_query_result) == 0:
        book_customer_structure.error_message = "There is no customer registered under the given credentials."
        return


    customer_id = customer_info_query_result[0][0]

    # Execute an SQL query to get the next highest booking ID which will be the ID for the new booking.
    booking_id_query = "SELECT MAX(bookingid)+1 FROM project.Booking;"
    m_database_cursor.execute(booking_id_query)
    result = m_database_cursor.fetchall()
    new_booking_id = result[0][0]
    if new_booking_id is None:
        new_booking_id = 1

    book_customer_structure.booking_id = new_booking_id

    # Run an SQL query to book a customer for a given room.
    booking_query = "INSERT INTO project.Booking (bookingid, reservestartdatetime, reserveenddatetime, totalnumberofoccupants," \
                    " roomnumber, hotelchaincontactid, customercontactid) VALUES ({0},'{1}','{2}',{3},{4},{5},{6});".format(new_booking_id,reserve_start_datetime,
                                                                                                                        reserve_end_datetime, number_of_occupants, room_number, hotel_chain_contact_id, customer_id)

    try:
        m_database_cursor.execute(booking_query)
        m_database_connection.commit()
    except Exception as insert_error:
        book_customer_structure.error_message = str(insert_error)
        # Rollback the entire transaction so none of it happens.
        m_database_connection.rollback()


def _process_obtain_hotel_room_options_request(city_to_hotel_room_options_map):

    # For each city in the database, create a HotelRoomOptions to hold all the possible room types available in that city.

    # Create a query to get the capacity of the largest hotel room in each city.
    get_cities_max_room_capacity_query = "SELECT City, MAX(Capacity) as highest_room_capacity FROM project.ContactableEntity NATURAL JOIN project.HotelChain " \
                                 "JOIN project.HotelRoom ON HotelChain.ContactID = HotelRoom.HotelChainContactID " \
                                 "NATURAL JOIN project.RoomType NATURAL JOIN project.RoomTypeAmenity " \
                                 "GROUP BY City;"

    m_database_cursor.execute(get_cities_max_room_capacity_query)
    city_max_rooms = m_database_cursor.fetchall()

    # Iterate through the pairs of city and largest room capacity and begin the creation of the city's HotelRoomOptions object.
    for city_name, maximum_room_capacity in city_max_rooms:
        room_option_structure = HotelRoomOptions()
        room_option_structure.maximum_capacity = maximum_room_capacity
        # Store the information object in the given (and resulting) map.
        city_to_hotel_room_options_map[city_name] = room_option_structure


    # Create a query to get all the view types available among all hotel rooms per city.
    get_cities_view_type_query = "SELECT DISTINCT City, View FROM project.ContactableEntity NATURAL JOIN project.HotelChain " \
                                 "JOIN project.HotelRoom ON HotelChain.ContactID = HotelRoom.HotelChainContactID " \
                                 "NATURAL JOIN project.RoomType NATURAL JOIN project.RoomTypeAmenity;"

    m_database_cursor.execute(get_cities_view_type_query)
    city_view_types = m_database_cursor.fetchall()

    # Iterate through the pairs of city and view and update the HotelRoomOptions objects for each city.
    for city_name, view_type in city_view_types:
        if city_name in city_to_hotel_room_options_map:
            if view_type is not None:
                city_to_hotel_room_options_map[city_name].list_of_views.append(view_type)


    # Create a query to get all the amenity types available among all hotel rooms per city.
    get_cities_amenity_type_query = "SELECT DISTINCT City, Amenity FROM project.ContactableEntity NATURAL JOIN project.HotelChain " \
                                    "JOIN project.HotelRoom ON HotelChain.ContactID = HotelRoom.HotelChainContactID " \
                                    "NATURAL JOIN project.RoomType NATURAL JOIN project.RoomTypeAmenity;"

    m_database_cursor.execute(get_cities_amenity_type_query)
    city_amenity_types = m_database_cursor.fetchall()

    # Iterate through the pairs of city and amenity and update the HotelRoomOptions objects for each city.
    for city_name, amenity_type in city_amenity_types:
        if city_name in city_to_hotel_room_options_map:
            city_to_hotel_room_options_map[city_name].list_of_amenities.append(amenity_type)



def _process_search_hotel_rooms_request(hotel_room_search_structure):

    # Extract all the search criteria from the given request structure.

    city = hotel_room_search_structure.city
    start = str(hotel_room_search_structure.reserve_start_unix_epoch)
    end = str(hotel_room_search_structure.reserve_end_unix_epoch)
    min = str(hotel_room_search_structure.minimum_cost)
    max = str(hotel_room_search_structure.maximum_cost)
    num_occupants = hotel_room_search_structure.number_of_occupants
    view = hotel_room_search_structure.view_type
    sql_amenity_list = str(hotel_room_search_structure.list_of_amenities).replace("[","(").replace("]", ")")
    number_of_amenities = str(len(hotel_room_search_structure.list_of_amenities))

    if number_of_amenities == '0':
        # Must treat this case seperately otherwise we get "()" without the inner quotations (Python and SQL are different for this specific case).
        sql_amenity_list = "('')"


    # Create a query to search for a hotel room that satisfies the given search criteria including the reservation datetime (unix epochs) and list of room amenities.
    room_search_query = "SELECT DISTINCT HotelRoom.HotelChainContactID, RoomNumber, BrandName, StarCategory, PhysicalAddress_Line1, City, Country, Postal_Zip, Price, Capacity, Extendable, View, PhoneNumber " \
                        "FROM project.ContactableEntity NATURAL JOIN project.HotelChain " \
                        "JOIN project.HotelBrand ON HotelChain.HotelBrandContactID=HotelBrand.ContactID " \
                        "JOIN project.PhoneInformation ON ContactableEntity.ContactID = PhoneInformation.ContactID " \
                        "JOIN project.HotelRoom ON HotelChain.ContactID = HotelRoom.HotelChainContactID " \
                        "NATURAL JOIN project.RoomType " \
                        "  " \
                        "WHERE City='{0}' AND Price>{1} AND Price<{2} AND View='{3}' AND " \
                        "(Capacity>={4} OR (Capacity>={5} AND Extendable=TRUE)) AND NOT EXISTS " \
                        " " \
                        "(SELECT * FROM project.Booking as B2 WHERE HotelRoom.RoomNumber=B2.RoomNumber AND HotelRoom.HotelChainContactID=B2.HotelChainContactID AND (" \
                            "(extract(epoch from B2.ReserveEndDateTime)>={6} AND extract(epoch from B2.ReserveEndDateTime)<={7}) OR " \
                            "(extract(epoch from B2.ReserveStartDateTime)>={6} AND extract(epoch from B2.ReserveStartDateTime)<={7}) OR " \
                            "(extract(epoch from B2.ReserveStartDateTime)<={6} AND extract(epoch from B2.ReserveEndDateTime)>={7}) " \
                                ") ) " \
                        "AND " \
                        "(SELECT Count(Amenity) from project.RoomTypeAmenity as RTA WHERE RTA.RoomTypeID=HotelRoom.RoomTypeID AND Amenity IN {8})={9};" \
                        "".format(city, min, max, view, str(num_occupants), str(num_occupants-1), start, end, sql_amenity_list, number_of_amenities)

    m_database_cursor.execute(room_search_query)
    hotel_room_search_results = m_database_cursor.fetchall()

    # Iterate through the hotel room search results and package the results in HotelRoomSearchResult and place them in the given structure's list for the caller to get.
    for HotelChainContactID, RoomNumber, BrandName, StarCategory, PhysicalAddress_Line1, City, Country, Postal_Zip, Price, Capacity, Extendable, View, PhoneNumber in hotel_room_search_results:

        current_result_structure = HotelRoomSearchResult()
        current_result_structure.hotelChainContactID = HotelChainContactID
        current_result_structure.room_number = RoomNumber
        current_result_structure.hotel_name = BrandName
        current_result_structure.star_category = StarCategory
        current_result_structure.full_address = PhysicalAddress_Line1 + ", " + City + ", " + Country + ", " + Postal_Zip
        current_result_structure.price = Price

        if str(Extendable) == "True":
            current_result_structure.capacity = str(int(Capacity )+ 1)
        else:
            current_result_structure.capacity = Capacity
        current_result_structure.view = View
        current_result_structure.phone_number = PhoneNumber

        hotel_room_search_structure.list_of_hotel_room_search_results.append(current_result_structure)


    # Now run a separate query to obtain the list of all amenities each room actually does have.
    for room_search_result in  hotel_room_search_structure.list_of_hotel_room_search_results:

        amenity_fetch_query = "SELECT  Amenity FROM project.HotelRoom NATURAL JOIN project.RoomType NATURAL JOIN project.RoomTypeAmenity " \
                              "WHERE HotelChainContactID={0} AND RoomNumber={1}".format(room_search_result.hotelChainContactID, room_search_result.room_number)

        m_database_cursor.execute(amenity_fetch_query)
        hotel_room_amenities = m_database_cursor.fetchall()

        for amenity_entries in hotel_room_amenities:
            for amenity_entry in amenity_entries:
                room_search_result.list_of_amenities.append(amenity_entry)



def _process_sql_query_request(sql_query_structure):
    # Process the request to execute a custom SQL query on the hotel database.

    sql_query = sql_query_structure.sql_query

    try:
        m_database_cursor.execute(sql_query)
    except Exception as query_error:
        m_database_connection.rollback()
        sql_query_structure.error_message = str(query_error)
        return

    query_status_msg = m_database_cursor.statusmessage

    if query_status_msg.upper().find("SELECT") == 0:
        try:
            sql_query_structure.sql_query_result_tuples = m_database_cursor.fetchall()
            sql_query_structure.sql_query_result_headers = m_database_cursor.description
            sql_query_structure.sql_query_result_row_count = m_database_cursor.rowcount

        except Exception as transaction_error:
            sql_query_structure.error_message = str(transaction_error)

    elif query_status_msg.upper().find("DELETE") == 0 or query_status_msg.upper().find("INSERT") == 0 or query_status_msg.upper().find("UPDATE") == 0 \
            or query_status_msg.upper().find("DROP") == 0 or query_status_msg.upper().find("CREATE") == 0:
        try:
            m_database_connection.commit()
        except Exception as transaction_error:
            m_database_connection.rollback()
            sql_query_structure.error_message = str(transaction_error)

    else:
        sql_query_structure.error_message = "Unrecognized SQL query."

def _process_customer_current_booking_info_request(customer_booking_info_structure):
    # process request to view current bookings for the customer
    customer_id = customer_booking_info_structure.customer_id

    booking_info_query = "SELECT BookingID,RoomTypeID,BrandName, physicaladdress_line1, physicaladdress_line2, city, prov_state, country, postal_zip, price, " \
                         "totalnumberofoccupants, view, roomnumber, reservestartdatetime, reserveenddatetime, checkindatetime, checkoutdatetime FROM " \
                         "project.Booking NATURAL LEFT JOIN project.Renting NATURAL JOIN project.HotelRoom " \
                         "NATURAL JOIN project.RoomType " \
                         "JOIN project.HotelChain ON HotelChain.ContactID=HotelRoom.hotelchaincontactid " \
                         "JOIN project.HotelBrand ON HotelBrand.ContactID=HotelChain.hotelbrandcontactid " \
                         "JOIN project.Contactableentity ON Contactableentity.ContactID=HotelChain.ContactID " \
                         "WHERE project.booking.customercontactid={0};".format(customer_id)

    m_database_cursor.execute(booking_info_query)
    booking_results = m_database_cursor.fetchall()

    for BookingID, RoomTypeID,BrandName, physicaladdress_line1, physicaladdress_line2, city, prov_state, country, postal_zip, price, \
        totalnumberofoccupants, view, roomnumber, reservestartdatetime, reserveenddatetime, checkindatetime, checkoutdatetime in booking_results:

                         full_address = physicaladdress_line1 + ", " + physicaladdress_line2 + ", " + city + ", " + prov_state + ", " + country + ", " + postal_zip

                         current_booking_info = CustomerBookingInfo(BookingID, BrandName, full_address, price, totalnumberofoccupants, view, roomnumber,
                                                                    reservestartdatetime, reserveenddatetime, checkindatetime, checkoutdatetime)


                         current_booking_info.list_of_amenities = _get_list_of_all_amenities("RoomTypeID", RoomTypeID, "RoomTypeAmenity")

                         customer_booking_info_structure.list_of_customer_booking_info.append(current_booking_info)



def _process_customer_past_booking_info_request(customer_booking_info_structure):
    # process request to view past bookings for the customer

    customer_id = customer_booking_info_structure.customer_id

    booking_info_query = "SELECT ArchiveID,HotelBrandName, hotelchainphysicaladdress_line1, hotelchainphysicaladdress_line2, " \
                         "hotelchaincity, hotelchainprov_state, hotelchaincountry, hotelchainpostal_zip, roomprice, " \
                         "totalnumberofoccupants, roomview, roomnumber, reservestartdatetime, reserveenddatetime, checkindatetime, checkoutdatetime FROM " \
                         "project.BookingArchive " \
                         "WHERE customercontactid={0};".format(customer_id)



    m_database_cursor.execute(booking_info_query)
    booking_results = m_database_cursor.fetchall()

    for ArchiveID,HotelBrandName, hotelchainphysicaladdress_line1, hotelchainphysicaladdress_line2, hotelchaincity, hotelchainprov_state, hotelchaincountry, \
        hotelchainpostal_zip, roomprice, totalnumberofoccupants, roomview, roomnumber, reservestartdatetime, reserveenddatetime, checkindatetime, checkoutdatetime \
            in booking_results:

                         full_address = hotelchainphysicaladdress_line1 + ", " + hotelchainphysicaladdress_line2 + ", " + hotelchaincity + ", " + hotelchainprov_state + ", " + hotelchaincountry + ", " + hotelchainpostal_zip

                         current_booking_info = CustomerBookingInfo(None, HotelBrandName, full_address, roomprice, totalnumberofoccupants, roomview, roomnumber,
                                                                    reservestartdatetime, reserveenddatetime, checkindatetime, checkoutdatetime)

                         current_booking_info.list_of_amenities =  _get_list_of_all_amenities("ArchiveID", ArchiveID, "BookingArchiveAmenity")

                         customer_booking_info_structure.list_of_customer_booking_info.append(current_booking_info)



def _get_list_of_all_amenities(identifier_name, identifier_value, table_name):
    # Obtain all the amenities for a specific room given its room type identifier.

    get_specific_room_amenities_query = "SELECT Amenity FROM project.{0} WHERE {1}={2};".format(table_name, identifier_name, str(identifier_value))

    m_database_cursor.execute(get_specific_room_amenities_query)
    hotel_rooms_amenities = m_database_cursor.fetchall()

    # Iterate through the list of tuples and package the values in a list, then return the list.
    all_amenities_for_room = []
    for Amenity in hotel_rooms_amenities:
        all_amenities_for_room.append(Amenity[0])

    return all_amenities_for_room


def _process_cancel_customer_booking_request(cancel_customer_booking_structure):
    # process the request to cancel the customer booking

    booking_id = cancel_customer_booking_structure.booking_id

    delete_query = "DELETE FROM Project.Booking " \
                   "WHERE bookingid={0}".format(booking_id)


    try:
        m_database_cursor.execute(delete_query)
        m_database_connection.commit()
    except Exception as delete_error:
        cancel_customer_booking_structure.error_message = str(delete_error)
        # Rollback the entire transaction so none of it happens.
        m_database_connection.rollback()


    if m_database_cursor.statusmessage == "DELETE 0":
        cancel_customer_booking_structure.error_message = "No Booking was found and so could not be deleted."
    elif m_database_cursor.statusmessage != "DELETE 1":
        cancel_customer_booking_structure.error_message = "Unexpected error occurred."




def _process_hotel_chain_characteristics_request(hotel_chain_characteristics_structure):

    # Obtain all the possible characteristics for all the rooms in one specific hotel chain.
    # This is what employees of the hotel can filter/search rooms with.

    hotel_chain_contact_id = hotel_chain_characteristics_structure.hotel_chain_contact_id

    # Create a query to obtain all the view types offered at the given hotel.
    get_view_types_query = "SELECT DISTINCT View FROM project.HotelRoom NATURAL JOIN project.RoomType " \
                           "WHERE HotelRoom.HotelChainContactID={0};".format(hotel_chain_contact_id)


    m_database_cursor.execute(get_view_types_query)
    view_types = m_database_cursor.fetchall()

    hotel_chain_characteristics_structure.list_of_views = []
    for view_type_single in view_types:
        hotel_chain_characteristics_structure.list_of_views.append(view_type_single[0])


    # Create a query to obtain the capacity of the largest room at the given hotel.
    get_max_capacity_query = "SELECT MAX(max_value) FROM " \
                           "((SELECT MAX(Capacity)+1 as max_value FROM project.HotelRoom NATURAL JOIN project.RoomType WHERE HotelRoom.HotelChainContactID={0} AND Extendable='True') " \
                           "UNION " \
                           "(SELECT MAX(Capacity) as max_value FROM project.HotelRoom NATURAL JOIN project.RoomType WHERE HotelRoom.HotelChainContactID={0} AND Extendable='False')) " \
                           "as max_extendable_and_non_extendable;".format(hotel_chain_contact_id)

    m_database_cursor.execute(get_max_capacity_query)
    max_capacity = m_database_cursor.fetchall()
    hotel_chain_characteristics_structure.maximum_capacity = int(max_capacity[0][0])


    # Create a query to obtain the price of the most expensive room at the given hotel.
    get_max_price_query = "SELECT MAX(Price) FROM project.HotelRoom NATURAL JOIN project.RoomType " \
                          "WHERE HotelRoom.HotelChainContactID={};".format(hotel_chain_contact_id)

    m_database_cursor.execute(get_max_price_query)
    max_price = m_database_cursor.fetchall()
    hotel_chain_characteristics_structure.maximum_cost = float(max_price[0][0])


    # Create a query to obtain all the amenities offered in at least one room at the given hotel.
    get_amenity_types_query = "SELECT DISTINCT Amenity FROM project.HotelRoom NATURAL JOIN project.RoomType NATURAL JOIN project.RoomTypeAmenity " \
                           "WHERE HotelRoom.HotelChainContactID={0};".format(hotel_chain_contact_id)

    m_database_cursor.execute(get_amenity_types_query)
    amenity_types = m_database_cursor.fetchall()

    hotel_chain_characteristics_structure.list_of_amenities = []
    for amenity_type_single in amenity_types:
        hotel_chain_characteristics_structure.list_of_amenities.append(amenity_type_single[0])



def _get_list_of_all_amenities(identifier_name, identifier_value, table_name):
    # Obtain all the amenities for a specific room given its room type identifier.

    get_specific_room_amenities_query = "SELECT Amenity FROM project.{0} WHERE {1}={2};".format(table_name, identifier_name, str(identifier_value))

    m_database_cursor.execute(get_specific_room_amenities_query)
    hotel_rooms_amenities = m_database_cursor.fetchall()

    # Iterate through the list of tuples and package the values in a list, then return the list.
    all_amenities_for_room = []
    for Amenity in hotel_rooms_amenities:
        all_amenities_for_room.append(Amenity[0])

    return all_amenities_for_room




def _process_all_hotel_rooms_in_chain_request(all_hotel_rooms_structure):
    # Process the request to obtain data about all the hotel rooms in a specific hotel.

    hotel_chain_contact_id = all_hotel_rooms_structure.hotel_chain_contact_id

    # Create a query to obtain all the necessary data about all the rooms in the specific hotel other than their amenities.
    get_all_rooms_info_query = "SELECT RoomNumber, RoomTypeID, Price, Capacity, Extendable, View, Extendable " \
                               "from project.HotelRoom NATURAL JOIN project.RoomType " \
                               "WHERE HotelRoom.HotelChainContactID={0} ORDER BY RoomNumber ASC;".format(hotel_chain_contact_id)

    m_database_cursor.execute(get_all_rooms_info_query)
    hotel_rooms_info_list = m_database_cursor.fetchall()

    # Package all the data (including the amenities) and place the results in the given structure.
    for RoomNumber, RoomTypeID, Price, Capacity, Extendable, View, Extendable in hotel_rooms_info_list:

        specific_hotel_room_info = SpecificHotelRoomInfo(RoomNumber, Price, Capacity, Extendable, View)

        specific_hotel_room_info.list_of_amenities = _get_list_of_all_amenities("RoomTypeID", RoomTypeID, "RoomTypeAmenity")

        all_hotel_rooms_structure.list_of_specific_hotel_room_info.append(specific_hotel_room_info)


def _process_available_hotel_rooms_in_chain_request(employee_hotel_room_search_structure):

    # Process the request to obtain data about all the hotel rooms in a specific hotel that are available and match the given criteria..

    # Extract the search criteria
    hotel_chain_contact_id = employee_hotel_room_search_structure.hotel_chain_contact_id
    start = employee_hotel_room_search_structure.reserve_start_unix_epoch
    end = employee_hotel_room_search_structure.reserve_end_unix_epoch
    min = employee_hotel_room_search_structure.minimum_cost
    max = employee_hotel_room_search_structure.maximum_cost
    minimum_capacity = int(employee_hotel_room_search_structure.minimum_capacity)

    # Convert from a string representation of a Python list to an SQL compatible list.
    sql_view_types = str(employee_hotel_room_search_structure.view_types).replace("[","(").replace("]", ")")

    # Convert from a string representation of a Python list to an SQL compatible list.
    sql_amenity_list = str(employee_hotel_room_search_structure.list_of_amenities).replace("[","(").replace("]", ")")
    number_of_amenities = str(len(employee_hotel_room_search_structure.list_of_amenities))

    if number_of_amenities == '0':
        # Must treat this case separately otherwise we get "()" without the inner quotations (Python and SQL are different for this specific case).
        sql_amenity_list = "('')"

    employee_room_search_query = "SELECT RoomTypeID, RoomNumber, Price, Capacity, Extendable, View FROM project.HotelRoom NATURAL JOIN project.RoomType " \
                                 "WHERE HotelChainContactID={0} AND Price>{1} AND Price<{2} AND View IN {3} AND " \
                                 "(Capacity>={4} OR (Capacity>={5} AND Extendable=TRUE)) AND NOT EXISTS " \
                                 "(SELECT * FROM project.Booking as B2 WHERE HotelRoom.RoomNumber=B2.RoomNumber AND HotelRoom.HotelChainContactID=B2.HotelChainContactID AND (" \
                                 "(extract(epoch from B2.ReserveEndDateTime)>={6} AND extract(epoch from B2.ReserveEndDateTime)<={7}) OR " \
                                 "(extract(epoch from B2.ReserveStartDateTime)>={6} AND extract(epoch from B2.ReserveStartDateTime)<={7}) OR " \
                                 "(extract(epoch from B2.ReserveStartDateTime)<={6} AND extract(epoch from B2.ReserveEndDateTime)>={7}) " \
                                 ") ) " \
                                 "AND " \
                                 "(SELECT Count(Amenity) from project.RoomTypeAmenity as RTA WHERE RTA.RoomTypeID=HotelRoom.RoomTypeID AND Amenity IN {8})={9} " \
                                 "ORDER BY RoomNumber ASC;" \
                                 "".format(hotel_chain_contact_id, min, max, sql_view_types, minimum_capacity, minimum_capacity-1, start, end, sql_amenity_list, number_of_amenities)


    m_database_cursor.execute(employee_room_search_query)
    hotel_room_search_results = m_database_cursor.fetchall()

    # Iterate through the hotel room search results and package the results in HotelRoomSearchResult and place them in the given structure's list for the caller to get.
    for RoomTypeID, RoomNumber, Price, Capacity, Extendable, View in hotel_room_search_results:

        specific_hotel_room_info = SpecificHotelRoomInfo(RoomNumber, Price, Capacity, Extendable, View)

        specific_hotel_room_info.list_of_amenities = _get_list_of_all_amenities("RoomTypeID", RoomTypeID, "RoomTypeAmenity")

        employee_hotel_room_search_structure.list_of_hotel_room_search_results.append(specific_hotel_room_info)



def _process_all_booked_hotel_rooms_in_chain_request(all_booked_hotel_rooms_structure):

    # Process the request to obtain data about all the hotel rooms in a specific hotel that are currently booked or rented.

    hotel_chain_contact_id = all_booked_hotel_rooms_structure.hotel_chain_contact_id

    # Create a query to obtain all the necessary data about all the rooms booked in the specific hotel other than their amenities.
    get_all_booked_rooms_info_query = "SELECT BookingID, RoomTypeID, RoomNumber, Price, Capacity, Extendable, View, FirstName, LastName, ReserveStartDateTime, ReserveEndDateTime, CheckInDateTime, CheckOutDateTime " \
                               "FROM project.booking NATURAL LEFT OUTER JOIN project.renting " \
                               "NATURAL JOIN project.HotelRoom NATURAL JOIN project.RoomType " \
                               "JOIN project.Customer ON Customer.PersonContactID = Booking.CustomerContactID " \
                               "JOIN project.Person ON Person.ContactID = Customer.PersonContactID " \
                               "WHERE HotelRoom.HotelChainContactID={0} ORDER BY RoomNumber ASC;".format(hotel_chain_contact_id)

    m_database_cursor.execute(get_all_booked_rooms_info_query)
    hotel_booked_rooms_info_list = m_database_cursor.fetchall()

    # Iterate through the hotel room search results and package the results in SpecificHotelBookings and place them in the given structure's list for the caller to get.
    for BookingID, RoomTypeID, RoomNumber, Price, Capacity, Extendable, View, FirstName, LastName, ReserveStartDateTime, ReserveEndDateTime, CheckInDateTime, CheckOutDateTime in hotel_booked_rooms_info_list:

        customer_full_name = ("{0} {1}".format(FirstName, LastName))

        specific_hotel_room_info = SpecificHotelBookings(RoomNumber, Price, Capacity, Extendable, View, customer_full_name, ReserveStartDateTime, ReserveEndDateTime, CheckInDateTime, CheckOutDateTime, BookingID)

        specific_hotel_room_info.list_of_amenities = _get_list_of_all_amenities("RoomTypeID", RoomTypeID, "RoomTypeAmenity")

        all_booked_hotel_rooms_structure.list_of_specific_hotel_room_bookings.append(specific_hotel_room_info)



def _process_check_in_customer_request(check_in_customer_structure):
    # Process the request to check-in a customer that already has a reserved booking in the system.

    booking_id = check_in_customer_structure.booking_id

    try:
        AuxiliaryDBAccessFunctions.insert_into_table(m_database_connection, "Renting",
                                                     "(BookingID, CheckInDateTime, CheckOutDateTime, Payment)",
                                                     "({0}, CURRENT_TIMESTAMP, NULL, 0)".format(booking_id))

    except Exception as error_msg:
        check_in_customer_structure.error_message = str(error_msg)
        m_database_connection.rollback()
        return

    # Get the current date time and the check-in time (this will be the same as CURRENT_TIMESTAMP above.
    check_in_customer_structure.check_in_date_time = datetime.now()



def _process_check_out_customer_request(checkout_customer_structure):

    booking_id = checkout_customer_structure.booking_id

    # 1. First step is to obtain all the information required for the room booking archive.

    # Create a query to obtain all the necessary data about all the rooms in the specific hotel other than their amenities.
    get_all_booking_archive_info_query = "SELECT RoomTypeID, ReserveStartDateTime, ReserveEndDateTime, TotalNumberOfOccupants, CheckInDateTime, CheckOutDateTime," \
                                         "RoomNumber, Price, Capacity, View, Extendable, BrandName, PhysicalAddress_Line1, PhysicalAddress_Line2, " \
                                         "City, Prov_State, Country, Postal_Zip, PersonContactID " \
                                         "FROM project.Booking " \
                                         "NATURAL LEFT JOIN project.Renting " \
                                         "NATURAL JOIN project.HotelRoom " \
                                         "NATURAL JOIN project.RoomType " \
                                         "JOIN project.Customer ON Booking.CustomerContactID = Customer.PersonContactID " \
                                         "JOIN project.HotelChain ON HotelRoom.HotelChainContactID = HotelChain.ContactID " \
                                         "JOIN project.HotelBrand ON HotelChain.HotelBrandContactID = HotelBrand.ContactID " \
                                         "JOIN project.ContactableEntity ON HotelChain.ContactID = ContactableEntity.ContactID " \
                                         "WHERE BookingID = {0};".format(booking_id)


    m_database_cursor.execute(get_all_booking_archive_info_query)
    booking_archive_info = m_database_cursor.fetchall()

    if len(booking_archive_info) == 0:
        checkout_customer_structure.error_message = "The specific booking could nto be found."
        return


    RoomTypeID, ReserveStartDateTime, ReserveEndDateTime, TotalNumberOfOccupants, CheckInDateTime, CheckOutDateTime, \
        RoomNumber, Price, Capacity, View, Extendable, BrandName, PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip, \
        PersonContactID =  booking_archive_info[0]

    list_of_room_amenities = _get_list_of_all_amenities("RoomTypeID", RoomTypeID, "RoomTypeAmenity")


    # The step of moving the booking data from current to archives must be an atomic transaction.

    try:
        # 2. Second step is delete the booking and renting data from the current bookings and current rentings tables.
        delete_booking_query = "DELETE FROM project.Booking WHERE BookingID={0};".format(booking_id)
        m_database_cursor.execute(delete_booking_query)


        # 3. Third step is to insert the booking data into the archives table.

        # Start by executing an SQL query to get the next highest booking archive ID which will be the ID for the new customer.
        query = "SELECT MAX(ArchiveID)+1 as new_archive_id_value FROM project.BookingArchive;"
        m_database_cursor.execute(query)
        new_ids = m_database_cursor.fetchall()
        # Grab the data out of the list of tuples and convert it to an integer.
        new_archive_id = int(new_ids[0][0])

        # Remove the milliseconds at the end, if any.
        CheckInDateTime = str(CheckInDateTime).split(".")[0]

        if CheckOutDateTime is None:
            CheckOutDateTime = "CURRENT_TIMESTAMP"
        else:
            CheckOutDateTime = "'" + str(CheckOutDateTime) + "'"

        booking_archive_insert_query = "INSERT INTO project.BookingArchive (ArchiveID, ReserveStartDateTime, ReserveEndDateTime, TotalNumberOfOccupants, CheckInDateTime, " \
                                       "CheckOutDateTime, RoomNumber, RoomPrice, RoomCapacity, RoomView, RoomExtendable, HotelBrandName, " \
                                       "HotelChainPhysicalAddress_Line1, HotelChainPhysicalAddress_Line2, HotelChainCity, HotelChainProv_State, HotelChainCountry, " \
                                       "HotelChainPostal_Zip, CustomerContactID) " \
                                       "VALUES ({0}, '{1}', '{2}', {3}, '{4}', " \
                                       "{5}, {6}, {7}, {8}, '{9}', '{10}', '{11}', " \
                                       "'{12}', '{13}', '{14}', '{15}', '{16}', '{17}', {18});" \
                                       "".format(new_archive_id, ReserveStartDateTime, ReserveEndDateTime, TotalNumberOfOccupants, CheckInDateTime, \
                                       CheckOutDateTime, RoomNumber, Price, Capacity, View, Extendable, BrandName, \
                                       PhysicalAddress_Line1, PhysicalAddress_Line2, City, Prov_State, Country, Postal_Zip, PersonContactID)

        m_database_cursor.execute(booking_archive_insert_query)

        for room_amenity in list_of_room_amenities:
            insert_booking_amenity_archive_query = "INSERT INTO project.BookingArchiveAmenity (ArchiveID, Amenity) VALUES ({0},'{1}');".format(new_archive_id, room_amenity)
            m_database_cursor.execute(insert_booking_amenity_archive_query)

        # This completed the atomic transaction.
        m_database_connection.commit()
        checkout_customer_structure.check_out_successful = True

    except Exception as error_msg:
        checkout_customer_structure.error_message = str(error_msg)
        # Rollback the entire transaction so none of it happens.
        m_database_connection.rollback()








# This module must subscribe to each topic published by GUI components so it can process their requests for database access.
ThePubSubMachine.subscribe(_process_authenticate_customer_request, AUTHENTICATE_CUSTOMER_TOPIC)
ThePubSubMachine.subscribe(_process_register_new_customer_request, REGISTER_NEW_CUSTOMER_TOPIC)
ThePubSubMachine.subscribe(_process_verify_person_email_existence_request, VERIFY_EMAIL_EXISTENCE_TOPIC)
ThePubSubMachine.subscribe(_process_reset_password_request, RESET_PASSWORD_TOPIC)
ThePubSubMachine.subscribe(_process_authenticate_employee_request, AUTHENTICATE_EMPLOYEE_TOPIC)
ThePubSubMachine.subscribe(_process_get_personal_information_request, GET_PERSONAL_INFO_TOPIC)
ThePubSubMachine.subscribe(_process_process_book_customer_request, BOOK_CUSTOMER_TOPIC)
ThePubSubMachine.subscribe(_process_obtain_hotel_room_options_request, OBTAIN_HOTEL_ROOM_OPTIONS_TOPIC)
ThePubSubMachine.subscribe(_process_search_hotel_rooms_request, SEARCH_HOTEL_ROOMS_TOPIC)
ThePubSubMachine.subscribe(_process_get_customer_information_request, GET_CUSTOMER_INFO_TOPIC)
ThePubSubMachine.subscribe(_process_sql_query_request, SQL_QUERY_TOPIC)
ThePubSubMachine.subscribe(_process_customer_current_booking_info_request, CUSTOMER_CURRENT_BOOKING_INFO_TOPIC)
ThePubSubMachine.subscribe(_process_hotel_chain_characteristics_request, HOTEL_CHAIN_CHARACTERISTICS_TOPIC)
ThePubSubMachine.subscribe(_process_customer_past_booking_info_request, CUSTOMER_PAST_BOOKING_INFO_TOPIC)
ThePubSubMachine.subscribe(_process_all_hotel_rooms_in_chain_request, ALL_HOTEL_ROOMS_IN_CHAIN_TOPIC)
ThePubSubMachine.subscribe(_process_cancel_customer_booking_request, CANCEL_CUSTOMER_BOOKING_TOPIC)
ThePubSubMachine.subscribe(_process_available_hotel_rooms_in_chain_request, AVAILABLE_HOTEL_ROOMS_IN_CHAIN_TOPIC)
ThePubSubMachine.subscribe(_process_all_booked_hotel_rooms_in_chain_request, ALL_BOOKED_HOTEL_ROOMS_IN_CHAIN_TOPIC)
ThePubSubMachine.subscribe(_process_check_in_customer_request, CHECK_IN_CUSTOMER_TOPIC)
ThePubSubMachine.subscribe(_process_check_out_customer_request, CHECK_OUT_CUSTOMER_TOPIC)





if __name__ == "__main__":
    # This can be used to test some of the service functions if needed.
    pass
