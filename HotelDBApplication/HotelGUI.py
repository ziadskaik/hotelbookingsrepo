"""
Created on : March 2 2021

Author : KD

This is the main script that launches the entire hotel bookings GUI application.
"""

from PyQt5.QtWidgets import QApplication

import sys
from pubsub import pub as ThePubSubMachine

from GenericUtilities.PublishSubscribeTopics import *


# These singletons must be imported so they that get initialized.
from ComponentFactory import ComponentFactoryModule
from DatabaseConnection import DatabaseConnectionModule

# This is the component names and their parameter. These components are all instantiated right when the application is launched.
INITIAL_COMPONENTS_AND_ARGS  = {"WelcomePage" : {}}


if __name__ == "__main__":

    app = QApplication(sys.argv)

    # Instantiate every component in the initial list.
    for current_component_name, current_keyword_args in INITIAL_COMPONENTS_AND_ARGS.items():
        # This message is a request for instantiating a new component. This message is published on the machine and makes its way to the component factory.
        ThePubSubMachine.sendMessage(GENERATE_NEW_COMPONENT_TOPIC, component_name=current_component_name, kwargs=current_keyword_args)

    app.exec_()

