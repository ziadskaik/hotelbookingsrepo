

import smtplib, ssl

port = 465  # For SSL
smtp_server = "smtp.gmail.com"

sender_email = "group43hotelbookingservices@gmail.com"  # Enter your address
password = "OurH0telBookingServiceRocks*"



registration_successful_message = """\
Subject: Welcome to Hotel Booking Services.

This message is to confirm your registration as a customer to the Hotel Bookings application.

Your customer ID is
"""


def send_registration_confirmation_email(receiver_email_address, customer_id):


    context = ssl.create_default_context()
    with smtplib.SMTP_SSL(smtp_server, port, context=context) as server:
        server.login(sender_email, password)
        server.sendmail(sender_email, receiver_email_address, registration_successful_message.rstrip() + " " + str(customer_id) + ".")



password_reset_message = """\
Subject: Hotel Booking Services Password Reset

This message is to reset your password in the application. Enter the code below to verify your identity.

If you did not request a password reset, please contact your system administrator. 

Your one-time verification code is 
"""

def send_password_reset_email(receiver_email_address, verification_code):


    context = ssl.create_default_context()
    with smtplib.SMTP_SSL(smtp_server, port, context=context) as server:
        server.login(sender_email, password)
        server.sendmail(sender_email, receiver_email_address, password_reset_message.rstrip() + " " + str(verification_code) + ".")



password_reset_confirmation_message = """\
Subject: Hotel Booking Services - Your password has successfully been changed

This message is to confirm that your password was just reset.

If you did not request a password reset, please contact your system administrator. 
"""

def send_password_reset_confirmation_email(receiver_email_address):


    context = ssl.create_default_context()
    with smtplib.SMTP_SSL(smtp_server, port, context=context) as server:
        server.login(sender_email, password)
        server.sendmail(sender_email, receiver_email_address, password_reset_confirmation_message.rstrip())



room_booking_confirmation_message = """\
Subject: Hotel Room Booking Confirmation.


"""


def send_booking_confirmation_email(room_booking_info_structure):

    hotel_reservation_email_string = room_booking_confirmation_message + "Dear " + room_booking_info_structure.customer_name + ",\n\n"
    hotel_reservation_email_string += "This message is to confirm your following hotel reservation.\n\n"

    hotel_reservation_email_string += "Booking ID : " + str(room_booking_info_structure.booking_id)
    hotel_reservation_email_string += "\n" +room_booking_info_structure.hotel_name + " (" + room_booking_info_structure.star_category + " stars)"
    hotel_reservation_email_string += "\n" + room_booking_info_structure.full_address
    hotel_reservation_email_string += "\n" + room_booking_info_structure.phone_number
    hotel_reservation_email_string += "\nReservation start : " + room_booking_info_structure.reserve_start_datetime
    hotel_reservation_email_string += "\nReservation end : " + room_booking_info_structure.reserve_end_datetime
    hotel_reservation_email_string += "\nTotal cost for booking : $" + str(room_booking_info_structure.total_cost) + " CAD"
    hotel_reservation_email_string += "\n" + room_booking_info_structure.view_type + " view"
    hotel_reservation_email_string += "\nAmenities : " + str(room_booking_info_structure.list_of_amenities).replace("[", "").replace("]", "").replace("'", "")


    receiver_email_address = room_booking_info_structure.customer_email_address

    context = ssl.create_default_context()
    with smtplib.SMTP_SSL(smtp_server, port, context=context) as server:
        server.login(sender_email, password)
        server.sendmail(sender_email, receiver_email_address, hotel_reservation_email_string.rstrip())


room_booking_cancellation_confirmation_message = """\
Subject: Hotel Room Booking Cancellation Confirmation.


"""
def send_booking_cancellation_confirmation_email(customer_name, customer_email, booking_id, hotel_brand_name):

    hotel_reservation_email_string = room_booking_confirmation_message + "Dear " + customer_name + ",\n\n"
    hotel_reservation_email_string += "This message is to confirm your following hotel booking cancellation.\n\n"

    hotel_reservation_email_string += "Booking ID : " + str(booking_id)
    hotel_reservation_email_string += "\n" + hotel_brand_name


    receiver_email_address = customer_email

    if receiver_email_address is None:
        return

    context = ssl.create_default_context()
    with smtplib.SMTP_SSL(smtp_server, port, context=context) as server:
        server.login(sender_email, password)
        server.sendmail(sender_email, receiver_email_address, hotel_reservation_email_string.rstrip())