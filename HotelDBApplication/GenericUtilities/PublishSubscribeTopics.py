"""
Created on : March 9 2021

Author : KD

This module contains all the hardcoded names of all topics used for the publish-subscribe machinery.
"""


GENERATE_NEW_COMPONENT_TOPIC = "GenerateNewComponentTopic"

AUTHENTICATE_CUSTOMER_TOPIC = "AuthenticateCustomerTopic"

REGISTER_NEW_CUSTOMER_TOPIC = "RegisterNewCustomerTopic"

VERIFY_EMAIL_EXISTENCE_TOPIC = "VerifyEmailExistenceTopic"

RESET_PASSWORD_TOPIC = "ResetPasswordTopic"

AUTHENTICATE_EMPLOYEE_TOPIC = "AuthenticateEmployeeTopic"

GET_PERSONAL_INFO_TOPIC = "GetPersonalInformationTopic"

BOOK_CUSTOMER_TOPIC = "BookCustomerTopic"

OBTAIN_HOTEL_ROOM_OPTIONS_TOPIC = "ObtainHotelRoomOptionsTopic"

SEARCH_HOTEL_ROOMS_TOPIC = "SearchHotelRoomsTopic"

GET_CUSTOMER_INFO_TOPIC = "GetCustomerInfoTopic"

SQL_QUERY_TOPIC = "SQLQueryTopic"

HOTEL_CHAIN_CHARACTERISTICS_TOPIC = "HotelChainCharacteristicsTopic"
CUSTOMER_CURRENT_BOOKING_INFO_TOPIC = "CustomerCurrentBookingInfoTopic"

ALL_HOTEL_ROOMS_IN_CHAIN_TOPIC = "AllHotelRoomsInChainTopic"
CUSTOMER_PAST_BOOKING_INFO_TOPIC = "CustomerPastBookingInfoTopic"

AVAILABLE_HOTEL_ROOMS_IN_CHAIN_TOPIC = "AvailableHotelRoomsInChainTopic"

ALL_BOOKED_HOTEL_ROOMS_IN_CHAIN_TOPIC = "AllBookedHotelRoomsInChainTopic"

EMPLOYEE_BOOKING_COMPLETION_TOPIC = "EmployeeBookingCompletionTopic"

EMPLOYEE_BOOKING_ABORTED_TOPIC = "EmployeeBookingAbortedTopic"

CHECK_IN_CUSTOMER_TOPIC = "CheckInCustomerTopic"

CHECK_OUT_CUSTOMER_COMPLETION_TOPIC = "CheckOutCustomerCompletionTopic"

CHECK_OUT_CUSTOMER_TOPIC = "CheckOutCustomerTopic"


CANCEL_CUSTOMER_BOOKING_TOPIC = "CustomerCancelBookingTopic"

