"""
Created on : March 6 2021

Author : KD

This is a library for various small, pop-up, modal dialogs for user feedback.
"""

import sys
from PyQt5.QtWidgets import QMessageBox, QApplication, QErrorMessage



def show_yes_or_no_dialog(title, text):
    """
    Display a dialog to obtain a decision from the user.

    Args:
        title (str): The title of the dialog.
        text (str): The main text containing the question for the user.

    Returns:
        True if the user selected 'Yes' and False if the user selected 'No'.
    """

    choice = QMessageBox.question(None, title, text, QMessageBox.Yes | QMessageBox.No)
    return choice == QMessageBox.Yes



def show_error_dialog(title, details):
    """
    Display a dialog to inform the user of an error that occurred.

    Args:
        title (str): The title of the dialog.
        details (str): A detailed message for the user.

    Returns:
        None
    """

    msg = QMessageBox()
    msg.setIcon(QMessageBox.Critical)
    msg.setText("Error")
    msg.setInformativeText(details)
    msg.setWindowTitle(title)
    msg.exec_()


def show_success_dialog(title, message):
    """
    Display a dialog to inform the user that an operation was successful.

    Args:
        title (str): The title of the dialog.
        message (str): A detailed message for the user.

    Returns:

    """
    QMessageBox.information(None, title, message)




if __name__ == "__main__":

    app = QApplication(sys.argv)
    # get_Yes_or_No_Dialog("Test", "Quit?")
    # show_error_dialog("Error", "More details")
    show_success_dialog("QMessageBox.information()", "Download the selected item.")
    app.exec_()