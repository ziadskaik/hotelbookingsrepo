"""
Created on : March 9 2021

Author : KD

This module contains all the data structures for transportable messages used to exchange data between various distant
entities in the application via the publish-subscribe machinery.
"""



class AuthenticateCustomerStructure:
    """
    This is the structure for communicating requests for customer authentication into the system.

    Attributes:
        customer_id_or_email_or_phone (str) : A string representation either the customer ID, the customer email, or the customer phone number.
        plain_text_password (str): A string for the unencrypted password.

        authentication_success (bool): A Boolean flag to indicate whether the customer was successfully authenticated or not.
        customer_id (str) : The customer's contactable entity ID.
        customer_full_name (str) : The employee's first and last name concatenated with a space in between.
        special_admin_login (bool) : A special backdoor flag for administrator login.
    """

    def __init__(self, customer_id_or_email_or_phone, plain_text_password):
        # This is the request portion of the message.
        self.customer_id_or_email_or_phone = customer_id_or_email_or_phone
        self.plain_text_password = plain_text_password

        # This is the response portion of the message.
        self.authentication_success = False
        self.customer_id = None
        self.customer_full_name = None
        self.special_admin_login = False



class RegisterCustomerStructure:
    """
    This is the structure for communicating requests for registering new customers into the system.

    Attributes:
        first_name (str) : The customer's first name.
        last_name (str) : The customer's last name.
        sin (str) : The customer's sin.
        address_line_1 (str) : The customer's first address line.
        address_line_2 (str) : The customer's second address line.
        city (str) : The customer's city of residence
        prov_state (str) : The customer's province of residence.
        country (str) : The customer's country of residence.
        postal_zip (str) : The postal or zip code of the residence.
        phone (str) : The customer's phone number.
        email (str) : The customer's email address.
        plain_text_password (str): A string for the unencrypted password.

        error_message (str): A message indicating the error that occurred or none if there were no errors.
        new_customer_id (int): The newly generated ID for the customer or -1 if an error occurred.
    """


    def __init__(self, first_name, last_name, sin, address_line_1, address_line_2, city, prov_state, country, postal_zip, phone, email, plain_text_password):
        # This is the request portion of the message.
        self.first_name = first_name
        self.last_name = last_name
        self.sin = sin
        self.address_line_1 = address_line_1
        self.address_line_2 = address_line_2
        self.city = city
        self.prov_state = prov_state
        self.country = country
        self.postal_zip = postal_zip
        self.phone = phone
        self.email = email
        self.plain_text_password = plain_text_password

        # This is the response portion of the message.
        self.error_message = None
        self.new_customer_id = -1



class VerifyEmailExistenceStructure:
    """
    This is the structure for communicating requests to verify that the given email is in fact registered in the system for either a customer or an employee.

    Attributes:
        email_address (str) : A string representation the customer or employee email.

        email_exists (bool): A Boolean flag to indicate whether the given customer or employee email exists or not.
    """

    def __init__(self, email_address):
        # This is the request portion of the message.
        self.email_address = email_address

        # This is the response portion of the message.
        self.email_exists = False



class ResetPasswordStructure:
    """
    This is the structure for communicating requests to reset the password for an employee or a customer.

    Attributes:
        person_email (str) : A string representation the customer or employee email.
        new_plain_text_password (bool): A string representing the new password for the employee or customer.

        error_message (str): A message indicating the error that occurred or none if there were no errors.
        password_update_successful (bool): A Boolean flag to indicate if the password update was successful or not.

    """

    def __init__(self, person_email, new_plain_text_password):
        # This is the request portion of the message.
        self.person_email = person_email
        self.new_plain_text_password = new_plain_text_password

        # This is the response portion of the message.
        self.error_message = None
        self.password_update_successful = False



class AuthenticateEmployeeStructure:
    """
    This is the structure for communicating requests for employee authentication into the system.

    Attributes:
        employee_id_or_email_or_phone (str) : A string representation either the employee ID, the employee email, or the employee phone number.
        plain_text_password (str): A string for the unencrypted password.

        authentication_success (bool): A Boolean flag to indicate whether the employee was successfully authenticated or not.
        error_message (str): A message indicating the error that occurred or none if there were no errors.
        employee_id (str) : The employee's contactable entity ID.
        employee_full_name (str) : The employee's first and last name concatenated with a space in between.
        list_of_hotel_id_name_and_full_address_tuples (list<tuple>) : The list of hotel IDs, hotel brand names, and hotel (partial) addresses which the employee works for.
        special_admin_login (bool) : A special backdoor flag for administrator login.
    """

    def __init__(self, employee_id_or_email_or_phone, plain_text_password):
        # This is the request portion of the message.
        self.employee_id_or_email_or_phone = employee_id_or_email_or_phone
        self.plain_text_password = plain_text_password

        # This is the response portion of the message.
        self.authentication_success = False
        self.error_message = None
        self.employee_id = None
        self.employee_full_name = None
        self.list_of_hotel_id_name_and_full_address_tuples = []
        self.special_admin_login = False



class GetPersonalInformationStructure:

    """
    This is the structure for getting personal information for an employee.

    Attributes:
        employee_name (str) : A string representation of the employee name.
        employee_id (int)   : An integer representation of the employee id.

        sin (int) : An integer representation of the employee SIN.
        address (str) : A string representation of the employee address.
        phone_number (int) : An integer representation of the employee phone number.
        email (str) : A string representation of the employee email.
        salary (int) : An integer representation of the employee salary.
        date_of_hire(str) : A string representation of the employee date of hire.
    """
        
    def __init__(self, employee_name, employee_id):
        # This is the request portion of the message.
        self.name = employee_name
        self.id = employee_id

        # This is the response portion of the message.
        self.sin = None

        self.line1 = ""
        self.line2 = ""
        self.city = ""
        self.prov_state = ""
        self.country = ""
        self.postal_zip = ""

        self.phone_number = None
        self.email = ""
        self.salary = None
        self.date_of_hire = ""



class BookCustomerStructure:
    """
    This is the structure for booking the customer in a room.

    Attributes:

        hotel_chain_contact_id: An integer representing the hotel contact id.
        room_number: An integer representing the room number the customer will be booked for.
        reserve_start_datetime: A date representing the start date time for the reservation.
        reserve_end_datetime: A date representing the end date time for the reservation.
        customer_identification :  A variable type representing the customer identification ( ID/email/phone number)
        number_of_occupants: An integer representing the total number of occupants for the room which will be booked.
        booking_id (int)    : An integer representing the booking id.
        error_message (str): A message indicating the error that occurred or none if there were no errors.
    """

    def __init__(self, hotel_chain_contact_id, room_number, reserve_start_datetime, reserve_end_datetime,
                 customer_identification, number_of_occupants):
        # This is the request portion of the message.
        self.hotel_chain_contact_id = hotel_chain_contact_id
        self.room_number = room_number
        self.reserve_start_datetime = reserve_start_datetime
        self.reserve_end_datetime = reserve_end_datetime
        self.customer_identification = customer_identification
        self.number_of_occupants = number_of_occupants

        # This is the response portion of the message.
        self.booking_id = None
        self.error_message = None



class HotelRoomOptions:
    """
    This is the structure to obtain the types of rooms available, for each city, in the system.

    Attributes:
        maximum_capacity (int): The capacity for the largest room of any hotel in a given city.
        list_of_views (list): The list of all views types among all hotel rooms in the city.
        list_of_amenities (list): The list of all amenity types among all hotel rooms in the city
    """

    def __init__(self):
        self.maximum_capacity = None
        self.list_of_views = []
        self.list_of_amenities = []



class HotelRoomSearchResult:
    """
    This is the structure to obtain a list of available rooms for a given search criteria.

    The city and amenities are not included in the structure since they are fixed an known ahead of time.

    Attributes:
        hotelChainContactID (str): The ID for the hotel chain the room belongs to.
        room_number (str): The room's number.
        hotel_name (str): The name of the hotel brand the room belong to.
        star_category (str): The rating of the hotel chain the room belongs to.
        full_address (str): The complete address, all in one line, of the hotel chain the room belong to.
        price (str): The price (per night) of the hotel room.
        capacity (str): The maximum capacity of the room (this includes the extension for the room, if any).
        view (str): The view type for the room.
        phone_number (str): The phone number for the hotel chain the room belongs to.
        list_of_amenities (list<str>): The list of all amenities the room does have.
    """

    def __init__(self):
        self.hotelChainContactID = None
        self.room_number = None
        self.hotel_name = None
        self.star_category = None
        self.full_address = None
        self.price = None
        self.capacity = None
        self.view = None
        self.phone_number = None
        self.list_of_amenities = []

    def __eq__(self, other):
        """Overrides the default implementation"""
        if isinstance(other, HotelRoomSearchResult):
            result = True
            result = result and self.hotel_name == other.hotel_name
            result = result and self.star_category == other.star_category
            result = result and self.full_address == other.full_address
            result = result and self.price == other.price
            result = result and self.capacity == other.capacity
            result = result and self.view == other.view
            result = result and self.phone_number == other.phone_number
            return result
        return False

    def __hash__(self):
        return hash((self.hotel_name, self.star_category, self.full_address, self.price, self.capacity, self.view, self.phone_number))



class HotelRoomSearchStructure:
    """
    This is the structure for communicating requests to search for hotel rooms in the system with certain criteria.

    Attributes:
        city (str) : The city in which to search for a hotel room.
        reserve_start_unix_epoch (str) : The unix epoch in seconds for the time to begin the reservation.
        reserve_end_unix_epoch (str) : The unix epoch in seconds for the time to end the reservation
        minimum_cost (str) : The minimum cost of the hotel room, per night, to search for.
        maximum_cost (str) : The maximum cost of the hotel room, per night, to search for
        number_of_occupants (str) : The intended number of occupants for the hotel room reservation.
        view_type (str) : The view type for the room.
        list_of_amenities (list<str>): The list of all amenities the room must have.


        error_message (str): A message indicating the error that occurred or none if there were no errors.
        list_of_hotel_room_search_results (list<HotelRoomSearchResult>) : The list information on hotel rooms that match the given search criteria.
    """

    def __init__(self, city, reserve_start_unix_epoch, reserve_end_unix_epoch, minimum_cost, maximum_cost, number_of_occupants, view_type, list_of_amenities):
        # This is the request portion of the message.
        self.city = city
        self.reserve_start_unix_epoch = reserve_start_unix_epoch
        self.reserve_end_unix_epoch = reserve_end_unix_epoch
        self.minimum_cost = minimum_cost
        self.maximum_cost = maximum_cost
        self.number_of_occupants = number_of_occupants
        self.view_type = view_type
        self.list_of_amenities = list_of_amenities


        # This is the response portion of the message.
        self.error_message = None
        self.list_of_hotel_room_search_results = []



class HotelRoomBookingInfoStructure:
    """
    This is the structure for communicating requests to search for hotel rooms in the system with certain criteria.

    Attributes:
        hotel_name (str): The name of the hotel brand the room belong to.
        star_category (str): The rating of the hotel chain the room belongs to.
        full_address (str): The complete address, all in one line, of the hotel chain the room belong to.
        phone_number (str): The phone number for the hotel chain the room belongs to.
        reserve_start_datetime (str) : The human readable timestamp the reservation begins on.
        reserve_end_datetime (str) : The human readable timestamp the reservation ends on.
        total_cost(str) : The total cost of the room for the entire duration.
        capacity (str): The maximum capacity of the room (this includes the extension for the room, if any).
        view_type (str): The view type for the room.
        list_of_amenities (list<str>): The list of all amenities the room has.
        booking_id (str) : The id for the booking.
        customer_name (str) : The name of the client the room is booked under.
        customer_email_address (str): The email address of the customer.
    """

    def __init__(self, hotel_name, star_category, full_address, phone_number, reserve_start_datetime, reserve_end_datetime, total_cost, capacity, view_type, list_of_amenities,
                 booking_id, customer_name, customer_email_address):
        self.hotel_name = hotel_name
        self.star_category = star_category
        self.full_address = full_address
        self.phone_number = phone_number
        self.reserve_start_datetime = reserve_start_datetime
        self.reserve_end_datetime = reserve_end_datetime
        self.total_cost = total_cost
        self.capacity = capacity
        self.view_type = view_type
        self.list_of_amenities = list_of_amenities
        self.booking_id = booking_id
        self.customer_name = customer_name
        self.customer_email_address = customer_email_address



class CustomerInformationStructure:
    """
    This is the structure for communicating requests to search for hotel rooms in the system with certain criteria.

    Attributes:
        customer_id (str): The customer's id.
        customer_name (str): The customer's full name.
        customer_email_address (str): The customer's email address.
    """

    def __init__(self, customer_id):
        # This is the request portion of the message.
        self.customer_id = customer_id

        # This is the response portion of the message.
        self.customer_name = None
        self.customer_email_address = None



class SQLQueryStructure:
    """
    This is the structure for executing custom SQL queries by database admins.

    Attributes:
        sql_query (str) : The sql query.
        error_message (str): A message indicating the error that occurred or none if there were no errors.
        sql_query_result: The result of an sql query when applicable.
    """

    def __init__(self, sql_query):
        # This is the request portion of the message.
        self.sql_query = sql_query


        # This is the response portion of the message.

        self.error_message = None
        self.sql_query_result_row_count = 0
        self.sql_query_result_tuples = None
        self.sql_query_result_headers = None



class HotelChainCharacteristicsStructure:
    """
    This is the structure for communicating requests to obtain all the characteristics of rooms in a specific hotel chain.

    Attributes:
        hotel_chain_contact_id (str) : The contact id for the specific hotel chain.
        list_of_views (list<str>): The list of all views available at the specific hotel chain.
        maximum_capacity (int): The capacity for the largest room in specific hotel chain.
        maximum_cost (float): The cost of the most expensive room in the specific hotel chain.
        list_of_amenities (list<str>): The list of all amenities available in the specific hotel chain.
    """

    def __init__(self, hotel_chain_contact_id):
        # This is the request portion of the message.
        self.hotel_chain_contact_id = hotel_chain_contact_id

        # This is the response portion of the message.
        self.list_of_views = None
        self.maximum_capacity = None
        self.maximum_cost = None
        self.list_of_amenities = None
        

class CustomerBookingInfo:
    """
    This structure holds data about a specific booking for a customer.

    Attributes:
        booking_id (str) : The booking id for the booking.
        hotel_name (str): The name of the hotel brand the room belong to.
        full_address (str): The complete address, all in one line, of the hotel chain the room belong to.
        price (str) : The price per night for the room.
        number_of_occupants (str): The number of occupants in the room for the booking.
        view_type (str): The view type for the room.
        room_number (str): The number for the room in the hotel.
        list_of_amenities (list<str>): The list of all amenities the room has.
        reserve_start_datetime (datetime) : The datetime for the start of the reservation.
        reserve_end_datetime (datetime) : The datetime for the end of the reservation.
        check_in_datetime (datetime) : The datetime for when the customer was checked-in, if any.
        check_out_datetime (datetime) : The datetime for the customer was checked-out, if any
    """

    def __init__(self, booking_id, hotel_name, full_address, price, number_of_occupants, view_type, room_number, reserve_start_datetime, reserve_end_datetime,
                 check_in_datetime, check_out_datetime):
        self.booking_id = booking_id
        self.hotel_name = hotel_name
        self.full_address = full_address
        self.price_per_night = price
        self.number_of_occupants = number_of_occupants
        self.view_type = view_type
        self.room_number = room_number
        self.reserve_start_datetime = reserve_start_datetime
        self.reserve_end_datetime = reserve_end_datetime
        self.check_in_datetime = check_in_datetime
        self.check_out_datetime = check_out_datetime
        self.list_of_amenities = []



class CustomerBookingsStructure:
    """
    This is the structure for requesting the current/past bookings for a customer

    Attributes:
        customer_id (str) : The contact id for the customer.

        error_message (str): A message indicating the error that occurred or none if there were no errors.
        list_of_customer_booking_info <CustomerBookingInfoStructure>: The list of booking information for the specific customer.
    """
    def __init__(self, customer_id):
        # This is the request portion of the message.
        self.customer_id = customer_id

        # This is the response portion of the message.
        self.error_message = None
        self.list_of_customer_booking_info = []


class CancelCustomerBookingStructure:
    """
    This is the structure for cancelling the customer booking.

    Attributes:
        booking_id (str) : The booking id corresponding to the booking entry that will be cancelled.

        error_message (str): A message indicating the error that occurred or none if there were no errors.

    """
    def __init__(self, booking_id):
        # This is the request portion of the message.
        self.booking_id = booking_id

        # This is the response portion of the message.
        self.error_message = None



class SpecificHotelRoomInfo:
    """
    This structure contains information about a specific room in a hotel chain.

    Attributes:
        room_number (str) : The room's number in the hotel.
        price (str): The room's price.
        capacity (str): The capacity for the room, taking into account whether it is extendable or not.
        extendable (str): 'True' if the room can be extended by adding one more bed and false otherwise.
        view_type (str): The view type for the room.
        list_of_amenities (list<str>): The list of all amenities available in the specific hotel chain.
    """

    def __init__(self, room_number, price, capacity, extendable, view_type):
        self.room_number = room_number
        self.price = price
        self.capacity = capacity
        self.extendable = extendable
        self.view_type = view_type
        self.list_of_amenities = []



class AllHotelRoomsStructure:
    """
    This is the structure for communicating requests to obtain data about all the hotel rooms in a specific hotel chain.

    Attributes:
        hotel_chain_contact_id (str) : The contact id for the specific hotel chain.
        list_of_specific_hotel_room_info (list<SpecificHotelRoomInfo>): The list of all hotel room data for the specific hotel.
    """

    def __init__(self, hotel_chain_contact_id):
        # This is the request portion of the message.
        self.hotel_chain_contact_id = hotel_chain_contact_id

        # This is the response portion of the message.
        self.list_of_specific_hotel_room_info = []



class EmployeeHotelRoomSearchStructure:
    """
    This is the structure for communicating requests to search for available hotel rooms in a specific hotel chain.

    Attributes:
        hotel_chain_contact_id (str): The ID for the hotel chain to search rooms into.
        reserve_start_unix_epoch (str) : The unix epoch in seconds for the time to begin the reservation.
        reserve_end_unix_epoch (str) : The unix epoch in seconds for the time to end the reservation
        minimum_cost (str) : The minimum cost of the hotel room, per night, to search for.
        maximum_cost (str) : The maximum cost of the hotel room, per night, to search for
        minimum_capacity (str) : The minimum capacity of the room.
        view_types (list<str>) : The acceptable view types for the room.
        list_of_amenities (list<str>): The list of all amenities the room must have.


        error_message (str): A message indicating the error that occurred or none if there were no errors.
        list_of_hotel_room_search_results (list<SpecificHotelRoomInfo>) : The list information on hotel rooms that match the given search criteria.
    """

    def __init__(self, hotel_chain_contact_id, reserve_start_unix_epoch, reserve_end_unix_epoch, minimum_cost, maximum_cost, minimum_capacity, view_types, list_of_amenities):
        # This is the request portion of the message.
        self.hotel_chain_contact_id = hotel_chain_contact_id
        self.reserve_start_unix_epoch = reserve_start_unix_epoch
        self.reserve_end_unix_epoch = reserve_end_unix_epoch
        self.minimum_cost = minimum_cost
        self.maximum_cost = maximum_cost
        self.minimum_capacity = minimum_capacity
        self.view_types = view_types
        self.list_of_amenities = list_of_amenities


        # This is the response portion of the message.
        self.error_message = None
        self.list_of_hotel_room_search_results = []



class SpecificHotelBookings:
    """
    This structure contains information about a booking and/or renting in a specific hotel chain.

    Attributes:
        room_number (str) : The room's number in the hotel.
        price (str): The room's price.
        capacity (str): The capacity for the room, taking into account whether it is extendable or not.
        extendable (str): 'True' if the room can be extended by adding one more bed and false otherwise.
        view_type (str): The view type for the room.
        list_of_amenities (list<str>): The list of all amenities available in the specific hotel chain.
        customer_full_name (str): The first and last name of the customer.
        reserve_start_datetime (datetime): A date representing the start date time for the reservation of the room.
        reserve_end_datetime (datetime): A date representing the end date time for the reservation of the room.
        check_in_datetime (datetime): A date representing the start date time for the check-in of the room, if any.
        check_out_datetime (datetime): A date representing the end date time for the check-out of the room, if any.
        booking_id (str): The unique identifier for the specific booking.
    """

    def __init__(self, room_number, price, capacity, extendable, view_type, customer_full_name, reserve_start_datetime, reserve_end_datetime, check_in_datetime, check_out_datetime, booking_id):
        self.room_number = room_number
        self.price = price
        self.capacity = capacity
        self.extendable = extendable
        self.view_type = view_type
        self.customer_full_name = customer_full_name
        self.list_of_amenities = []
        self.reserve_start_datetime = reserve_start_datetime
        self.reserve_end_datetime = reserve_end_datetime
        self.check_in_datetime = check_in_datetime
        self.check_out_datetime = check_out_datetime
        self.booking_id = booking_id



class AllBookedRoomsStructure:
    """
    This is the structure for communicating requests to obtain all hotel rooms booked or rented in a specific hotel chain.

    Attributes:
        hotel_chain_contact_id (str): The ID for the hotel chain to search rooms into.

        error_message (str): A message indicating the error that occurred or none if there were no errors.
        list_of_specific_hotel_room_bookings (list<SpecificHotelBookings>) : The list information on hotel room booking and rentings for a specific hotel chain.
    """

    def __init__(self, hotel_chain_contact_id):
        # This is the request portion of the message.
        self.hotel_chain_contact_id = hotel_chain_contact_id

        # This is the response portion of the message.
        self.error_message = None
        self.list_of_specific_hotel_room_bookings = []



class CheckInCustomerStructure:
    """
    This is the structure for checking-in customers once a booking already exist for them.

    Attributes:

        booking_id (str): The unique identifier for the specific booking.

        check_in_date_time (datetime): The date timestamp for when the customer was checked-in.
        error_message (str): A message indicating the error that occurred or none if there were no errors.
    """

    def __init__(self, booking_id):
        # This is the request portion of the message.
        self.booking_id = booking_id

        # This is the response portion of the message.
        self.check_in_date_time = None
        self.error_message = None



class CheckOutCustomerStructure:
    """
    This is the structure for checking-out customers and processing their payment.

    Attributes:

        booking_id (str): The unique identifier for the specific booking.

        check_out_successful (bool): True if the checkout transaction was successful and False otherwise.
        error_message (str): A message indicating the error that occurred or none if there were no errors.
    """

    def __init__(self, booking_id):
        # This is the request portion of the message.
        self.booking_id = booking_id

        # This is the response portion of the message.
        self.check_out_successful = False
        self.error_message = None

