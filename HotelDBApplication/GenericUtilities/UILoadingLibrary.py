"""
Created on : March 9 2021

Author : KD

This module provides services to easily load GUIs from their respective UI file name.

This is done abstract the loading mechanism from GUI components and works for both development and deployment circumstances.
"""

import os
from PyQt5 import uic

from PyEquivalentUIFiles.gui_name_to_class_reference_map import GUI_NAME_TO_CLASS_REFERENCE_MAP


ABSOLUTE_PATH_TO_THIS_MODULE = os.path.dirname(os.path.abspath(__file__))
ABSOLUTE_PATH_TO_COMPONENT_PACKAGE_LIST = ABSOLUTE_PATH_TO_THIS_MODULE.split(os.path.sep)[:-1] + ["Components"]
ABSOLUTE_PATH_TO_COMPONENT_PACKAGE = os.path.sep.join(ABSOLUTE_PATH_TO_COMPONENT_PACKAGE_LIST)

RELATIVE_PATH_TO_GENERATED_GUI_PACKAGE_NAME = "PyEquivalentUIFiles"
ABSOLUTE_PATH_TO_GENERATED_GUI_PACKAGE_NAME = os.path.abspath(RELATIVE_PATH_TO_GENERATED_GUI_PACKAGE_NAME)



def find_original_ui_file_in_project(ui_file_name):

    if not os.path.exists(ABSOLUTE_PATH_TO_COMPONENT_PACKAGE):
        # This is necessary to prevent going any further during application execution after deployment.
        return None

    # Traverse the entire components package and look for the UI file. Returns its full path if found.
    for root, dirs, files in os.walk(ABSOLUTE_PATH_TO_COMPONENT_PACKAGE):
        for file_name in files:
            if file_name == ui_file_name:
                # The file was found.
                relative_path = os.path.join(root, file_name)
                absolute_path = os.path.abspath(relative_path)
                return absolute_path


def find_equivalent_python_gui_file(ui_file_name):
    # Look in the generated PY equivalent UI files to see if the ui file equivalent is there.

    # Convert from .ui extension to .py extension.
    py_file_name = ui_file_name.replace(".ui", ".py")

    # Path to where the python equivalent file potentially is.
    path_to_py_file = ABSOLUTE_PATH_TO_GENERATED_GUI_PACKAGE_NAME + os.path.sep + py_file_name

    # Check of the file is there and if so return the path to it.
    if os.path.exists(path_to_py_file) and os.path.isfile(path_to_py_file):
        return path_to_py_file



def load_gui(ui_file_name, q_widget_controller):
    """

    Args:
        ui_file_name (str): The name of the .ui file for the GUI to load.
        q_widget_controller (QWidget): A reference to the controller object of a component.

    Returns:
        The Qt object representing the loaded GUI.
    """

    # Look for the original .ui file in the project. It should be in the same package as the controller trying to load it.
    path_to_ui_file = find_original_ui_file_in_project(ui_file_name)

    if path_to_ui_file is not None:
        # Since the UI file was found, it gets loaded into the controller by default.
        loaded_gui = uic.loadUi(path_to_ui_file, q_widget_controller)
        loaded_gui.show()
        return loaded_gui

    # Drop the .ui extension.
    ui_file_extensionless_name = ui_file_name.split(".")[0]


    # Check to see if the GUI has an equivalent generated .py file.
    if ui_file_extensionless_name in GUI_NAME_TO_CLASS_REFERENCE_MAP:
        # Must load the GUI from this file.

        # Get a reference to the equivalent Python class.
        equivalent_py_class_reference =  GUI_NAME_TO_CLASS_REFERENCE_MAP[ui_file_extensionless_name]

        # Instantiate the equivalent Python class and load it onto the controller.
        loaded_gui = equivalent_py_class_reference()
        loaded_gui.setupUi(q_widget_controller)
        q_widget_controller.show()

        return loaded_gui


    else:
        # The component's GUI was not found in any format anywhere in the entire project.
        print("ERROR : " + ui_file_name + " not found.")




